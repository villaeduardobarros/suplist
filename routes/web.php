<?php

use Illuminate\Support\Facades\Route;

# home
Route::get('/', 'web\\home\\HomeController@index')->name('web.home');

# search
Route::get('busca', 'web\\search\\SearchController@index')->name('web.search');

# sign up for newsletters
Route::post('cadastro/newsletter', 'web\\home\\HomeController@newsletter')->name('web.footer.newsletter.register');

# new company registration
Route::get('cadastro/{token?}', 'web\\register\\RegisterController@index')->name('web.register');
Route::post('cadastro', 'web\\register\\RegisterController@store')->name('web.register.store');

# search cities by state
Route::post('cidades', 'web\\cities\\CitiesController@search')->name('web.cities.search');
Route::post('cep/check', 'web\\cities\\CitiesController@checkZipcode')->name('web.cities.zipcode.check');
Route::post('cep', 'web\\cities\\CitiesController@zipcode')->name('web.cities.zipcode');
# modal zipcode
Route::post('modal/zipcode', 'web\\modal\\ModalController@showZipcode')->name('web.modal.zipcode');

# terms of use
Route::get('termos-de-uso', 'web\\terms\\TermsOfUserController@index')->name('web.terms');

# sac
Route::get('sac', 'web\\sac\\SacController@index')->name('web.sac');
Route::post('sac/cadastro', 'web\\sac\\SacController@store')->name('web.sac.store');

# about
Route::get('sobre', 'web\\about\\AboutController@index')->name('web.about');

# privacy policy
Route::get('politica-de-privacidade', 'web\\policy\\PolicyController@index')->name('web.policy');

#messaging
Route::get('mensageiro', 'web\\messaging\\MessagingController@send')->name('web.messaging');

# finish order
Route::post('finalizar/pedido', 'web\\orders\\OrdersController@store')->name('web.finish.order');

# finalize the registration
Route::get('empresas/login', 'web\\draft\\DraftController@login')->name('web.draft.login');
Route::post('empresas/login/authenticate', 'web\\draft\\DraftController@authenticate')->name('web.draft.authenticate');
Route::post('empresas/cadastro', 'web\\draft\\DraftController@store')->name('web.draft.store');

# completes the registration of the new company
Route::group(['middleware' => 'drafts'], function() {
	Route::group(['middleware' => 'auth:drafts'], function() {

		Route::get('empresas/dados', 'web\\draft\\DraftController@create')->name('web.draft.data');
		Route::post('empresas/cadastro', 'web\\draft\\DraftController@store')->name('web.draft.store');
		Route::get('empresas/sair', 'web\\draft\\DraftController@index')->name('web.draft.logout');

	});
});

# modal product details
Route::post('modal/product-detail', 'web\\modal\\ModalController@showProductsDetails')->name('web.modal.product.detail');
# displays variations in modal
Route::post('modal/variations-detail', 'web\\modal\\ModalController@showVariationsDetails')->name('web.modal.variations.detail');
# modal shopping cart details
Route::post('modal/shopping-cart-detail', 'web\\modal\\ModalController@showShoppingCartDetails')->name('web.modal.shopping.cart.detail');
# add shopping cart
Route::post('shopping-cart/store', 'web\\cart\\CartController@store')->name('web.shopping.cart.store');
Route::post('shopping-cart/show', 'web\\cart\\CartController@show')->name('web.shopping.cart.show');
Route::post('shopping-cart/show-form', 'web\\cart\\CartController@showForm')->name('web.shopping.cart.show.form');
Route::post('shopping-cart/check-zipcode', 'web\\cart\\CartController@checkZipCode')->name('web.shopping.cart.check.zipcode');
Route::post('shopping-cart/update', 'web\\cart\\CartController@update')->name('web.shopping.cart.update');
Route::post('shopping-cart/delete', 'web\\cart\\CartController@delete')->name('web.shopping.cart.delete');
Route::get('shopping-cart/destroy', 'web\\cart\\CartController@destroy')->name('web.shopping.cart.destroy');

# *********************************
# external product listing
Route::get('token/{token}', 'web\\products\\ProductsController@singleToken')->name('web.products.single.token');
# *********************************

# displays the images
Route::get('storage/{filename}', function($filename){
    return \Image::make(storage_path('public/' . $filename))->response();
});

# sitemap
Route::get('sitemap.xml', 'web\\sitemap\\SitemapController@index')->name('sitemap.xml');

# ********************************
# **** companies control panel
# ********************************
Route::get('control/login', 'control\\login\\LoginController@index')->name('control.login');
Route::post('control/authenticate', 'control\\login\\LoginController@authenticate')->name('control-authenticate');
Route::get('control/logout', 'control\\login\\LoginController@logout')->name('control.logout');

Route::group(['middleware' => 'companies'], function() {
	Route::group(['middleware' => 'auth:companies'], function() {

		Route::get('control/home', 'control\\home\\HomeController@index')->name('control.home');

		Route::get('control/account', 'control\\account\\AccountController@edit')->name('control.account.edit');
		Route::put('control/account/update', 'control\\account\\AccountController@update')->name('control.account.update');

		# modal dynamic rate
		Route::post('control/modal/dynamic-rate', 'control\\modal\\ModalController@showDynamicRate')->name('control.modal.dynamic.rate');
		Route::post('control/modal/dynamic-rate/list', 'control\\modal\\ModalController@listDynamicRate')->name('control.modal.dynamic.rate.list');
		Route::post('control/modal/dynamic-rate/store', 'control\\modal\\ModalController@storeDynamicRate')->name('control.modal.dynamic.rate.store');
		Route::post('control/modal/dynamic-rate/delete', 'control\\modal\\ModalController@destroyDynamicRate')->name('control.modal.dynamic.rate.destroy');

		# products categories
		Route::get('control/products/categories', 'control\\products\\CategoriesController@index')->name('control.products.categories');
		Route::post('control/products/categories/store', 'control\\products\\CategoriesController@store')->name('control.products.categories.store');
		Route::get('control/products/categories/{id}', 'control\\products\\CategoriesController@edit')->name('control.products.categories.edit');
		Route::put('control/products/categories/update', 'control\\products\\CategoriesController@update')->name('control.products.categories.update');
		Route::get('control/products/categories/delete/{id}', 'control\\products\\CategoriesController@destroy')->name('control.products.categories.destroy');
		
		# products attributes
		Route::get('control/products/attributes', 'control\\products\\AttributesController@index')->name('control.products.attributes');
		Route::post('control/products/attributes/store', 'control\\products\\AttributesController@store')->name('control.products.attributes.store');
		Route::get('control/products/attributes/{id}', 'control\\products\\AttributesController@edit')->name('control.products.attributes.edit');
		Route::put('control/products/attributes/update', 'control\\products\\AttributesController@update')->name('control.products.attributes.update');
		Route::get('control/products/attributes/delete/{id}', 'control\\products\\AttributesController@destroy')->name('control.products.attributes.destroy');
		
		# products attributes options
		Route::get('control/products/attributes-options/{attribute_id}', 'control\\products\\AttributesOptionsController@index')->name('control.products.attributes.options');
		Route::post('control/products/attributes-options/store', 'control\\products\\AttributesOptionsController@store')->name('control.products.attributes.options.store');
		Route::get('control/products/attributes-options/{attribute_id}/{id}', 'control\\products\\AttributesOptionsController@edit')->name('control.products.attributes.options.edit');
		Route::put('control/products/attributes-options/update', 'control\\products\\AttributesOptionsController@update')->name('control.products.attributes.options.update');
		Route::get('control/products/attributes-options/delete/{attribute_id}/{id}', 'control\\products\\AttributesOptionsController@destroy')->name('control.products.attributes.options.destroy');

		# modal attributes options
		Route::post('control/modal/attributes-options', 'control\\modal\\ModalController@showAttributesOptions')->name('control.modal.attributes.options');
		Route::post('control/modal/attributes-options/change', 'control\\modal\\ModalController@changeAttributesOptions')->name('control.modal.attributes.options.change');
		Route::post('control/modal/attributes-options/store', 'control\\modal\\ModalController@storeAttributesOptions')->name('control.modal.attributes.options.store');
		
		# products
		Route::get('control/products', 'control\\products\\ProductsController@index')->name('control.products');
		Route::get('control/products/create', 'control\\products\\ProductsController@create')->name('control.products.create');
		Route::post('control/products/store', 'control\\products\\ProductsController@store')->name('control.products.store');
		Route::get('control/products/{id}', 'control\\products\\ProductsController@edit')->name('control.products.edit');
		Route::put('control/products/update', 'control\\products\\ProductsController@update')->name('control.products.update');
		Route::get('control/products/delete/{id}', 'control\\products\\ProductsController@destroy')->name('control.products.destroy');
		Route::post('control/products/show-variation-options', 'control\\products\\ProductsController@showVariationsOptions')->name('control.products.show.variation.options');
		
		# customers
		Route::get('control/orders', 'control\orders\OrdersController@index')->name('control.orders');
		Route::get('control/orders/{id}', 'control\orders\OrdersController@detail')->name('control.orders.detail');
		Route::get('control/orders/print/{id}', 'control\orders\OrdersController@print')->name('control.orders.print');

		# customers
		Route::get('control/customers', 'control\customers\CustomersController@index')->name('control.customers');

		# companies
		Route::get('control/companies', 'control\\companies\\CompaniesController@index')->name('control.companies');
		# companies categories
		Route::get('control/companies/categories', 'control\\companies\\CategoriesController@index')->name('control.companies.categories');
		Route::post('control/companies/categories/store', 'control\\companies\\CategoriesController@store')->name('control.companies.categories.store');
		Route::get('control/companies/categories/{id}', 'control\\companies\\CategoriesController@edit')->name('control.companies.categories.edit');
		Route::get('control/companies/categories/delete/{id}', 'control\\companies\\CategoriesController@destroy')->name('control.companies.categories.destroy');
		
	});
});


# used to display segments and products by company
Route::get('{cityuf}/{segment?}', 'web\\products\\ProductsController@segment')->name('web.products.segment');
Route::get('{cityuf}/{segment}/{company}', 'web\\products\\ProductsController@single')->name('web.products.single');
Route::post('{cityuf}/{segment}/{company}/request', 'web\\products\\ProductsController@request')->name('web.products.request');