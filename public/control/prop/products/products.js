;(function($, window, undefined)
{
	"use strict";
	
	$(document).ready(function(){

		if (document.getElementById('tableItems') !== null) 
		{
			$('#tableItems').dataTable({
				'pagingType': 'simple_numbers',
				'order': [],
				'columnDefs': [{
					'targets': [], 
					'searchable': true, 
					'orderable': false, 
					'visible': true
				}]
			});

		}


		if (document.getElementById('formProducts') !== null) 
		{
			$('select#type').change(function(){
				var value = $(this).val();
				if (value == 1) {

					$('#attributesOptions').hide();
					$('#getModal').html('');

				} else {
					$('#attributesOptions').show();					
				}
			});

			$('select#addAttribute').change(function(){

				var url = $(this).data('url');
				$.ajax({
					type: 'POST',
					url: url,
					data: {
						"_token":		'' + $('input[name="_token"]').val() + '',
						action:			$('input[name=action]').val(),
						attribute_id:	$(this).val(),
						products_id:	$('input[name=id]').val(),
					},
					success: function (dataReturn) {
						
						$('#listOptions').append(dataReturn);

					}			
				});

			});


			var formName = $('#formProducts');
			var formAction = $('#formProducts').attr('action');
			
			mask();

			// validação formulário
			formName.validate({
				ignore: '', 
				rules: {
					category:		'required',
					title: 			'required',
					description: 	'required'
				},
				messages: {
					category:		'Campo obrigatório',
					title: 			'Campo obrigatório',
					description: 	'Campo obrigatório'
				},
				submitHandler: function(form) {

					formName.find('#buttonForm').prop('disabled', true).html('<i class="entypo-cw"></i> Salvar');

					var formData = new FormData(form);
					$.ajax({
						url: 			formAction,
						type: 			'post',
						data: 			formData,
						dataType:		'json',
						mimeType: 		'multipart/form-data',
						contentType: 	false,
						cache: 			false,
						processData: 	false,
						success: function(data, textStatus, xhr) {

							formName[0].reset();
							$('#messageAlert').removeClass().addClass('alert alert-success').html(xhr.responseJSON.message).show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){
								$('span.message').removeClass().html('');
								window.location = xhr.responseJSON.redirect;
							}, 2500);

						},
						error: function(xhr, textStatus) {

							$('#messageAlert').removeClass().addClass('alert alert-danger').html(xhr.responseJSON.message).show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ 
								$('#messageAlert').removeClass().html('').hide();
								$('#buttonForm').prop('disabled', false).html('Salvar');
							}, 2500);

						}  
					
					});

				}

			});

		}

	});
	
})(jQuery, window);