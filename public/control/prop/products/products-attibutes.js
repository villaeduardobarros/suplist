;(function($, window, undefined)
{
	"use strict";
	
	$(document).ready(function(){

		if (document.getElementById('tableItems') !== null) 
		{
			$('#tableItems').dataTable({
				'pagingType': 'simple_numbers',
				'order': [],
				'columnDefs': [{
					'targets': [], 
					'searchable': true, 
					'orderable': false, 
					'visible': true
				}]
			});

		}


		if (document.getElementById('formAttibutes') !== null) 
		{
			var formName = $('#formAttibutes');
			var formAction = $('#formAttibutes').attr('action');
			
			mask();

			// validação formulário
			formName.validate({
				ignore: '', 
				rules: {
					title:		'required',
					quantity:	'required',
				},
				messages: {
					title:		'Campo obrigatório',
					quantity:	'Campo obrigatório',
				},
				submitHandler: function(form) {

					formName.find('#buttonForm').prop('disabled', true).html('<i class="entypo-cw"></i> Salvar');

					var formData = new FormData(form);
					$.ajax({
						url: 			formAction,
						type: 			'post',
						data: 			formData,
						dataType:		'json',
						mimeType: 		'multipart/form-data',
						contentType: 	false,
						cache: 			false,
						processData: 	false,
						success: function(data, textStatus, xhr) {

							formName[0].reset();
							$('#messageAlert').removeClass().addClass('alert alert-success').html(xhr.responseJSON.message).show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){
								$('span.message').removeClass().html('');
								window.location = xhr.responseJSON.redirect;
							}, 2500);

						},
						error: function(xhr, textStatus) {

							$('#messageAlert').removeClass().addClass('alert alert-danger').html(xhr.responseJSON.message).show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ 
								$('#messageAlert').removeClass().html('').hide();
								$('#buttonForm').prop('disabled', false).html('Salvar');
							}, 2500);

						}  
					
					});

				}

			});

		}


		if (document.getElementById('formAttibutesOptions') !== null) 
		{
			var formName = $('#formAttibutesOptions');
			var formAction = $('#formAttibutesOptions').attr('action');
			
			mask();

			// validação formulário
			formName.validate({
				ignore: '', 
				rules: {
					title:		'required',
				},
				messages: {
					title:		'Campo obrigatório',
				},
				submitHandler: function(form) {

					formName.find('#buttonForm').prop('disabled', true).html('<i class="entypo-cw"></i> Salvar');

					var formData = new FormData(form);
					$.ajax({
						url: 			formAction,
						type: 			'post',
						data: 			formData,
						dataType:		'json',
						mimeType: 		'multipart/form-data',
						contentType: 	false,
						cache: 			false,
						processData: 	false,
						success: function(data, textStatus, xhr) {

							formName[0].reset();
							$('#messageAlert').removeClass().addClass('alert alert-success').html(xhr.responseJSON.message).show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){
								$('span.message').removeClass().html('');
								window.location = xhr.responseJSON.redirect;
							}, 2500);

						},
						error: function(xhr, textStatus) {

							$('#messageAlert').removeClass().addClass('alert alert-danger').html(xhr.responseJSON.message).show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ 
								$('#messageAlert').removeClass().html('').hide();
								$('#buttonForm').prop('disabled', false).html('Salvar');
							}, 2500);

						}  
					
					});

				}

			});

		}

	});
	
})(jQuery, window);