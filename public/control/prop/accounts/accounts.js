;(function($, window, undefined)
{
	"use strict";
	
	$(document).ready(function(){

		if (document.getElementById('formAccount') !== null) 
		{
			$('select#delivery').change(function(){
				if ($(this).val() == 1) {
					$('div#settings_delivery').show();
				} else {
					$('div#settings_delivery').hide();
					$('select#settings_fee').val('');
					$('input#value_delivery').val('');
				}
			});

			$('select#settings_fee').change(function(){
				if ($(this).val() == 1) {
					$('div#onetime_fee').show();
					$('#dynamic_rate').hide();
				}
				
				if ($(this).val() == 2) {
					$('div#onetime_fee').hide();
					$('div#onetime_fee input#value_delivery').val('');
					$('#dynamic_rate').show();
				}
			});

			var formName = $('#formAccount');
			var formAction = $('#formAccount').attr('action');
			
			mask();

			// form validation
			formName.validate({
				ignore: '',
				rules: {
					email: {
						required:	true,
						email:		true
					},
					name:			'required'
				},
				messages: {
					email: {
						required:	'O campo obrigatório',
						email:		'E-mail informado inválido',
					},
					name:			'O campo obrigatório'
				},
				submitHandler: function(form) {

					formName.find('#buttonForm').prop('disabled', true).html('<i class="entypo-cw"></i> Salvar');

					var formData = new FormData(form);
					$.ajax({
						url: 			formAction,
						type: 			'post',
						data: 			formData,
						dataType:		'json',
						mimeType: 		'multipart/form-data',
						contentType: 	false,
						cache: 			false,
						processData: 	false,
						success: function(data, textStatus, xhr) {

							formName[0].reset();
							$('#messageAlert').removeClass().addClass('alert alert-success').html(xhr.responseJSON.message).show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){
								$('span.message').removeClass().html('');
								window.location = xhr.responseJSON.redirect;
							}, 2500);

						},
						error: function(xhr, textStatus) {

							$('#messageAlert').removeClass().addClass('alert alert-danger').html(xhr.responseJSON.message).show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ 
								$('#messageAlert').removeClass().html('').hide();
								$('#buttonForm').prop('disabled', false).html('Salvar');
							}, 2500);

						}
					
					});

				}

			});

		}

	});
	
})(jQuery, window);


var addDynamicRate = function()
{
	$('#getModal').load('control/modal/dynamic-rate', {
		_token: $('meta[name=csrf-token]').attr('content'),
	}, function(data) {

        $('#dynamicRateModal').modal({
            backdrop: 'static',
            keyboard: false, 
            show:     true
		});
		
        $('#dynamicRateModal').on('shown.bs.modal', function(){

			listDynamicRate();
			
			mask();

			submitFormDinamicRate();

		});
		
        $('#dynamicRateModal').on('hidden.bs.modal', function (){
            $('#getModal').html('');
		});
		
	});
}


var submitFormDinamicRate = function()
{
	$('#formDinamicRate').validate({
		ignore: '', 
		rules: {
			start_distance:	'required',
			end_distance:	'required',
			value_fee:		'required',
		},
		messages: {
			start_distance:	'Campo obrigatório',
			end_distance:	'Campo obrigatório',
			value_fee:		'Campo obrigatório',
		},
		submitHandler: function(form) {

			var formData = new FormData(form);
			$.ajax({
				url:			'control/modal/dynamic-rate/store',
				type:			'post',
				data:			formData,
				dataType:		'json',
				mimeType:		'multipart/form-data',
				contentType:	false,
				cache:			false,
				processData:	false,
				success: function(data, textStatus, xhr) {

					$('#formDinamicRate')[0].reset();
					$('#formDinamicRate #messageAlert').removeClass().addClass('alert alert-success').html(xhr.responseJSON.message).show();
					setTimeout(function(){
						$('#formDinamicRate #messageAlert').removeClass().html('');
					}, 2500);

					listDynamicRate();

				},
				error: function(xhr, textStatus) {

					$('#formDinamicRate #messageAlert').removeClass().addClass('alert alert-danger').html(xhr.responseJSON.message).show();
					setTimeout(function(){ 
						$('#formDinamicRate #messageAlert').removeClass().html('').hide();
					}, 4000);

				}  
			
			});

		}

	});
}


var listDynamicRate = function()
{
	$('.list-distances').load('control/modal/dynamic-rate/list', {
		_token: $('meta[name=csrf-token]').attr('content'),
	});
}


var deleteDynamicRate = function(idRate)
{
	if (confirm("Deseja realmente excluir esta taxa?") == true) {

		$.post('control/modal/dynamic-rate/delete', {
			_token: $('meta[name=csrf-token]').attr('content'),
			id: idRate
		}, function(dataReturn) {

			$('#formDinamicRate #messageAlert').removeClass().addClass('alert alert-' + dataReturn.message_action + '').html(dataReturn.message).show();
			setTimeout(function(){
				$('#formDinamicRate #messageAlert').removeClass().html('');
			}, 2500);

			listDynamicRate();

		});
			
	} else { return false; }
}