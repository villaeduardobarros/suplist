// *******************************
// **** modal - link attribute
var addAttributesOptions = function(products_id)
{
	$('#getModal').load('control/modal/attributes-options', {
		_token: $('meta[name=csrf-token]').attr('content'),
		products_id: products_id
	}, function(data) {

        $('#attributesModal').modal({
            backdrop: 'static',
            keyboard: false, 
            show:     true
		});
		
        $('#attributesModal').on('shown.bs.modal', function(){
			
			$('select#attribute_id').change(function(){
				$('#imgPreloader').show();
				showAttributesOptions($(this).val(), products_id);
			});

			submitFormAttributesOptions();

		});
		
        $('#attributesModal').on('hidden.bs.modal', function (){
            $('#getModal').html('');
		});
		
	});
}


var submitFormAttributesOptions = function()
{
	$('#formAttributesOptions').validate({
		ignore: '', 
		rules: {
			attribute_id:	'required',
			'option[]':	{
				required: true
			}
		},
		messages: {
			attribute_id:	'Campo atributo é obrigatório',
			'option[]':	'Selecione pelo menos uma opção'
		},
		submitHandler: function(form) {

			var formData = new FormData(form);
			$.ajax({
				url:			'control/modal/attributes-options/store',
				type:			'post',
				data:			formData,
				dataType:		'json',
				mimeType:		'multipart/form-data',
				contentType:	false,
				cache:			false,
				processData:	false,
				success: function(data, textStatus, xhr) {

					$('#formAttributesOptions')[0].reset();
					$('#formAttributesOptions #messageAlert').removeClass().addClass('alert alert-success').html(xhr.responseJSON.message).show();
					setTimeout(function(){
						$('#formAttributesOptions #messageAlert').removeClass().html('');
						$('#formAttributesOptions .list-options').html('');
					}, 2500);

				},
				error: function(xhr, textStatus) {

					$('#formAttributesOptions #messageAlert').removeClass().addClass('alert alert-danger').html(xhr.responseJSON.message).show();
					setTimeout(function(){ 
						$('#formAttributesOptions #messageAlert').removeClass().html('').hide();
					}, 2500);

				}  
			
			});

		}

	});
}


// *******************************
// **** modal - displays options for the selected attribute
var showAttributesOptions = function(attributes_id, products_id)
{
	$('.list-options').load('control/modal/attributes-options/change', {
		_token: $('meta[name=csrf-token]').attr('content'),
		attributes_id: attributes_id,
		products_id: products_id
	});
} 