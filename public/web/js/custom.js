(function ($, window, undefined) {
	"use strict";

	$(document).ready(function () {

		// $('.checkZipcode').on('click', function(event){
		// 	event.preventDefault();

		// 	var href = $(this).attr('href');
			
		// 	$.ajax({
		//		crossDomain: true
		// 		type: 'POST',
		// 		dataType: 'json',
		// 		url: baseUrl + '/cep/check',
		// 		data: {
		// 			_token: $('meta[name=csrf-token]').attr('content'),
		// 			href: href
		// 		},
		// 		success: function (data, textStatus, xhr) {

		// 			window.location = data.redirect;

		// 		},
		// 		error: function (xhr, textStatus) {

		// 			$('#getModal').load('modal/zipcode', {
		// 				_token: $('meta[name=csrf-token]').attr('content')
		// 			}, function(data) {
				
		// 				$('#zipcodemodal').modal({
		// 					backdrop: 'static',
		// 					keyboard: false,
		// 					show:     true
		// 				});
				
		// 				$('#zipcodemodal').on('shown.bs.modal', function(){

		// 					mask();

		// 					var formName = $('#formZipcodeModal');

		// 					if ($('#zipcode_modal').val() == '') {

		// 						formName.find('p.message').css('color', '#ff5d5d').html('O campo CEP é obrigatório!');
		// 						setTimeout(function () { formName.find('p.message').html(''); }, 3500);
			
		// 					} else {
			
		// 						var zipcode = $('#zip_code').val().replace('-', '');
		// 						zipcode(zipcode, formName);
							
		// 					}
				
		// 				});
				
		// 				$('#zipcodemodal').on('hidden.bs.modal', function (){
		// 					$('#getModal').html('');
		// 				});
				
		// 			});

		// 		}
		// 	});

		// });

		// zipcode [banner]
		if (document.getElementById('formZipCode') !== null) {

			mask();

			$('#formZipCode').on('keyup keypress', function(e) {
				var keyCode = e.keyCode || e.which;
				if (keyCode === 13) {
					$('button#btnZipCode').click();
					return false;
				}
			});

			$('button#btnZipCode').click(function(){

				if ($('#zip_code').val() == '') {

					$('#formZipCode p.message').css('color', '#ff5d5d').html('O campo CEP é obrigatório!');
					setTimeout(function () { $('#formZipCode p.message').html(''); }, 3500);

				} else {

					var zipcode = $('#zip_code').val().replace('-', '');

					$.ajax({
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded'
						},
						crossDomain: true,
						type: 'GET',
						dataType: 'json',
						url: 'https://viacep.com.br/ws/' + zipcode + '/json/?callback=?',
						success: function (dataReturnViaCep, textStatus, xhr) {
				
							var action = $('#formZipCode').attr('action');
							$.ajax({
								headers: {
									'Content-Type': 'application/x-www-form-urlencoded'
								},
								crossDomain: true,
								type: 'POST',
								dataType: 'json',
								url: action,
								data: {
									_token: $('meta[name=csrf-token]').attr('content'),
									city: dataReturnViaCep.localidade,
									zipcode: $('#zip_code').val()
								},
								success: function (dataReturn, textStatus, xhr) {
				
									window.location = dataReturn.redirect;
				
								},
								error: function (xhr, textStatus) {
				
									$('#formZipCode p.message').css('color', '#ff5d5d').html(xhr.responseJSON.message);
									setTimeout(function () { $('#formZipCode p.message').html(''); }, 3500);
				
								}
							});
				
						}
					});
					// zipcode(zipcode, formName);
					
				}


			});

		}

		// register newsletter [footer]
		if (document.getElementById('formNewsletter') !== null) {
			var formName = $('#formNewsletter');

			// form validation
			formName.validate({
				ignore: '',
				rules: {
					email_news: {
						required: true,
						email: true
					},
				},
				messages: {
					email_news: {
						required: 'Campo obrigatório',
						email: 'E-mail informado inválido'
					},
				},
				showErrors: function (errorMap, errorList) {
					formName.find('span.message').empty();
					if (errorList.length) {
						formName.find('span.message').css('color', '#ff5d5d').html(errorList[0].message);
					}
				},
				submitHandler: function (form) {

					formName.find('span.message').css('color', '#ff5d5d').html('<i class="fas fa-spinner fa-spin"></i>');

					var action = formName.attr('action');
					$.ajax({
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded'
						},
						crossDomain: true,
						type: 'POST',
						dataType: 'json',
						url: action,
						data: formName.serialize(),
						success: function (data, textStatus, xhr) {

							formName[0].reset();
							formName.find('span.message').css('color', '#208036').html(xhr.responseJSON.message);
							setTimeout(function () { formName.find('span.message').html(''); }, 5000);

						},
						error: function (xhr, textStatus) {

							formName.find('span.message').css('color', '#ff5d5d').html(xhr.responseJSON.message);
							setTimeout(function () { formName.find('span.message').html(''); }, 3500);

						}
					});

				}
			});
		}


		// pre-registration of companies [drafts]
		if (document.getElementById('formRegisterDraft') !== null) {
			var formName = $('#formRegisterDraft');

			mask();

			// form validation
			formName.validate({
				ignore: '',
				rules: {
					name: 'required',
					email: {
						required: true,
						email: true
					},
					phone: 'required',
				},
				messages: {
					name: 'O campo nome é obrigatório',
					email: {
						required: 'O campo e-mail é obrigatório',
						email: 'E-mail informado inválido',
					},
					phone: 'O campo telefone é obrigatório',
				},
				// showErrors: function (errorMap, errorList) {
				// 	formName.find('p.message').empty();
				// 	if (errorList.length) {
				// 		formName.find('p.message').css('color', '#ff5d5d').html(errorList[0].message);
				// 	}
				// },
				submitHandler: function (form) {

					formName.find('button').prop('disabled', true).html('<i class="fas fa-spinner fa-spin"></i> CADASTRAR');

					var action = formName.attr('action');
					$.ajax({
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded'
						},
						crossDomain: true,
						type: 'POST',
						dataType: 'json',
						url: action,
						data: formName.serialize(),
						success: function (data, textStatus, xhr) {

							formName[0].reset();
							formName.find('p.message').css('color', '#208036').html(xhr.responseJSON.message);
							// setTimeout(function () {
							// 	formName.find('p.message').html('');
							// 	formName.find('button').prop('disabled', false).html('CADASTRAR');
							// }, 3500);

						},
						error: function (xhr, textStatus) {

							formName.find('p.message').css('color', '#ff5d5d').html(xhr.responseJSON.message);
							// setTimeout(function () {
							// 	formName.find('p.message').html('');
							// 	formName.find('button').prop('disabled', false).html('CADASTRAR');
							// }, 3500);

						}
					});

				}
			});
		}


		// login form [draft] for the company to finalize its registration
		if (document.getElementById('formLoginDraft') !== null) {
			var formName = $('#formLoginDraft');

			// form validation
			formName.validate({
				ignore: '',
				rules: {
					email: {
						required: true,
						email: true
					},
					password: 'required',
				},
				messages: {
					email: {
						required: 'O campo e-mail é obrigatório',
						email: 'E-mail informado inválido',
					},
					password: 'O campo senha é obrigatório',
				},
				// showErrors: function (errorMap, errorList) {
				// 	formName.find('p.message').empty();
				// 	if (errorList.length) {
				// 		formName.find('p.message').css('color', '#ff5d5d').html(errorList[0].message);
				// 	}
				// },
				submitHandler: function (form) {

					formName.find('button').prop('disabled', true).html('<i class="fas fa-spinner fa-spin"></i> ENTRAR');

					var action = formName.attr('action');
					$.ajax({
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded'
						},
						crossDomain: true,
						type: 'POST',
						dataType: 'json',
						url: action,
						data: formName.serialize(),
						success: function (data, textStatus, xhr) {

							formName[0].reset();
							formName.find('p.message').css('color', '#208036').html(xhr.responseJSON.message);
							setTimeout(function () {
								formName.find('p.message').html('');
								window.location = xhr.responseJSON.redirect;
							}, 1800);

						},
						error: function (xhr, textStatus) {

							console.log(xhr);
							console.log(textStatus);

							formName.find('p.message').css('color', '#ff5d5d').html(xhr.responseJSON.message);
							setTimeout(function () {
								formName.find('p.message').html('');
								formName.find('button').prop('disabled', false).html('ENTRAR');
							}, 3500);

						}
					});

				}
			});
		}


		// form for the company to finalize its registration
		if (document.getElementById('formCompleteDraft') !== null) {
			// segment
			$('select#segment').change(function () {
				if ($(this).val() == 'other') {
					$('input#other').show();
				} else {
					$('input#other').val('').hide();
				}
			});

			// kind of person
			$('select#type_person').change(function () {
				$('#companies, #individuals').hide();
				$('#companies input, #individuals input').val('');
				if ($(this).val() == 1) {
					$('#individuals').show();
					$('#companies').hide();
				} else {
					$('#companies').show();
					$('#individuals').hide();
				}
			});

			// file
			$('#btnSelectFile').click(function () {
				$('input#image').click();
			});

			$('input#image').change(function (e) {
				$('#fileSelected').attr('placeholder', $(this).val());
			});

			var formName = $('#formCompleteDraft');

			mask();

			// form validation
			formName.validate({
				ignore: '',
				rules: {
					email: {
						required:	true,
						email:		true
					},
					password:		'required',
					name:			'required',
					phone:			'required',
					segment:		'required',
					other: {
						required: function () {
							if ($('#segment').val() == 'other') {
								return true;
							} else { return false; }
						}
					},
					type_person:	'required',
					cell_phone:		'required',
					fantasy_name:	'required',
					reg_of_individuals: {
						required: function () {
							if ($('#type_person').val() == 1) {
								return true;
							} else { return false; }
						}
					},
					company_name: {
						required: function () {
							if ($('#type_person').val() == 2) {
								return true;
							} else { return false; }
						}
					},
					reg_legal_entity: {
						required: function () {
							if ($('#type_person').val() == 2) {
								return true;
							} else { return false; }
						}
					},
					title:			'required',
					zip_code:		'required',
					state:			'required',
					city:			'required',
					address:		'required',
					number:			'required',
					neighborhood:	'required',
					plan:			'required',
				},
				messages: {
					email: {
						required:	'O campo obrigatório',
						email:		'E-mail informado inválido',
					},
					password:			'O campo obrigatório',
					name:				'O campo obrigatório',
					phone:				'O campo obrigatório',
					segment:			'O campo obrigatório',
					other:				'O campo obrigatório',
					type_person:		'O campo obrigatório',
					cell_phone:			'O campo obrigatório',
					fantasy_name:		'O campo obrigatório',
					reg_of_individuals:	'O campo obrigatório',
					company_name:		'O campo obrigatório',
					reg_legal_entity:	'O campo obrigatório',
					title:				'O campo obrigatório',
					zip_code:			'O campo obrigatório',
					state:				'O campo obrigatório',
					city:				'O campo obrigatório',
					address:			'O campo obrigatório',
					number:				'O campo obrigatório',
					neighborhood:		'O campo obrigatório',
					plan:				'O campo obrigatório',
				},
				// showErrors: function (errorMap, errorList) {
				// 	formName.find('p.message').empty();
				// 	if (errorList.length) {
				// 		formName.find('p.message').css('color', '#ff5d5d').html(errorList[0].message);
				// 	}
				// },
				submitHandler: function(form) {

					formName.find('button').prop('disabled', true).html('<i class="fas fa-spinner fa-spin"></i> SALVAR');

					var action = formName.attr('action');

					var formData = new FormData(form);
					$.ajax({
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded'
						},
						crossDomain: 	true,
						url: 			action,
						type: 			'post',
						data: 			formData,
						dataType:		'json',
						mimeType: 		'multipart/form-data',
						contentType: 	false,
						cache: 			false,
						processData: 	false,
						success: function (data, textStatus, xhr) {

							formName[0].reset();
							$('html, body').animate({ scrollTop:formName.find('p.message').offset().top }, 1800);
							formName.find('p.message').css('color', '#208036').html(xhr.responseJSON.message);
							setTimeout(function () {
								formName.find('p.message').html('');
								window.location = xhr.responseJSON.redirect;
							}, 3500);

						},
						error: function (xhr, textStatus) {

							$('html, body').animate({ scrollTop:formName.find('p.message').offset().top }, 1800);
							formName.find('p.message').css('color', '#ff5d5d').html(xhr.responseJSON.message);
							setTimeout(function () {
								formName.find('p.message').html('');
								formName.find('button').prop('disabled', false).html('SALVAR');
							}, 3500);

						}

					});

				}
			});
		}


		// sac form
		if (document.getElementById('formRegisterSac') !== null) {
			var formName = $('#formRegisterSac');

			mask();

			// form validation
			formName.validate({
				ignore: '',
				rules: {
					email: {
						required: true,
						email: true
					},
					name: 'required',
					phone: 'required',
					subject: 'required',
					message: 'required',
				},
				messages: {
					email: {
						required: 'O campo e-mail é obrigatório',
						email: 'E-mail informado inválido',
					},
					name: 'O campo nome do responsável é obrigatório',
					phone: 'O campo telefone é obrigatório',
					subject: 'O campo assunto é obrigatório',
					message: 'O campo mensagem é obrigatório',
				},
				// showErrors: function (errorMap, errorList) {
				// 	formName.find('p.message').empty();
				// 	if (errorList.length) {
				// 		formName.find('p.message').css('color', '#ff5d5d').html(errorList[0].message);
				// 	}
				// },
				submitHandler: function (form) {

					formName.find('button').prop('disabled', true).html('<i class="fas fa-spinner fa-spin"></i> ENVIAR');

					var action = formName.attr('action');
					$.ajax({
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded'
						},
						crossDomain: true,
						type: 'POST',
						dataType: 'json',
						url: action,
						data: formName.serialize(),
						success: function (data, textStatus, xhr) {

							formName[0].reset();
							formName.find('p.message').css('color', '#208036').html(xhr.responseJSON.message);
							setTimeout(function () {
								formName.find('p.message').html('');
								window.location = xhr.responseJSON.redirect;
							}, 3500);

						},
						error: function (xhr, textStatus) {

							formName.find('p.message').css('color', '#ff5d5d').html(xhr.responseJSON.message);
							setTimeout(function () {
								formName.find('p.message').html('');
								formName.find('button').prop('disabled', false).html('ENVIAR');
							}, 3500);

						}
					});

				}
			});
		}


		// // sac form
		// if (document.getElementById('singleProd') !== null) {

		// 	$('.view-details-prod').on('click', function(){

		// 		var product_id = $(this).data('id-prod');
		// 		var openClose = $(this).data('open-close');

		// 		if (Boolean(openClose) == true) {

		// 			viewDetails(product_id);

		// 		} else { alert('Este estabelecimento encontra-se fechado no momento!'); }

		// 	});
		// }

	});

})(jQuery, window);


function zipcode(zipcode, formName)
{
	$.ajax({
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		crossDomain: true,
		type: 'GET',
		dataType: 'json',
		url: 'https://viacep.com.br/ws/' + zipcode + '/json/?callback=?',
		success: function (data, textStatus, xhr) {

			var action = formName.attr('action');
			$.ajax({
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				crossDomain: true,
				type: 'POST',
				dataType: 'json',
				url: action,
				data: {
					_token: $('meta[name=csrf-token]').attr('content'),
					city: data.localidade,
					zipcode: zipcode
				},
				success: function (data, textStatus, xhr) {

					window.location = data.redirect;

				},
				error: function (xhr, textStatus) {

					formName.find('p.message').css('color', '#ff5d5d').html(xhr.responseJSON.message);
					setTimeout(function () { formName.find('p.message').html(''); }, 3500);

				}
			});

		}
	});
}


var viewDetails = function(product_id)
{
	$.ajax({
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		crossDomain: true,
		type: 'POST',
		url: baseUrl + '/modal/product-detail',
		data: {
			_token: $('meta[name=csrf-token]').attr('content'),
			product_id: product_id
		},
		success: function (data, textStatus, xhr) {

			$('#getModal').html(data);

			$('#productmodal').modal({
				backdrop: 'static',
				keyboard: false,
				show:     true
			});
	
			$('#productmodal').on('shown.bs.modal', function(){
	
				$('#imgPreloader').show();
	
				// with the open modal, if any, displays the variations
				$('.list-options').load(baseUrl + '/modal/variations-detail', {
					_token: $('meta[name=csrf-token]').attr('content'),
					product_id: product_id
				});
	
			});
	
			$('#productmodal').on('hidden.bs.modal', function (){
				$('#getModal').html('');
			});

		}
	});
}


// add product in shopping cart
function addShoppingCart(product_id)
{
	$.ajax({
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		crossDomain: true,
		type: 'POST',
		dataType: 'json',
		url: baseUrl + '/shopping-cart/store',
		data: $('#formAttributesOptions').serialize(), 
		success: function (data, textStatus, xhr) {
			
			$('#formAttributesOptions')[0].reset();
			$('#cartresume .total-count').html(xhr.responseJSON.count);
			$('#formAttributesOptions p.message')
				.css('margin', '15px 0 0')
				.css('text-align', 'center')
				.css('padding', '8px')
				.css('color', '#208036')
				.css('background-color', '#d4ffc9')
				.html(xhr.responseJSON.message);
			setTimeout(function () {
				$('#formAttributesOptions p.message').removeAttr('style').html('');
			}, 3500);

		},
		error: function (xhr, textStatus) {

			console.log(textStatus);
			console.log(xhr);

			$('#formAttributesOptions p.message')
				.css('margin', '15px 0 0')
				.css('text-align', 'center')
				.css('padding', '8px')
				.css('color', '#ff5d5d')
				.css('background-color', '#ffd5d5')
				.html(xhr.responseJSON.message);
			setTimeout(function () {
				$('#formAttributesOptions p.message').removeAttr('style').html('');
				$('#formAttributesOptions button').prop('disabled', false).html('Adicionar ao carrinho');
			}, 3500);

		}
	});
}


// shopping cart displays
function showShoppingCart()
{
	$('#getModal').load(baseUrl + '/modal/shopping-cart-detail', {
		_token: $('meta[name=csrf-token]').attr('content'),
	}, function(data) {

        $('#cart').modal({
            backdrop: 'static',
            keyboard: false,
            show:     true
		});

        $('#cart').on('shown.bs.modal', function(){

			$('.list-product-cart').load(baseUrl + '/shopping-cart/show', {
				_token: $('meta[name=csrf-token]').attr('content')
			});
			
			$('.form-cart').load(baseUrl + '/shopping-cart/show-form', {
				_token: $('meta[name=csrf-token]').attr('content')
			}, function(){

				mask();

				$('#formRequest #receive').on('change', function(){

					var valueSubTotal = $('#formRequest input[name="value_subtotal"]').val();

					if ($(this).val() == 1) {

						$('#formRequest #zip_code').on('blur', function(){
					
							var zipCode = $(this).val();
							
							$.post(baseUrl + '/shopping-cart/check-zipcode', {
								_token: $('meta[name=csrf-token]').attr('content'),
								zip_code: zipCode, 
								value_subtotal: valueSubTotal,
								company_id: $('#formRequest input[name="companies_id"]').val(),
								company_latitude: $('#formRequest input[name="companies_lat"]').val(),
								company_longitude: $('#formRequest input[name="companies_lon"]').val()
							}, function(dataReturn){
		
								var items = dataReturn.split('|');
		
								$('span.value-delivery').html('R$ ' + maskValue(parseFloat(items[0]).toFixed(2)));
								$('form#formRequest input[name="value_delivery"]').val(items[0]);
		
								$('span.value-total').html('R$ ' + maskValue(parseFloat(items[1]).toFixed(2)));
								$('form#formRequest input[name="value_total"]').val(items[1]);
		
							});
		
						});

					} else {

						$('form#formRequest input[name="value_delivery"]').val('');
						$('form#formRequest input[name="zip_code"]').val('');

						$('span.value-delivery').html('R$ 0,00');
						$('span.value-total').html('R$ ' + maskValue(parseFloat(valueSubTotal).toFixed(2)));
						$('form#formRequest input[name="value_total"]').val(valueSubTotal);


					}

				});

				$('#sendRequest').click(function(){
					formRequest();
					$('#formRequest').submit();
				});

			});

		});

        $('#cart').on('hidden.bs.modal', function (){
            $('#getModal').html('');
		});

	});
	
}


function maskValue(value)
{
    value = value.toString().replace(/\D/g,"");
    value = value.toString().replace(/(\d)(\d{8})$/,"$1.$2");
    value = value.toString().replace(/(\d)(\d{5})$/,"$1.$2");
    value = value.toString().replace(/(\d)(\d{2})$/,"$1,$2");
    return value;
}


function changeQuantity(rowId, action, quantity)
{
	$.ajax({
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		type: 'POST',
		dataType: 'json',
		url: baseUrl + '/shopping-cart/update',
		data: {
			_token: $('meta[name=csrf-token]').attr('content'),
			rowId: rowId,
			action: action,
			quantity: quantity
		}, 
		success: function (data, textStatus, xhr) {
	
			$('.list-product-cart').load(baseUrl + '/shopping-cart/show', {
				_token: $('meta[name=csrf-token]').attr('content')
			});

		},
		error: function (xhr, textStatus) {

			$('.list-product-cart').load(baseUrl + '/shopping-cart/show', {
				_token: $('meta[name=csrf-token]').attr('content')
			});

			$('.form-cart').remove();
			$('#footerCart').hide();

		}
	});

	
}


function removeItem(rowId)
{
	$.ajax({
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		type: 'POST',
		dataType: 'json',
		url: baseUrl + '/shopping-cart/delete',
		data: {
			_token: $('meta[name=csrf-token]').attr('content'),
			rowId: rowId
		}, 
		success: function (data, textStatus, xhr) {

			$('.list-product-cart').load(baseUrl + '/shopping-cart/show', {
				_token: $('meta[name=csrf-token]').attr('content')
			});

		},
		error: function (xhr, textStatus) {

			$('.list-product-cart').load(baseUrl + '/shopping-cart/show', {
				_token: $('meta[name=csrf-token]').attr('content')
			});

			$('.form-cart').remove();
			$('#footerCart').hide();

		}
	});
}


function formRequest()
{
	var formName = $('#formRequest');

	// form validation
	formName.validate({
		ignore: '',
		rules: {
			name:			'required',
			email: {
				required:	true,
				email:		true
			},
			cell_phone:		'required',
			payment_form:	'required',
			receive:		'required',
			zip_code: {
				required: function () {
					if ($('#receive').val() == 1) {
						return true;
					} else { return false; }
				}
			},
			state: {
				required: function () {
					if ($('#receive').val() == 1) {
						return true;
					} else { return false; }
				}
			},
			city: {
				required: function () {
					if ($('#receive').val() == 1) {
						return true;
					} else { return false; }
				}
			},
			address: {
				required: function () {
					if ($('#receive').val() == 1) {
						return true;
					} else { return false; }
				}
			},
			number: {
				required: function () {
					if ($('#receive').val() == 1) {
						return true;
					} else { return false; }
				}
			},
			neighborhood: {
				required: function () {
					if ($('#receive').val() == 1) {
						return true;
					} else { return false; }
				}
			},
		},
		messages: {
			name:			'O campo nome é obrigatório',
			email: {
				required:	'O campo e-mail é obrigatório',
				email:		'E-mail informado inválido',
			},
			cell_phone:		'O campo celuar é obrigatório',
			payment_form:	'Selecione uma forma de pagamento',
			receive:		'Seecione uma forma de entrega',
			zip_code:		'O campo CEP é obrigatório',
			state:			'O campo estado é obrigatório',
			city:			'O campo cidade é obrigatório',
			address:		'O campo endereço é obrigatório',
			number:			'O campo número é obrigatório',
			neighborhood:	'O campo bairro é obrigatório',
		},
		// showErrors: function (errorMap, errorList) {
		// 	formName.find('p.message').empty();
		// 	if (errorList.length) {
		// 		formName.find('p.message').css('color', '#ff5d5d').html(errorList[0].message);
		// 	}
		// },
		submitHandler: function (form) {

			$('#sendRequest').prop('disabled', true).html('<i class="fas fa-spinner fa-spin"></i> ENVIAR');

			var action = formName.attr('action');

			$.ajax({
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				type: 'POST',
				dataType: 'json',
				url: action,
				data: formName.serialize(),
				success: function (data, textStatus, xhr) {

					$('#sendRequest').prop('disabled', false).html('ENVIAR');
					
					formName.find('p.message')
						.css('margin', '15px 0 0')
						.css('text-align', 'center')
						.css('padding', '8px')
						.css('color', '#208036')
						.css('background-color', '#d4ffc9')
						.html(xhr.responseJSON.message);

					setTimeout(function () {
						formName[0].reset();
						formName.find('p.message').html('');
						$('#sendRequest').prop('disabled', false).html('ENVIAR');
					}, 3500);

					window.open("https://api.whatsapp.com/send?phone=55" + xhr.responseJSON.whatsapp + "&text=" + xhr.responseJSON.content);

				},
				error: function (xhr, textStatus) {

					formName.find('p.message')
						.css('margin', '15px 0 0')
						.css('text-align', 'center')
						.css('padding', '8px')
						.css('color', '#ff5d5d')
						.css('background-color', '#ffd5d5')
						.html(xhr.responseJSON.message);

					setTimeout(function () {
						formName.find('p.message').html('');
						$('#sendRequest').prop('disabled', false).html('ENVIAR');
					}, 3500);

				}
			});

		}
	});
}


// search cities by selected state
function selectCities(state_id)
{
	$('select#city').empty();
	$('select#city').html('<option value="">Carregando...</option>');

	$.ajax({
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		type: 'POST',
		dataType: 'json',
		url: baseUrl + '/cidades',
		data: {
			_token: $('meta[name=csrf-token]').attr('content'),
			state_id: state_id
		},
		success: function (dataReturn, textStatus, xhr) {

			$('select#city').empty();

			var html = '<option value="">Selecione a cidade</option>';

			for (var index in dataReturn) {

				html += '<option value="' + dataReturn[index]['id'] + '">' + dataReturn[index]['title'] + '</option>';

			}

			$('select#city').append(html);

		},
		error: function (xhr, textStatus) {

			$('select#city').empty();
			$('select#city').append('<option value="">Nenhuma cidade encontrada</option>');

		}
	});
}


// when selecting the payment method
function selectPaymentsForm(form)
{
	if (form == 1) {

		$('#changeOfMoney').show();

	} else { $('#changeOfMoney').hide(); }
}


// when selecting the delivery method
function selectDeliveryForm(form)
{
	if (form == 1) {

		$('#changeDeliveryForm').show();

	} else { $('#changeDeliveryForm').hide(); }
}


// masks of the input fields [used in every project]
function mask()
{
	// hours
	if ($('input').hasClass('hours')) {
		$('.hours').mask('99:99');
	}

	// percentage
	if ($('input').hasClass('value')) {
		$('.value').maskMoney({
			thousands: '.',
			decimal: ',',
			affixesStay: true,
			allowZero: true
		});
	}

	// cpf
	if ($('input').hasClass('cpf')) {
		$('.cpf').mask('999.999.999-99');
	}

	// cnpj
	if ($('input').hasClass('cnpj')) {
		$('.cnpj').mask('99.999.999/9999-99');
	}

	// zip code
	if ($('input').hasClass('zipcode')) {
		$('.zipcode').mask('99999-999');
	}

	// phone
	if ($('input').hasClass('phone')) {
		var SPMaskBehavior = function (val) {
			return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
		},
			spOptions = {
				onKeyPress: function (val, e, field, options) {
					field.mask(SPMaskBehavior.apply({}, arguments), options);
				}
			};
		$('.phone').mask(SPMaskBehavior, spOptions);
	}
}


// only accepts number
function checkNumber(e)
{
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		return false;
	}
}
