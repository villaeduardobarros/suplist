/*

$('.openmodal').click(function(){
    var name = $(this).data('name');
    var price = $(this).data('price');
    $('#productmodal .modal-title').html(name);
    $('#productmodal .modal-body img.photo').attr('src',$(this).data('image'));
    $('#productmodal .modal-body img.photo').attr('alt',name);
    $('#productmodal .modal-body p.description').html( $(this).data('content'));

    $('#productmodal .add-to-cart').data('name',  name);
    $('#productmodal .add-to-cart').data('price', price);
    $('#productmodal input[name=qtd]').val(1);
    $('#productmodal').modal();
  });
*/

  $('a[href*="#"]').click(function () {
    var target = $( $(this).attr('href') );
    var body = $("html, body");
    body.stop().animate({ scrollTop: target.offset().top - 80  }, 500, "swing", function () {

    });
  });
  // ************************************************
  // Shopping Cart API
  // ************************************************

  var shoppingCart = (function () {
    // =============================
    // Private methods and propeties
    // =============================
    cart = [];

    // Constructor
    function Item(name, price, count) {
      this.name = name;
      this.price = price;
      this.count = count;
    }

    // Save cart
    function saveCart() {
      sessionStorage.setItem("shoppingCart", JSON.stringify(cart));
    }

    // Load cart
    function loadCart() {
      cart = JSON.parse(sessionStorage.getItem("shoppingCart"));
    }
    if (sessionStorage.getItem("shoppingCart") != null) {
      loadCart();
    }

    // =============================
    // Public methods and propeties
    // =============================
    var obj = {};

    // Add to cart
    obj.addItemToCart = function (name, price, count) {
      for (var item in cart) {
        if (cart[item].name === name) {
          cart[item].count++;
          saveCart();
          return;
        }
      }
      var item = new Item(name, price, count);
      cart.push(item);
      saveCart();
    };
    // Set count from item
    obj.setCountForItem = function (name, count) {
      for (var i in cart) {
        if (cart[i].name === name) {
          cart[i].count = count;
          break;
        }
      }
    };
    // Remove item from cart
    obj.removeItemFromCart = function (name) {
      for (var item in cart) {
        if (cart[item].name === name) {
          cart[item].count--;
          if (cart[item].count === 0) {
            cart.splice(item, 1);
          }
          break;
        }
      }
      saveCart();
    };

    // Remove all items from cart
    obj.removeItemFromCartAll = function (name) {
      for (var item in cart) {
        if (cart[item].name === name) {
          cart.splice(item, 1);
          break;
        }
      }
      saveCart();
    };

    // Clear cart
    obj.clearCart = function () {
      cart = [];
      saveCart();
    };

    // Count cart
    obj.totalCount = function () {
      var totalCount = 0;
      for (var item in cart) {
        totalCount += cart[item].count;
      }
      return totalCount;
    };

    // Total cart
    obj.totalCart = function () {
      var totalCart = 0;
      for (var item in cart) {
        totalCart += cart[item].price * cart[item].count;
      }
      return Number(totalCart.toFixed(2));
    };

    // List cart
    obj.listCart = function () {
      var cartCopy = [];
      for (i in cart) {
        item = cart[i];
        itemCopy = {};
        for (p in item) {
          itemCopy[p] = item[p];
        }
        itemCopy.total = Number(item.price * item.count).toFixed(2);
        cartCopy.push(itemCopy);
      }
      return cartCopy;
    };

    // cart : Array
    // Item : Object/Class
    // addItemToCart : Function
    // removeItemFromCart : Function
    // removeItemFromCartAll : Function
    // clearCart : Function
    // countCart : Function
    // totalCart : Function
    // listCart : Function
    // saveCart : Function
    // loadCart : Function
    return obj;
  })();



  // Clear items
  $(".clear-cart").click(function () {
    shoppingCart.clearCart();
    displayCart();
  });

  function displayCart() {
    var cartArray = shoppingCart.listCart();
    var output = "";

    for (var i in cartArray) {
      var priceRS = cartArray[i].price.toLocaleString("pt-BR", {
        style: "currency",
        currency: "BRL",
      });
      var totalRS = cartArray[i].total.toLocaleString("pt-BR", {
        style: "currency",
        currency: "BRL",
      });

      if ($(window).width() < 960) {
        output +=
          "<tr>" +
          "<td style='padding-right:0; border:none;'>" +
          cartArray[i].name +
          "</td>" +
          "<td style='padding-right:0; border:none;'>(" +
          priceRS +
          ")</td>" +
          "</tr>" +
          "<tr style='border-bottom: 1px solid #dee2e6;'>" +
          "<td style='padding-right:0; border:none;'><div class='input-group'><button hidden class='minus-item input-group-addon btn btn-sm btn-primary m-0' data-name='" +
          cartArray[i].name +
          "' style='padding: 0 1.5rem;height:36px;'>-</button>" +
          "<input type='number' class='item-count form-control' data-name='" +
          cartArray[i].name +
          "' value='" +
          cartArray[i].count +
          "' style='height: 36px;padding: 0 ; text-align: center; line-height: 36px; min-height: auto;'>" +
          "<button  hidden class='plus-item btn btn-sm btn-primary m-0 input-group-addon' data-name='" +
          cartArray[i].name +
          "' style='padding: 0 1.5rem;height:36px;'>+</button></div></td>" +
          "<td style='padding-right:0; border:none;'><button class='delete-item btn btn-sm btn-danger m-0' data-name='" +
          cartArray[i].name +
          "' style='padding: 0 .75rem; height:36px;'>X</button></td>" +
          " = " +
          "<td style='padding-right:0; border:none;'>" +
          totalRS +
          "</td>" +
          "</tr>";
      } else {
        output +=
          "<tr>" +
          "<td style='padding-right:0'>" +
          cartArray[i].name +
          "</td>" +
          "<td style='padding-right:0'>(" +
          priceRS +
          ")</td>" +
          "<td style='padding-right:0'><div class='input-group'><button  hidden class='minus-item input-group-addon btn btn-sm btn-primary m-0' data-name='" +
          cartArray[i].name +
          "' style='padding: 0 1.5rem;height:36px;'>-</button>" +
          "<input type='number' class='item-count form-control' data-name='" +
          cartArray[i].name +
          "' value='" +
          cartArray[i].count +
          "' style='height: 36px;padding: 0 ;text-align: center; line-height: 36px; min-height: auto;'>" +
          "<button  hidden class='plus-item btn btn-sm btn-primary m-0 input-group-addon' data-name='" +
          cartArray[i].name +
          "' style='padding: 0 1.5rem;height:36px;'>+</button></div></td>" +
          "<td style='padding-right:0'><button class='delete-item btn btn-sm btn-danger m-0' data-name='" +
          cartArray[i].name +
          "' style='padding: 0 .75rem; height:36px;'>X</button></td>" +
          " = " +
          "<td style='padding-right:0'>" +
          totalRS +
          "</td>" +
          "</tr>";
      }
    }
    $(".show-cart").html(output);

    var totalCartRS = shoppingCart
      .totalCart()
      .toLocaleString("pt-BR", { style: "currency", currency: "BRL" });
    $(".total-cart").html(totalCartRS);
    $(".total-count").html(shoppingCart.totalCount());
  }

  // Delete item button

  $(".show-cart").on("click", ".delete-item", function (event) {
    var name = $(this).data("name");
    shoppingCart.removeItemFromCartAll(name);
    displayCart();
  });

  // -1
  $(".show-cart").on("click", ".minus-item", function (event) {
    var name = $(this).data("name");
    shoppingCart.removeItemFromCart(name);
    displayCart();
  });
  // +1
  $(".show-cart").on("click", ".plus-item", function (event) {
    var name = $(this).data("name");
    shoppingCart.addItemToCart(name);
    displayCart();
  });

  // Item count input
  $(".show-cart").on("change", ".item-count", function (event) {
    var name = $(this).data("name");
    var count = Number($(this).val());
    shoppingCart.setCountForItem(name, count);
    displayCart();
  });

  displayCart();

  $(".modal input, .modal select").on("change", function () {
    if (
      $("input[name='nome']").val() != "" &&
      $("input[name='email']").val() != "" &&
      $("input[name='endereco']").val() != ""
    ) {
      $("#send").attr("disabled", false);
    } else {
      $("#send").attr("disabled", true);
    }
  });

  $("#send").on("click", function () {
    var nome = $("input[name='nome']").val();
    var email = $("input[name='email']").val();
    var endereco = $("input[name='endereco']").val();
    var observacoes = $("textarea[name='observacoes']").val();
    var pagamento = $("select[name='pgto']").val();
    var pedido = "";
    for (var i in cart) {
      pedido += cart[i].count + " " + cart[i].name + "%0A";
    }
    var valor = "*Valor total: R$ " + shoppingCart.totalCart() + "*";
    var msg =
      "Olá, meu nome é " +
      nome +
      " <" +
      email +
      ">%0A %0A*Segue meu pedido:* %0A" +
      pedido +
      "%0A" +
      valor +
      "%0A %0A" +
      "Forma de pagamento: " +
      pagamento +
      "%0A %0A" +
      "Endereço: " +
      endereco +
      "%0A %0A" +
      "Observações: " +
      observacoes;

   /** aqui é pra apontar pro servico onde vai salvar no banco */
   /*$.post("https://padariasaorafael.com.br/save.php", {
      nome: nome,
      email: email,
      pedido: pedido + valor,
    });*/

    /** aqui envia pro whatsapp */
    window.open("https://api.whatsapp.com/send?phone=5516993807724&text=" + msg);
    //window.open("https://api.whatsapp.com/send?phone=5516993807724&text="+msg);
  });

  $(document).on("scroll", function () {
    if ($(this).scrollTop() >= 476) {
      $("#pagination").addClass("fixedtop");

    } else {

      $("#pagination").removeClass("fixedtop");
    }

    if ($(this).scrollTop() >= 370) {
      $("#filterm").addClass("fixedtop");

    } else {

      $("#filterm").removeClass("fixedtop");
    }
  });
