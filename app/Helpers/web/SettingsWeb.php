<?php

namespace App\Helpers\web;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

use App\Models\Address;
use App\Models\Category;
use App\Models\Company;
use App\Models\CompanyDistances;
use App\Models\PostalCode;
use App\Models\Product;
use App\Models\Setting;

use DB;

class SettingsWeb
{
	public static function getCss()
	{
		$arrCss = array(
			'https://use.fontawesome.com/releases/v5.11.2/css/all.css',
			'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap',
			'web/css/bootstrap.min.css',
			'web/css/mdb-free.min.css',
			'web/css/style.css',
		);
		return $arrCss;
	}


	public static function getJs()
	{
		$arrJs = array(
			'web/js/jquery-3.4.1.min.js',
			'web/js/popper.min.js',
			'web/js/bootstrap.min.js',
			'web/js/mdb.min.js',
			'web/js/jquery.validate.min.js',
			'web/js/jquery.mask.min.js',
			'web/js/jquery.maskmoney.min.js',
			'web/js/custom.js',
		);
		return $arrJs;
	}


	public static function searchItem($label = NULL)
	{
		$settingsQuery = Setting::where('label', $label);
		if ($settingsQuery->exists()) {

			$row = $settingsQuery->first();

			return $row->value;

		}
	}


	public static function mountPreviewCitiesFooter()
	{
		$html = '<ul class="list-unstyled">';

			$query = Address::leftJoin('cities', 'cities.id', '=', 'addresses.cities_id')
							->leftJoin('states', 'states.id', '=', 'addresses.states_id')
							->leftJoin('companies as company', function ($join) {
									$join->on('company.id', '=', 'addresses.companies_id');
									$join->where('company.is_active', 1);
							})
							->where('addresses.is_active', 1)
							->groupByRaw('cities.id, cities.title, cities.slug, states.abbreviation')
							->limit(4)
							->select(
									'cities.id as idCity',
									'cities.title as titleCity',
									'cities.slug as slugCity',
									'states.abbreviation'
							);

			foreach ($query->get() as $row) {

				$html .= '<li>
					<a href="' . route('web.home') . '/' . $row->slugCity . '-' . strtolower($row->abbreviation) . '">' . $row->titleCity . '-' . $row->abbreviation . '</a>
				</li>';

			}

		$html .= '</ul>';

		return $html;
	}


	public static function mountPreviewProducts($view='normal', $title=NULL, $limit=NULL, $random=NULL, $featured=NULL, $promotion=NULL, $release=NULL, $searchedTerm=NULL)
	{
		$html = NULL;

		$query = static::getProducts($limit, $random, $featured, $promotion, $release, $searchedTerm);

		switch ($view) {

			case 'normal':

				$html .= '<h4 class="text-center font-weight-bold dark-grey-text mb-5">
					<strong>' . $title . '</strong>
				</h4>';

				if ($query->exists()) {

					foreach ($query->get() as $row) {

						$url = $row->slugCity . '-' . strtolower($row->abbreviation) . '/' . $row->slugSegment . '/' . $row->slugCompany . '#' . $row->slugCategory;

						$value = (($row->value_promotional) ? $row->value_promotional : $row->value);

						$html .= ' <div class="card hoverable mb-4" itemscope itemtype="https://schema.org/Product">
							<div class="card-body">

								<a href="' . $url . '" class="row align-items-center text-dark checkZipcode"  itemprop="url">
									<div class="col-5 px-0">
										<img itemprop="image" src="' . Storage::disk('s3')->url(static::searchItem('folder-upload-products') . 'large/' . $row->idCompany . '/' . $row->id . '/' . $row->file) . '" class="img-fluid" title="' . $row->title . '">
									</div>

									<div class="col-7" itemprop="offers" itemscope itemtype="https://schema.org/Offer">
										<strong  itemprop="name">' . $row->title . '</strong>

										<p itemprop="description">' . str_limit($row->description, 60) . '</p>

										<h6 class="h6-responsive font-weight-bold dark-grey-text"  itemprop="price">
											<strong>R$ ' . number_format($value, 2, ',', '.') . '</strong>
										</h6>
									</div>
								</a>

							</div>
						</div>';

					}
				
				} else { $html .= '<p class="text-center">Não existem "' . $title . '" cadastradas até o momento...</p>'; }

			break;

			case 'featured':

				$html .= '<h3 class="font-weight-bold mb-4 pb-2">' . $title . '</h3>

				<p class="grey-text w-responsive mx-auto mb-5">Confira a baixo os produtos mais pedidos.</p>

				<div class="row">';

					if ($query->exists()) {

						foreach ($query->get() as $row) {

							$url = $row->slugCity . '-' . strtolower($row->abbreviation) . '/' . $row->slugSegment . '/' . $row->slugCompany . '#' . $row->slugCategory;

							$value = (($row->value_promotional) ? $row->value_promotional : $row->value);

							$html .= '<div class="col-lg-4 col-md-12 mb-4">
								<div class="card hoverable wider card-ecommerce" itemscope itemtype="https://schema.org/Product">
									<div class=" view-cascade overlay">
										<a href="' . $url . '">
											<img  itemprop="image" src="' . Storage::disk('s3')->url(static::searchItem('folder-upload-products') . 'large/' . $row->idCompany . '/' . $row->id . '/' . $row->file) . '" class="card-img-top" alt="' . $row->title . '">
										</a>

										<div class="mask rgba-white-slight"></div>
									</div>

									<a href="' . $url . '" class="card-body card-body-cascade text-center pb-4  text-dark"  itemprop="url">
										<h5 class="card-title"  itemprop="name">
											<strong>' . $row->title . '</strong>
										</h5>

										<p class="card-text" itemprop="description">' . $row->description . '</p>

										<h6 class="h6-responsive font-weight-bold dark-grey-text"  itemprop="offers" itemscope itemtype="https://schema.org/Offer">
											<span itemprop="price">R$ '  . number_format($value, 2, ',', '.') .  '</span>
										</h6>
									</a>
								</div>
							</div>';

						}

					}

				$html .= '</div>';

			break;

			case 'search':

				if ($query->exists()) {

					foreach ($query->get() as $row) {

						$url = $row->slugCity . '-' . strtolower($row->abbreviation) . '/' . $row->slugSegment . '/' . $row->slugCompany . '#' . $row->slugCategory;

						$value = (($row->value_promotional) ? $row->value_promotional : $row->value);

						$html .= '<div class="col-lg-4 col-md-6 col-12">
							<div class="card hoverable mb-4" itemscope itemtype="https://schema.org/Product">
								<div class="card-body">

									<a href="' . $url . '" class="row align-items-center text-dark" itemprop="url">
										<div class="col-5 px-0">
											<img itemprop="image" src="' . Storage::disk('s3')->url(static::searchItem('folder-upload-products') . 'large/' .  $row->idCompany . '/' . $row->id . '/' . $row->file) . '" class="img-fluid" title="' . $row->title . '">
										</div>

										<div class="col-7"  itemprop="offers" itemscope itemtype="https://schema.org/Offer">
											<strong itemprop="name">' . $row->title . '</strong>

											<p itemprop="description">' . str_limit($row->description, 60) . '</p>

											<h6 class="h6-responsive font-weight-bold dark-grey-text">
												<strong itemprop="price">R$ ' . number_format($value, 2, ',', '.') . '</strong>
											</h6>
										</div>
									</a>

								</div>
							</div>

						</div>';
					}

				}

			break;
		}

		return $html;
	}


	public static function getProducts($limit=NULL, $random=NULL, $featured=NULL, $promotion=NULL, $release=NULL, $searchedTerm=NULL)
	{
		$query = Product::leftJoin('companies', 'companies.id', '=', 'products.companies_id')
						  ->leftJoin('categories as segment', function ($join) {
								$join->on('segment.id', '=', 'companies.categories_id');
								$join->where('segment.type', 1);
								$join->where('segment.is_active', 1);
						  })
						  ->leftJoin('categories as category', function ($join) {
								$join->on('category.id', '=', 'products.categories_id');
								$join->where('category.type', 2);
								$join->where('category.is_active', 1);
						  })
						  ->leftjoin('addresses', 'addresses.companies_id', '=', 'companies.id')
						  ->leftJoin('cities', 'cities.id', '=', 'addresses.cities_id')
						  ->leftJoin('states', 'states.id', '=', 'addresses.states_id')
						  ->leftJoin('images', function ($join) {
								$join->on('images.products_id', '=', 'products.id');
								$join->where('images.type', 3);
						  })
						  ->where('products.is_active', 1)
						  ->select(
								'products.*',
								'companies.id as idCompany',
								'companies.slug as slugCompany',
								'images.file',
								'segment.title as titleSegment',
								'segment.slug as slugSegment',
								'category.title as titleCategory',
								'category.slug as slugCategory',
								'cities.slug as slugCity',
								'states.abbreviation'
						  );

		if ($limit) {
			$query->limit($limit);
		}

		if ($random) {
			$query->inRandomOrder();
		}

		if ($featured) {
			$query->where('products.is_featured', 1);
		}

		if ($promotion) {
			$query->whereNotNull('products.value_promotional');
		}

		if ($release) {
			$query->orderBy('products.created_at', 'desc');
		}

		if ($searchedTerm) {
			$query->where('products.title', 'like', '%' . $searchedTerm . '%');
			$query->orWhere('products.description', 'like', '%' . $searchedTerm . '%');
			$query->orWhere('category.title', 'like', '%' . $searchedTerm . '%');
		}

		return $query;
	}


	public static function mountPreviewCompanies($view='normal', $limit=NULL, $random=NULL, $companiesId=NULL, $searchedTerm=NULL)
	{
		$html = NULL;

		$query = static::getCompanies($limit, $random, $companiesId, $searchedTerm);
		if ($query->exists()) {

			switch ($view) {

				case 'normal':

					foreach ($query->get() as $row) {

						$url = route('web.home') . '/' . $row->slugCity . '-' . strtolower($row->abbreviation) . '/' . $row->slugSegment . '/' . $row->slug;

						$html .= '<div class="col-lg-4 col-md-6 col-12">
						<div class="card hoverable mb-4" itemscope itemtype="https://schema.org/LocalBusiness">
							<div class="card-body">

							<a href="' . $url . '" class="row align-items-center text-dark"  title="' . $row->fantasy_name . '" itemprop="url">
								<div class="col-5 px-0">

								<img itemprop="image" src="' . Storage::disk('s3')->url(static::searchItem('folder-upload-companies') . $row->id . '/' . $row->file) . '" class="img-fluid" title="' . $row->fantasy_name . '">

								</div>
								<div class="col-7">

								<strong itemprop="name">' . $row->fantasy_name . '</strong>

								<p itemprop="description">' . str_limit($row->description, 70) . '</p>

								</div>
							</a>

							</div>
						</div>

						</div>';
					}

				break;

			}

		}

		return $html;
	}


	public static function getCompanies($limit=NULL, $random=NULL, $companiesId=NULL, $searchedTerm=NULL)
	{
		$query = Company::leftJoin('categories as segment', function ($join) {
			$join->on('segment.id', '=', 'companies.categories_id');
			$join->where('segment.type', 1);
			$join->where('segment.is_active', 1);
		})
			->leftJoin('addresses', 'addresses.companies_id', '=', 'companies.id')
			->leftJoin('cities', 'cities.id', '=', 'addresses.cities_id')
			->leftJoin('states', 'states.id', '=', 'addresses.states_id')
			->leftJoin('images', function ($join) {
				$join->on('images.companies_id', '=', 'companies.id');
				$join->where('images.type', 1);
			})
			->where('companies.is_active', 1)
			->select(
				'companies.*',
				'images.file',
				'segment.title as titleSegment',
				'segment.slug as slugSegment',
				'cities.slug as slugCity',
				'states.abbreviation'
			);

		if ($limit) {
			$query->limit($limit);
		}

		if ($random) {
			$query->inRandomOrder();
		}

		if ($companiesId) {
			$query->whereIn('companies.id', $companiesId);
		}

		if ($searchedTerm) {
			$query->where('fantasy_name', 'like', '%' . $searchedTerm . '%');
			$query->orWhere('companies.description', 'like', '%' . $searchedTerm . '%');
			$query->orWhere('segment.title', 'like', '%' . $searchedTerm . '%');
		}

		return $query;
	}


	public static function insertZipCode($zipcode)
	{
		$zipcodeAdjusted = str_replace('-', '', $zipcode);

		$checkPostalCode = PostalCode::where('zip_code', $zipcode);
		if ($checkPostalCode->doesntExist()) {

			$guzzle = new \GuzzleHttp\Client();
			$apiCall = $guzzle->get('https://www.cepaberto.com/api/v3/cep?cep=' . $zipcodeAdjusted, [
				'headers' => [
					"Authorization"	=> 'Token token=e33d29b935a9fdb5c2b61d5283115bd8',
					"Accept"		=> "application/json",
				]
			]);

			if ($apiCall->getBody()) {

				$result = json_decode($apiCall->getBody()->getContents());

				$postalC = new PostalCode;
				$postalC->zip_code	= $zipcode;
				$postalC->latitude	= $result->latitude;
				$postalC->longitude	= $result->longitude;
				$postalC->save();

			}

			return false;

		} else { return $checkPostalCode; }
	}


	public static function checkDistance($companyId, $companyLog, $companyLat, $longitude, $latitude)
	{
		$guzzle = new \GuzzleHttp\Client();
		$apiCallMapeia = $guzzle->get('https://www.mapeia.com.br/route/v1/driving/' . $companyLog . ',' . $companyLat . ';' . $longitude . ',' . $latitude . '?overview=false&alternatives=false&steps=false&hints=;');
		if ($apiCallMapeia->getBody()) {

			$resultApiMapeia = json_decode($apiCallMapeia->getBody()->getContents());

			$distanceApi = floatval($resultApiMapeia->routes[0]->distance/1000);

			$companyDistance = CompanyDistances::where('start_distance', '<=', $distanceApi)
												 ->where('end_distance', '>=', $distanceApi)
												 ->where('companies_id', $companyId);
			// dd($companyDistance->toSql(), $companyDistance->getBindings());
			if ($companyDistance->exists()) {

				return $companyDistance->first()->value;

			} else { return 0; }

		} else { return 0; }
	}
}
