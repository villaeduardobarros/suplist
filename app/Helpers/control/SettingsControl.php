<?php

namespace App\Helpers\control;

use Illuminate\Http\Request;

use App\Models\Setting;

class SettingsControl
{
	
	public static function getCss()
	{
		$arrCss = array(
			'control/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css',
			'control/css/font-icons/entypo/css/entypo.css',
			'control/css/font-icons/entypo/css/animation.css',
			'http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic',
			'control/css/font-icons/font-awesome/css/font-awesome.min.css',
			'control/css/neon.css',
		);

		return $arrCss;
	}
	

	public static function getJs()
	{
		$arrJs = array(
			'control/js/gsap/main-gsap.js',
			'control/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js',
			'control/js/bootstrap.min.js',
			'control/js/joinable.js',
			'control/js/resizeable.js',
			'control/js/neon-api.js',
			'control/js/neon-custom.js',
		);
		
		return $arrJs;
	}


	public static function searchItem($label = NULL)
	{
		$settingsQuery = Setting::where('label', $label);
		if ($settingsQuery->exists()) {

			$row = $settingsQuery->first();

			return $row->value;

		}
	}

}
