<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

use Auth;
use Closure;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        }
    }

    
    public function handle($request, Closure $next, $guard = null)
    {
        // if (!Auth::guard('customers')->check()) {
        //     return redirect()->route('control.login');
        // }

        // if (!Auth::guard('drafts')->check()) {
        //     return redirect()->route('web.home');
        // }

        // if (!Auth::guard('companies')->check()) {
        //     return redirect()->route('control.login');
        // }

        if (Auth::guard($guard)->guest()) {
            
            if ($request->ajax() | $request->wantsJson()) {

                return response('Unauthorized', 401);

            } else {

                if (!Auth::guard('customers')->check()) {
                    return redirect()->route('control.login');
                }

                if (!Auth::guard('drafts')->check()) {
                    return redirect()->route('web.home');
                }

                if (!Auth::guard('companies')->check()) {
                    return redirect()->route('control.login');
                }

            }

        }

        return $next($request);
    }
}
