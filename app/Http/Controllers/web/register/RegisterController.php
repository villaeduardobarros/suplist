<?php

namespace App\Http\Controllers\web\register;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

use App\Http\Controllers\Controller;
use App\Mail\EmailDraft;
use App\Models\Affiliate;
use App\Models\Draft;
use App\Models\Messaging;
use App\Models\Newsletter;

use Auth;
use Mail;
use stdClass;
use SettingsHelperWeb;

class RegisterController extends Controller
{
	protected $arrCss;
	protected $arrJs;

	public function __construct()
	{
		$this->arrCss	= SettingsHelperWeb::getCss();
		$this->arrJs	= SettingsHelperWeb::getJs();
	}


	public function index($tokenAffiliate=NULL)
	{
		if ($tokenAffiliate) {

			$queryAffiliate = Affiliate::where('token', $tokenAffiliate)
										 ->where('is_active', 1);
			if ($queryAffiliate->exists()) {

				$affiliate = $queryAffiliate->first();
				$tokenAffiliateId = $affiliate->id;

			} else {

				$tokenAffiliateId = NULL;
				Session::flash('message', 'O código do afiliado informado inválido!');

			}

		} else { $tokenAffiliateId = NULL; }

		return view('web.register.register', [
			'arrCss'			=> $this->arrCss,
			'arrJs'				=> $this->arrJs,
			'banner'			=> false,
			'tokenAffiliateId'	=> $tokenAffiliateId,
		]);
	}

    
	public function store(Request $request)
	{
		$validator = Validator::make(
			$request->all(),
			[
				'name'	=> 'required',
				'email'	=> 'required|email',
				'phone'	=> 'required',
			],
			[
				'required'	=> 'Campo obrigatório',
				'email'		=> 'Informe um e-mail válido',
			]
        );

        if ($validator->fails()) {
			
			# return
			$code = 409;
			$response = $validator->messages();

		} else {

			$draft = Draft::where('email', $request->email);
			if ($draft->exists()) {

				# return
				$code = 409;
				$response = [
					'message_action'	=> 'danger',
					'message'			=> 'O e-mail informado já foi utilizado!'
				];

			} else {
		
				$password = str_random(8);

				$hashedPassword = Hash::make(md5($password));

				$draft = new Draft;
				$draft->name			= $request->name;
				$draft->email			= $request->email;
				$draft->phone			= $request->phone;
				$draft->password		= $hashedPassword;
				$draft->is_active		= 1;
				$draft->affiliates_id	= $request->affiliate_id;
				if ($draft->save()) {

					if ($request->news == 1) {

						$queryNewsletter = Newsletter::where('email', $request->email);
						if ($queryNewsletter->doesntExist()) {

							$news = new Newsletter;
							$news->email		= $request->email;
							$news->is_active	= 1;
							$news->save();

						}

					}

					# register message that will be sent by the command
					$message = new Messaging();
					$message->email_from	= SettingsHelperWeb::searchItem('register-email');
					$message->name_from		= SettingsHelperWeb::searchItem('title-project');
					$message->email_to		= $request->email;
					$message->name_to		= $request->name;
					$message->subject		= SettingsHelperWeb::searchItem('register-subject');
					$message->view			= SettingsHelperWeb::searchItem('register-view');
					$message->field			= json_encode([
												'nameTo'		=> $request->name,
												'emailTo'		=> $request->email,
												'passwordTemp'	=> $password,
												'url'			=> route('web.draft.login')
											  ]);
					$message->is_active		= 1;
					$message->drafts_id		= $draft->id;
					$message->save();

					# return
					$code = 201;
					$response = [
						'message_action'	=> 'success',
						'message'			=> 'Seu cadastro foi efetuado com sucesso!<br>Em breve você receberá instruções no e-mail cadastrado.'
					];

				} else {

					# return
					$code = 409;
					$response = [
						'message_action'	=> 'danger',
						'message'			=> 'Não foi possível cadastrar o e-mail!'
					];
				}

			}

		}

		return response()->json($response, $code);
	}

}