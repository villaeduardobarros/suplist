<?php

namespace App\Http\Controllers\web\about;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Plan;

use SettingsHelperWeb;

class AboutController extends Controller
{
	
	protected $arrCss;
	protected $arrJs;

	public function __construct()
	{
		$this->arrCss = SettingsHelperWeb::getCss();
		$this->arrJs = SettingsHelperWeb::getJs();
	}


	public function index()
	{
		$plans = Plan::all();

		return view('web.about.about', [
			'arrCss'	=> $this->arrCss,
			'arrJs'		=> $this->arrJs,
			'banner'	=> false,
			'plans'		=> $plans,
		]);
	}
}