<?php

namespace App\Http\Controllers\web\modal;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\AttributeOption;
use App\Models\Product;
use App\Models\Statistic;
use App\Models\VariationOption;

use DB;
use SettingsHelperWeb;
use Storage;

class ModalController extends Controller
{
	public function showProductsDetails(Request $request)
	{
		$variation = NULL;

		# product
		$product = Product::where('products.id', $request->product_id)
							->leftJoin('images', 'images.products_id', '=', 'products.id')
							->where('products.is_active', DB::raw("1"))
							->select(
								'products.*',
								'images.file'
							);
		if ($product->exists()) {

			$rowpro = $product->first();

			# records access to the product
			$statistic = new Statistic;
			$statistic->products_id = $rowpro->id;
			$statistic->save();


			$image = NULL;
			if ($rowpro->file) {
				$image = Storage::disk('s3')->url(SettingsHelperWeb::searchItem('folder-upload-products') . 'large/' . $rowpro->companies_id . '/' . $rowpro->id . '/' . $rowpro->file);
			}

			$value = (($rowpro->value_promotional) ? $rowpro->value_promotional : $rowpro->value);

			return view('web.modal.products-modal', [
				'action'	=> 'add',
				'product'	=> $rowpro,
				'image'		=> $image,
				'value'		=> $value
			]);

		}
	}
	
	
	public function showVariationsDetails(Request $request)
	{
		$variation = NULL;

		$variationOption = VariationOption::where('products_id', $request->product_id)
											->leftJoin('attributes', function($join){
												$join->on('attributes.id', '=', 'variations_options.attributes_id');
												$join->where('attributes.is_active', DB::raw("1"));
											})
											->groupBy('attributes_id')
											->select(
												'products_id',
												'attributes_id',
												'attributes.title as attributeTitle',
												'attributes.slug as attributeSlug',
												'attributes.quantity',
												DB::raw('group_concat(attributes_options_id) options')
											);
		// dd($variationOption->toSql(), $variationOption->getBindings());
		if ($variationOption->exists()) {

			$variation .= '<hr class="mb-1">';

			foreach ($variationOption->get() as $rowvar) {

				$variation .= '<div class="opcional">
					<h6 class="mb-0"><strong>' . $rowvar->attributeTitle . '</strong></h6>
					<p class="mb-0"><small>Escolha ' . $rowvar->quantity . (($rowvar->quantity == 1) ? ' opção' : ' opções') . '</small></p>';

					$options = AttributeOption::whereIn('id', explode(',', $rowvar->options))
												->orderBy('title', 'desc');
					if ($options->exists()) {

						if ($rowvar->quantity == 1) {
							
							foreach ($options->get() as $rowopt) {

								$variation .= '<div class="custom-control custom-radio">
									<input type="radio" class="custom-control-input" id="' . strtolower($rowopt->title) . '" name="' . str_replace('-', '_', $rowvar->attributeSlug) . '" value="' . $rowopt->id . '">
									<label class="custom-control-label" for="' . strtolower($rowopt->title) . '">' . $rowopt->title . '</label>
								</div>';

							}

						} else {
							
							foreach ($options->get() as $rowopt) {

								$variation .= '<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="' . strtolower($rowopt->title) . '" name="' . str_replace('-', '_', $rowvar->attributeSlug) . '[]" value="' . $rowopt->id . '">
									<label class="custom-control-label" for="' . strtolower($rowopt->title) . '">' . $rowopt->title . '</label>
								</div>';

							}

						}

					}

				$variation .= '</div>
				<hr class="mb-1">';				
	
			}

			return $variation;

		}
		
	}


	public function storeAttributesOptions(Request $request)
	{
		$flag = false;

		# deletes before linking the new options
		$optionsDelete = VariationOption::where('products_id', $request->product_id)
										  ->where('attributes_id', $request->attribute_id);
		if ($optionsDelete->exists()) {
		
			foreach ($optionsDelete->get() as $rowdel) {
				$destroy = VariationOption::destroy($rowdel->id);
			}
		
		}

		# perform the selected options and record
		foreach ($request->option as $option) {
						
			list($productsId, $attributesId, $attributesOptionsId) = explode('|', $option);

			$variationOption = new VariationOption;
			$variationOption->products_id			= $productsId;
			$variationOption->attributes_id			= $attributesId;
			$variationOption->attributes_options_id	= $attributesOptionsId;
			if ($variationOption->save()) {
				$flag = true;
			}

		}

		if ($flag == true) {

			$code = 201;
			$response = [
				'message_action'	=> 'success',
				'message'			=> 'Atributo vinculado com sucesso!'
			];

		} else {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Não foi possível vincular o artibuto!'
			];

		}

		return response()->json($response, $code);
	}


	# **** **************************************************************


	public function showShoppingCartDetails(Request $request)
	{
		return view('web.modal.request-modal');
	}


	# **** **************************************************************


	public function showZipcode(Request $request)
	{
		return view('web.modal.zipcode-modal');
	}
}