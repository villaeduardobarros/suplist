<?php

namespace App\Http\Controllers\web\terms;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use SettingsHelperWeb;

class TermsOfUserController extends Controller
{
	
	protected $arrCss;
	protected $arrJs;

	public function __construct()
	{
		$this->arrCss = SettingsHelperWeb::getCss();
		$this->arrJs = SettingsHelperWeb::getJs();
	}

	public function index()
	{
		return view('web.terms.terms', [
			'arrCss'	=> $this->arrCss,
			'arrJs'		=> $this->arrJs,
			'banner'	=> false
		]);
	}

}