<?php

namespace App\Http\Controllers\web\products;

use Illuminate\Http\Request;

use Spatie\OpeningHours\OpeningHours;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Category;
use App\Models\Company;
use App\Models\City;
use App\Models\ImageMdl;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\PaymentForm;
use App\Models\Product;
use App\Models\State;
use App\Models\Statistic;

use DateTime;
use DB;
use Redirect;
use SettingsHelperWeb;
use Storage;
use URL;

class ProductsController extends Controller
{
	
	protected $arrCss;
	protected $arrJs;
	protected $titleSlug;
	protected $citySlug;
	protected $cityId;
	protected $stateInitials;
	protected $stateId;
	protected $segmentsId;
	protected $titlePage;
	protected $companiesId;

	public function __construct()
	{
		$this->arrCss			= SettingsHelperWeb::getCss();
		$this->arrJs			= SettingsHelperWeb::getJs();
		$this->titleSlug		= NULL;
		$this->citySlug			= NULL;
		$this->cityId			= NULL;
		$this->stateInitials	= NULL;
		$this->stateId			= NULL;
		$this->segmentsId		= NULL;
		$this->titlePage		= NULL;
		$this->companiesId		= [];
	}


	public function index()
	{
        //
    }


	public function segment($cityState=NULL, $segment=NULL)
	{
		# check if the city was informed
		if ($cityState) {

			# breaks URL information
			$total			= strlen($cityState);
			$slugCity		= substr($cityState, 0, ($total-3));
			$abbreviation	= substr($cityState, ($total-2), $total);

			# checks if the information is true
			$checkCityState = City::where('cities.slug', $slugCity)
									->leftJoin('states', function($join) use ($abbreviation){
										$join->on('states.id', '=', 'cities.states_id');
										$join->where('states.abbreviation', '=', $abbreviation);
									})
									->select(
										'cities.title as titleCity',
										'cities.id as idCity',
										'states.id as idState',
										'states.abbreviation'
									);
			if ($checkCityState->exists()) {

				$data = $checkCityState->first();

				# disregards if there is segment not to record duplicate
				if (!$segment) {

					# records city access
					$statistic = new Statistic;
					$statistic->cities_id = $data->idCity;
					$statistic->save();
				
				}

				# page title
				$this->titlePage = '<span class="font-weight-bold">' . $data->titleCity . '-' . $data->abbreviation . '</span>';

				# records global variables
				$this->cityTitle		= $data->titleCity;
				$this->citySlug			= $slugCity;
				$this->cityId			= $data->idCity;
				$this->stateInitials	= $data->abbreviation;
				$this->stateId			= $data->idState;

				# seeks companies for information informed
				$address = Address::where('cities_id', $data->idCity)
									->where('states_id', $data->idState)
									->whereNotNull('companies_id')
									->select('companies_id');
				if ($address->exists()) {

					foreach ($address->get() as $company) {

						# assembles the array with the company id
						$this->companiesId[] = $company->companies_id;

					}

				} else {				

					return view('web.segments.segments', [
						'arrCss'	=> $this->arrCss,
						'arrJs'		=> $this->arrJs,
						'banner'	=> false,
						'titlePage'	=> $this->titlePage,
						'count'		=> count($this->companiesId),
					]);
	
				}

			} else {				

				return view('web.segments.segments', [
					'arrCss'	=> $this->arrCss,
					'arrJs'		=> $this->arrJs,
					'banner'	=> false,
					'titlePage'	=> $this->titlePage,
					'count'		=> count($this->companiesId),
					'companies'	=> NULL
				]);

			}

		}

		# checks if the segment was informed
		if ($segment) {

			$this->companiesId = NULL;

			# checks if the information is true
			$checkSegment = Category::where('slug', $segment)
								->where('type', 1)
								->where('is_active', 1);
			if ($checkSegment->exists()) {

				$segment = $checkSegment->first();

				# registers access in the segment
				$statistic = new Statistic;
				$statistic->categories_id = $segment->id;
				$statistic->cities_id = $this->cityId;
				$statistic->save();

				# page title
				$this->titlePage = '<span class="font-weight-bold">' . $segment->title . (($this->cityTitle) ? ' em ' . $this->cityTitle . '-' . $this->stateInitials : NULL) . '</span>';

				$companies = Company::where('categories_id', $segment->id)
									  ->where('is_active', 1)
									  ->select('id');
				if ($companies->exists()) {

					foreach ($companies->get() as $company) {

						# assembles the array with the company id
						$this->companiesId[] = $company->id;

					}

				}

			} else {

				return Redirect::to(route('web.home') . '/' . $this->citySlug . '-' . $this->stateInitials)
								 ->header('Cache-Control', 'no-store, no-cache, must-revalidate');

			}

		}

		if ($this->companiesId) {

			# removes duplicate values
			array_unique($this->companiesId);

			# assembles the view
			$companies = SettingsHelperWeb::mountPreviewCompanies('normal', NULL, TRUE, $this->companiesId);

			return view('web.segments.segments', [
				'arrCss'	=> $this->arrCss,
				'arrJs'		=> $this->arrJs,
				'banner'	=> false,
				'titlePage'	=> $this->titlePage,
				'count'		=> count($this->companiesId),
				'companies'	=> $companies
			]);

		} else {

			return Redirect::to(route('web.home'))
							 ->header('Cache-Control', 'no-store, no-cache, must-revalidate');

		}
    }


	public function single($cityState=NULL, $segment=NULL, $company=NULL)
	{
		if ($segment) {

			# search the selected company segment [type=1]
			$queryCategory = Category::where('slug', $segment)
									   ->where('type', 1)
									   ->where('is_active', 1);
			if ($queryCategory->exists()) {

				$rowseg = $queryCategory->first();

				# fetch the data of the selected company
				$queryCompany = Company::leftJoin('addresses', 'addresses.companies_id', '=', 'companies.id')
										 ->leftJoin('cities', 'cities.id', '=', 'addresses.cities_id')
										 ->leftJoin('states', 'states.id', '=', 'addresses.states_id')
										 ->leftJoin('images', function ($join) {
											$join->on('images.companies_id', '=', 'companies.id');
											$join->where('images.type', 1);
										 })
										 ->where('companies.slug', $company)
										 ->where('companies.categories_id', $rowseg->id)
										 ->where('companies.is_active', 1)
										 ->select(
												'companies.*', 
												'addresses.address',
												'addresses.number',
												'addresses.complement',
												'addresses.neighborhood',
												'addresses.zip_code',
												'images.file',
												'cities.title as titleCity',
												'cities.slug as slugCity',
												'states.title as titleState',
												'states.abbreviation'
										);

				#dd($queryCompany->toSql(), $queryCompany->getBindings());
							
				if ($queryCompany->exists()) {

					$rowcom = $queryCompany->first();

					# imagem logo
					$imageLogo = Storage::disk('s3')->url(SettingsHelperWeb::searchItem('folder-upload-companies') . $rowcom->id . '/' . $rowcom->file);

					# imagem banner
					$imageBanner = 'web/img/banner.jpg';
					$queryBanner = ImageMdl::where('images.companies_id', $rowcom->id)
											 ->where('images.type', 2);
					if ($queryBanner->exists()) {

						$rowban = $queryBanner->first();

						$imageBanner = Storage::disk('s3')->url(SettingsHelperWeb::searchItem('folder-upload-companies') . $rowban->companies_id . '/' . $rowban->file);

					}

					# records company access
					$statistic = new Statistic;
					$statistic->companies_id = $rowcom->id;
					$statistic->save();

					$addressComplete = $rowcom->address . 
									   (($rowcom->number) ? ', ' . $rowcom->number : NULL) . 
									   (($rowcom->complement) ? $rowcom->complement : NULL) . 
									   (($rowcom->neighborhood) ? ' - ' . $rowcom->neighborhood : NULL) . 
									   (($rowcom->zip_code) ? ' - ' . $rowcom->zip_code : NULL) . ', ' .
									   $rowcom->titleCity . '/' . $rowcom->titleState;

					
					# check if the establishment is OPEN or CLOSED
					$openAndClose = 0;

					$nameDayWeek = 'hours_' . strtolower(date('l', mktime(date('H'), date('i'), date('s'), date('m'), date('d')-1, date('Y'))));

					list($num, $opening, $closure, $ctpm) = explode('|', $rowcom->$nameDayWeek);

					list($hourOpe, $minuteOpe) = explode(':', $opening);
					$openingTime = mktime($hourOpe, $minuteOpe, '00', date('m'), date('d')-1, date('Y'));

					list($hourClo, $minuteClo) = explode(':', $closure);
					$closingTime = mktime($hourClo, $minuteClo, '00', date('m'), (($ctpm == 1) ? date('d') : date('d')), date('Y'));

					$atual = strtotime(date('Y-m-d H:i:s'));

					if ($atual >= $openingTime && $atual <= $closingTime) {

						$openAndClose = 1;

					} else {

						$nameDayWeek = 'hours_' . strtolower(date('l', mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'))));

						list($num, $opening, $closure, $ctpm) = explode('|', $rowcom->$nameDayWeek);

						list($hourOpe, $minuteOpe) = explode(':', $opening);
						$openingTime = mktime($hourOpe, $minuteOpe, '00', date('m'), date('d'), date('Y'));

						list($hourClo, $minuteClo) = explode(':', $closure);
						$closingTime = mktime($hourClo, $minuteClo, '00', date('m'), (($ctpm == 1) ? date('d')+1 : date('d')), date('Y'));

						$atual = strtotime(date('Y-m-d H:i:s'));

						if ($atual >= $openingTime && $atual <= $closingTime) {

							$openAndClose = 1;

						}

					}

					$arrProducts = NULL;

					# seeks products and their attributes
					$queryCategoriesProducts = Product::leftJoin('categories', function($join){
															$join->on('categories.id', '=', 'products.categories_id');
															$join->where('categories.type', 2);
															$join->where('categories.is_active', 1);
														})
														->where('products.companies_id', $rowcom->id)
														->where('products.is_active', 1)
														->orderBy('categories.ordernation', 'asc')
														->select(
															'products.*', 
															'categories.title as titleSegment',
															'categories.slug as slugSegment'
														);
					#dd($queryCategoriesProducts->toSql(), $queryCategoriesProducts->getBindings());
					if ($queryCategoriesProducts->exists()) {
						
						foreach ($queryCategoriesProducts->get() as $rowpro) {

							# imagem banner
							$imageProduct = 'web/img/product.png';
							$queryImageProduct = ImageMdl::where('images.products_id', $rowpro->id)
														   ->where('images.type', 3)
														   ->where('images.is_active', 1);
							if ($queryImageProduct->exists()) {

								$rowima = $queryImageProduct->first();

								$imageProduct = Storage::disk('s3')->url(SettingsHelperWeb::searchItem('folder-upload-products') . 'large/' . $rowcom->id . '/' . $rowpro->id . '/' . $rowima->file);

							}

							# creates an array that is used throughout the product view
							$arrProducts[$rowpro->titleSegment][] = array(
								'id'			=> $rowpro->id,
								'title'				=> $rowpro->title,
								'slug'				=> $rowpro->slug,
								'description'		=> $rowpro->description,
								'value'				=> $rowpro->value,
								'value_promotional'	=> $rowpro->value_promotional,
								'companies_id'		=> $rowpro->companies_id,
								'image'				=> $imageProduct
							);

						}

					}

					# order form
					# form states
					$states = State::all();
					# forms of payment form
					$paymentForm = PaymentForm::all();

					return view('web.products.single', [
						'arrCss'			=> $this->arrCss,
						'arrJs'				=> $this->arrJs,
						'banner'			=> false,
						'titleSegment'		=> $rowseg->title,
						'slugSegment'		=> $rowseg->slug,
						'company'			=> $rowcom,
						'imageBanner'		=> $imageBanner,
						'imageLogo'			=> $imageLogo,
						'addressComplete'	=> $addressComplete,
						'slugCity'			=> $rowcom->slugCity,
						'abbreviation'		=> $rowcom->abbreviation,
						'openAndClose'		=> $openAndClose,
						'states'			=> $states,
						'paymentForm'		=> $paymentForm,
						'arrProducts'		=> $arrProducts,

						'ogTitle'			=> $rowcom->fantasy_name . ' - ' . $rowseg->title . ' - ' . $rowcom->slugCity . '-' . $rowcom->abbreviation . ' - suplist.com.br',
						'ogDescription'		=> $rowcom->description,
						'ogImage'			=> (($rowcom->file) ? $rowcom->id . '/' . $rowcom->file : NULL),
					]);

				} else {
					return redirect()->route('web.products.segment', $segment);
				}

			} else {
				
				return Redirect::to(route('web.home'))
								 ->header('Cache-Control', 'no-store, no-cache, must-revalidate');

			}			

		} else {
			
			return Redirect::to(route('web.home'))
							 ->header('Cache-Control', 'no-store, no-cache, must-revalidate');

		}
	}


	public function singleToken($token=NULL)
	{
		# fetch the data of the selected company
		$queryCompany = Company::leftJoin('addresses', 'addresses.companies_id', '=', 'companies.id')
								 ->leftJoin('cities', 'cities.id', '=', 'addresses.cities_id')
								 ->leftJoin('states', 'states.id', '=', 'addresses.states_id')
								 ->leftJoin('images', function ($join) {
									$join->on('images.companies_id', '=', 'companies.id');
									$join->where('images.type', 1);
								 })
								 ->where('companies.token', $token)
								 ->where('companies.is_active', 1)
								 ->select(
									'companies.*', 
									'addresses.address',
									'addresses.number',
									'addresses.complement',
									'addresses.neighborhood',
									'addresses.zip_code',
									'images.file',
									'cities.title as titleCity',
									'cities.slug as slugCity',
									'states.title as titleState',
									'states.abbreviation'
								 );
							
		if ($queryCompany->exists()) {

			$rowcom = $queryCompany->first();

			# search the selected company segment [type=1]
			$queryCategory = Category::where('id', $rowcom->categories_id)
									   ->where('type', 1)
									   ->where('is_active', 1);
			$rowseg = $queryCategory->first();

			# imagem logo
			$imageLogo = URL::to('/') . '/web/img/logo_suplist.jpg';
			if ($rowcom->file) {
				$imageLogo = Storage::disk('s3')->url(SettingsHelperWeb::searchItem('folder-upload-companies') . $rowcom->id . '/' . $rowcom->file);
			}

			# imagem banner
			$imageBanner = URL::to('/') . '/web/img/banner.jpg';

			$queryBanner = ImageMdl::where('images.companies_id', $rowcom->id)
									 ->where('images.type', 2);
			if ($queryBanner->exists()) {

				$rowban = $queryBanner->first();

				$imageBanner = Storage::disk('s3')->url(SettingsHelperWeb::searchItem('folder-upload-companies') . $rowban->companies_id . '/' . $rowban->file);

			}

			# records company access
			$statistic = new Statistic;
			$statistic->companies_id = $rowcom->id;
			$statistic->save();

			$addressComplete = $rowcom->address . 
								(($rowcom->number) ? ', ' . $rowcom->number : NULL) . 
								(($rowcom->complement) ? $rowcom->complement : NULL) . 
								(($rowcom->neighborhood) ? ' - ' . $rowcom->neighborhood : NULL) . 
								(($rowcom->zip_code) ? ' - ' . $rowcom->zip_code : NULL) . ', ' .
								$rowcom->titleCity . '/' . $rowcom->titleState;

			
			# check if the establishment is OPEN or CLOSED
			$openAndClose = 0;

			$nameDayWeek = 'hours_' . strtolower(date('l', mktime(date('H'), date('i'), date('s'), date('m'), date('d')-1, date('Y'))));

			list($num, $opening, $closure, $ctpm) = explode('|', $rowcom->$nameDayWeek);

			list($hourOpe, $minuteOpe) = explode(':', $opening);
			$openingTime = mktime($hourOpe, $minuteOpe, '00', date('m'), date('d')-1, date('Y'));

			list($hourClo, $minuteClo) = explode(':', $closure);
			$closingTime = mktime($hourClo, $minuteClo, '00', date('m'), (($ctpm == 1) ? date('d') : date('d')), date('Y'));

			$atual = strtotime(date('Y-m-d H:i:s'));

			if ($atual >= $openingTime && $atual <= $closingTime) {

				$openAndClose = 1;

			} else {

				$nameDayWeek = 'hours_' . strtolower(date('l', mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'))));

				list($num, $opening, $closure, $ctpm) = explode('|', $rowcom->$nameDayWeek);

				list($hourOpe, $minuteOpe) = explode(':', $opening);
				$openingTime = mktime($hourOpe, $minuteOpe, '00', date('m'), date('d'), date('Y'));

				list($hourClo, $minuteClo) = explode(':', $closure);
				$closingTime = mktime($hourClo, $minuteClo, '00', date('m'), (($ctpm == 1) ? date('d')+1 : date('d')), date('Y'));

				$atual = strtotime(date('Y-m-d H:i:s'));

				if ($atual >= $openingTime && $atual <= $closingTime) {

					$openAndClose = 1;

				}

			}

			$arrProducts = NULL;

			# seeks products and their attributes
			$queryCategoriesProducts = Product::leftJoin('categories', function($join){
													$join->on('categories.id', '=', 'products.categories_id');
													$join->where('categories.type', 2);
													$join->where('categories.is_active', 1);
												})
												->where('products.companies_id', $rowcom->id)
												->where('products.is_active', 1)
												->orderBy('categories.ordernation', 'asc')
												->select(
													'products.*', 
													'categories.title as titleSegment',
													'categories.slug as slugSegment'
												);
			#dd($queryCategoriesProducts->toSql(), $queryCategoriesProducts->getBindings());
			if ($queryCategoriesProducts->exists()) {
				
				foreach ($queryCategoriesProducts->get() as $rowpro) {

					# imagem banner
					$imageProduct = URL::to('/') . '/web/img/product.png';
					$queryImageProduct = ImageMdl::where('images.products_id', $rowpro->id)
													->where('images.type', 3)
													->where('images.is_active', 1);
					if ($queryImageProduct->exists()) {

						$rowima = $queryImageProduct->first();

						$imageProduct = Storage::disk('s3')->url(SettingsHelperWeb::searchItem('folder-upload-products') . 'large/' . $rowcom->id . '/' . $rowpro->id . '/' . $rowima->file);

					}

					# creates an array that is used throughout the product view
					$arrProducts[$rowpro->titleSegment][] = array(
						'id'			=> $rowpro->id,
						'title'				=> $rowpro->title,
						'slug'				=> $rowpro->slug,
						'description'		=> $rowpro->description,
						'value'				=> $rowpro->value,
						'value_promotional'	=> $rowpro->value_promotional,
						'companies_id'		=> $rowpro->companies_id,
						'image'				=> $imageProduct
					);

				}

			}

			# order form
			# form states
			$states = State::all();
			# forms of payment form
			$paymentForm = PaymentForm::all();

			return view('web.products.single-token', [
				'arrCss'			=> $this->arrCss,
				'arrJs'				=> $this->arrJs,
				'banner'			=> false,
				'token'				=> $token,
				'titleSegment'		=> $rowseg->title,
				'slugSegment'		=> $rowseg->slug,
				'company'			=> $rowcom,
				'imageBanner'		=> $imageBanner,
				'imageLogo'			=> $imageLogo,
				'addressComplete'	=> $addressComplete,
				'slugCity'			=> $rowcom->slugCity,
				'abbreviation'		=> $rowcom->abbreviation,
				'openAndClose'		=> $openAndClose,
				'states'			=> $states,
				'paymentForm'		=> $paymentForm,
				'arrProducts'		=> $arrProducts,

				'ogTitle'			=> $rowcom->fantasy_name . ' - ' . $rowseg->title . ' - ' . $rowcom->slugCity . '-' . $rowcom->abbreviation . ' - suplist.com.br',
				'ogDescription'		=> $rowcom->description,
				'ogImage'			=> (($rowcom->file) ? $rowcom->id . '/' . $rowcom->file : NULL),
			]);

		} else {
			return redirect()->route('web.products.single.token', $token);
		}
	}

}