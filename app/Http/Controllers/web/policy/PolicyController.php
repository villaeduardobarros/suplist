<?php

namespace App\Http\Controllers\web\policy;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use SettingsHelperWeb;

class PolicyController extends Controller
{
	
	protected $arrCss;
	protected $arrJs;

	public function __construct()
	{
		$this->arrCss = SettingsHelperWeb::getCss();
		$this->arrJs = SettingsHelperWeb::getJs();
	}

	public function index()
	{
		return view('web.policy.policy', [
			'arrCss'	=> $this->arrCss,
			'arrJs'		=> $this->arrJs,
			'banner'	=> false
		]);
	}

}