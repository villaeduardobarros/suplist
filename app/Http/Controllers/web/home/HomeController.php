<?php

namespace App\Http\Controllers\web\home;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;
use App\Models\Newsletter;
use App\Models\Order;
use App\Models\Product;

use DB;
use SettingsHelperWeb;
use Storage;


class HomeController extends Controller
{
	
	protected $arrCss;
	protected $arrJs;

	public function __construct()
	{
		$this->arrCss = SettingsHelperWeb::getCss();
		$this->arrJs = SettingsHelperWeb::getJs();
	}

	public function index()
	{
		# assembles the display of the featured products
		$productsFeatured = SettingsHelperWeb::mountPreviewProducts('featured', 'Em destaque', 3, TRUE, TRUE);

		# assembles the visualization of products on sale
		$productsPromotions = SettingsHelperWeb::mountPreviewProducts('normal', 'Promoções', 3, TRUE, NULL, TRUE);

		# assembles the visualization of launch products
		$productsLaunches = SettingsHelperWeb::mountPreviewProducts('normal', 'Lançamentos', 3, NULL, NULL, NULL, TRUE);

		# assembles the visualization of launch products
		$productsMoreOrders = Order::leftJoin('orders_products', function ($join) {
										$join->on('orders_products.orders_id', '=', 'orders.id');
									 })
									 ->leftJoin('products', function ($join) {
										$join->on('products.id', '=', 'orders_products.products_id');
										$join->where('products.is_active', 1);
									 })
									 ->leftJoin('companies', 'companies.id', '=', 'products.companies_id')
									 ->leftJoin('categories as segment', function ($join) {
										 $join->on('segment.id', '=', 'companies.categories_id');
										 $join->where('segment.type', 1);
										 $join->where('segment.is_active', 1);
									 })
									 ->leftJoin('categories as category', function ($join) {
										 $join->on('category.id', '=', 'products.categories_id');
										 $join->where('category.type', 2);
										 $join->where('category.is_active', 1);
									 })
									 ->leftjoin('addresses', 'addresses.companies_id', '=', 'companies.id')
									 ->leftJoin('cities', 'cities.id', '=', 'addresses.cities_id')
									 ->leftJoin('states', 'states.id', '=', 'addresses.states_id')
									 ->leftJoin('images', function ($join) {
										 $join->on('images.products_id', '=', 'products.id');
										 $join->where('images.type', 3);
									 })
									 ->groupBy('orders_products.products_id')
									 ->orderByRaw('COUNT(*) desc')
									 ->orderBy('orders.created_at', 'desc')
									 ->limit(3)
									 ->select(
										 'products.*',
										 'companies.id as idCompany',
										 'companies.slug as slugCompany',
										 'images.file',
										 'segment.title as titleSegment',
										 'segment.slug as slugSegment',
										 'category.title as titleCategory',
										 'category.slug as slugCategory',
										 'cities.slug as slugCity',
										 'states.abbreviation'
									 );

		$html = '<h4 class="text-center font-weight-bold dark-grey-text mb-5">
			<strong>Mais Pedidos</strong>
		</h4>';

		if ($productsMoreOrders->exists()) {

			foreach ($productsMoreOrders->get() as $row) {

				$url = $row->slugCity . '-' . strtolower($row->abbreviation) . '/' . $row->slugSegment . '/' . $row->slugCompany . '#' . $row->slugCategory;

				$value = (($row->value_promotional) ? $row->value_promotional : $row->value);

				$html .= '<div class="card hoverable mb-4" itemscope itemtype="https://schema.org/Product">
					<div class="card-body">

						<a href="' . $url . '" class="row align-items-center text-dark checkZipcode" itemprop="url">
							<div class="col-5 px-0">
								<img itemprop="image" src="' . Storage::disk('s3')->url(SettingsHelperWeb::searchItem('folder-upload-products') . 'large/' . $row->companies_id . '/' . $row->id . '/' . $row->file) . '" class="img-fluid" title="' . $row->title . '">
							</div>

							<div class="col-7" itemprop="offers" itemscope itemtype="https://schema.org/Offer">
								<strong itemprop="name">' . $row->title . '</strong>

								<p itemprop="description">' . str_limit($row->description, 60) . '</p>

								<h6 class="h6-responsive font-weight-bold dark-grey-text"  itemprop="price">
									<strong>R$ ' . number_format($value, 2, ',', '.') . '</strong>
								</h6>
							</div>
						</a>

					</div>
				</div>';

			}

		}

		return view('web.home.home', [
			'arrCss'				=> $this->arrCss,
			'arrJs'					=> $this->arrJs,
			'banner'				=> true,
			'productsFeatured'		=> $productsFeatured,
			'productsPromotions'	=> $productsPromotions,
			'productsLaunches'		=> $productsLaunches,
			'productsMoreOrders'	=> $html,
		]);
	}


	public function newsletter(Request $request)
	{
		$validator = Validator::make(
			$request->all(),
			['email_news'	=> 'required|email'],
			['required'		=> 'Campo obrigatório']
        );

        if ($validator->fails()) {
			
			$code = 409;
			$response = $validator->messages();

		} else {

			$queryNewsletter = Newsletter::where('email', $request->email_news);
			if ($queryNewsletter->exists()) {

				$code = 409;
				$response = [
					'message_action'	=> 'danger',
					'message'			=> 'E-mail informado já cadastrado!'
				];

			} else {

				$news = new Newsletter;
				$news->email		= $request->email_news;
				$news->is_active	= 1;
				if ($news->save()) {

					$code = 201;
					$response = [
						'message_action'	=> 'success',
						'message'			=> 'E-mail informado já cadastrado!'
					];

				} else {

					$code = 409;
					$response = [
						'message_action'	=> 'danger',
						'danger'			=> 'Não foi possível cadastrar o e-mail!'
					];
				}

			}

		}

		return response()->json($response, $code);

	}
}