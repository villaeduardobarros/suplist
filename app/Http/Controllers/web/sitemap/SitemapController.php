<?php

namespace App\Http\Controllers\web\sitemap;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Category;
use App\Models\Company;

use URL;

class SitemapController extends Controller
{
	public function index()
	{
		$xml[] = array(
			'loc'			=> URL::to('/'),
			'changefreq'	=> 'weekly',
			'priority'		=> 1.0
		);

		$xml[] = array(
			'loc'			=> URL::to('/') . '/cadastro',
			'changefreq'	=> 'weekly',
			'priority'		=> 0.8
		);

		$xml[] = array(
			'loc'			=> URL::to('/') . '/termos-de-uso',
			'changefreq'	=> 'weekly',
			'priority'		=> 0.8
		);

		$xml[] = array(
			'loc'			=> URL::to('/') . '/sac',
			'changefreq'	=> 'weekly',
			'priority'		=> 0.8
		);

		$xml[] = array(
			'loc'			=> URL::to('/') . '/sobre',
			'changefreq'	=> 'weekly',
			'priority'		=> 0.8
		);

		$xml[] = array(
			'loc'			=> URL::to('/') . '/politica-de-privacidade',
			'changefreq'	=> 'weekly',
			'priority'		=> 0.8
		);

		# seek cities and states linked to companies
		$searchCityState = Address::join('companies', function ($join) {
										$join->on('companies.id', '=', 'addresses.companies_id');
									})
									->join('cities', 'cities.id', '=', 'addresses.cities_id')
									->join('states', 'states.id', '=', 'addresses.states_id')
									->where('companies.is_active', 1)
									->groupBy('cities.id')
									->select(
										'companies.id',
										'cities.id as idCity',
										'cities.title as titleCity',
										'cities.slug as slugCity',
										'states.id as idState',
										'states.title as titleState',
										'states.title as slugState',
										'states.abbreviation as initialsState'
									);
		
		if ($searchCityState->exists()) {
			
			foreach ($searchCityState->get() as $cityState) {

				$xml[] = array(
					'loc'			=> URL::to('/') . '/' . $cityState->slugCity . '-' . strtolower($cityState->initialsState),
					'changefreq'	=> 'weekly',
					'priority'		=> 0.7
				);

				# seek categories linked to cities and states
				$searchCategory = Category::join('companies', function ($join) {
												$join->on('companies.categories_id', '=', 'categories.id');
											})
											->join('addresses', 'addresses.id', '=', 'companies.id')
											->where('addresses.cities_id', $cityState->idCity)
											->where('addresses.states_id', $cityState->idState)
											->where('companies.is_active', 1)
											->groupBy('categories.id')
											->select(
												'categories.*'
											);
				if ($searchCategory->exists()) {
				
					foreach ($searchCategory->get() as $category) {

						$xml[] = array(
							'loc'			=> URL::to('/') . '/' . $cityState->slugCity . '-' . strtolower($cityState->initialsState) . '/' . $category->slug,
							'changefreq'	=> 'weekly',
							'priority'		=> 0.6
						);

						# seek companies linked to the categories
						$searchCompany = Company::where('categories_id', $category->id)
												  ->where('is_active', 1);
						if ($searchCompany->exists()) {

							foreach ($searchCompany->get() as $company) {

								$xml[] = array(
									'loc'			=> URL::to('/') . '/' . $cityState->slugCity . '-' . strtolower($cityState->initialsState) . '/' . $category->slug . '/' . $company->slug,
									'changefreq'	=> 'weekly',
									'priority'		=> 0.5
								);

							}

						}

					}

				}

			}

		}
			
		return response()->xml($xml);
	}
}
