<?php

namespace App\Http\Controllers\web\sac;

use App\Http\Controllers\Controller;
use App\Mail\Settings;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Mail;
use stdClass;
use SettingsHelperWeb;

class SacController extends Controller
{
	
	protected $arrCss;
	protected $arrJs;

	public function __construct()
	{
		$this->arrCss = SettingsHelperWeb::getCss();
		$this->arrJs = SettingsHelperWeb::getJs();
	}

	public function index()
	{
		return view('web.sac.sac', [
			'arrCss'	=> $this->arrCss,
			'arrJs'		=> $this->arrJs,
			'banner'	=> false
		]);
	}

    
	public function store(Request $request)
	{
		$validator = Validator::make(
			$request->all(),
			[
				'name'		=> 'required',
				'email'		=> 'required|email',
				'phone'		=> 'required',
				'subject'	=> 'required',
				'message'	=> 'required',
			],
			[
				'required'	=> 'Campo obrigatório',
				'email'		=> 'Informe um e-mail válido',
			]
        );

        if ($validator->fails()) {
			
			$code = 409;
			$response = $validator->messages();

		} else {

			if ($request->news == 1) {

				$queryNewsletter = Newsletter::where('email', $request->email);
				if ($queryNewsletter->doesntExist()) {

					$news = new Newsletter;
					$news->email		= $request->email;
					$news->is_active	= 1;
					$news->save();

				}

			}

			switch ($request->subject) {
				case 1:	$subject = 'Sugestão de melhoria';	break;
				case 2: $subject = 'Report a bug';			break;
				case 3: $subject = 'Reclamação';			break;
				case 4: $subject = 'Desejo anunciar';		break;
			}

			# dados do e-mail
			$data = new stdClass();				
			$data->emailFrom	= $request->email;
			$data->nameFrom		= $request->name;
			$data->phoneFrom	= $request->phone;
			$data->subjectFrom	= $subject;
			$data->messageFrom	= $request->message;
			$data->subject		= SettingsHelperWeb::searchItem('sac-subject');
			$data->view			= 'web.mail.sac';
			$data->nameTo		= SettingsHelperWeb::searchItem('sac-name');
			$data->emailTo		= SettingsHelperWeb::searchItem('sac-email');

			# envio
			Mail::send(new Settings($data));

			$code = 201;
			$response = [
				'message_action'	=> 'success',
				'message'			=> 'Sua mensagem foi enviada com sucesso!<br>Em breve retornaremos seu contato.'
			];

		}

		return response()->json($response, $code);
	}

}