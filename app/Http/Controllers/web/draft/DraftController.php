<?php

namespace App\Http\Controllers\web\draft;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Category;
use App\Models\Company;
use App\Models\CompanyUser;
use App\Models\Draft;
use App\Models\Messaging;
use App\Models\Plan;
use App\Models\State;

use Auth;
use SettingsHelperWeb;

class DraftController extends Controller
{
	protected $arrCss;
	protected $arrJs;

	public function __construct()
	{
		$this->arrCss = SettingsHelperWeb::getCss();
		$this->arrJs = SettingsHelperWeb::getJs();
	}


	public function index(Request $request)
	{
		array_push($this->arrJs, 'web/js/jquery.maskmoney.min.js');

		$queryDraft = Draft::where('email', Auth()->guard('drafts')->user()->email);
		if ($queryDraft->exists()) {

			$row = $queryDraft->get();

			$segments = Category::where('type', 1)
								  ->where('is_active', 1)
								  ->orderBy('title', 'asc')
								  ->get();
			
			$states = State::all();

			return view('web.drafts.draft', [
				'arrCss'	=> $this->arrCss,
				'arrJs'		=> $this->arrJs,
				'request'	=> $request,
				'segments'	=> $segments,
				'states'	=> $states,
			]);

		} else { return redirect()->route('web.register.logout'); }
		
	}


	public function login()
	{
		return view('web.drafts.login', [
			'arrCss' => $this->arrCss,
			'arrJs'	 => $this->arrJs
		]);
	}


	public function authenticate(Request $request)
	{
		$validator = Validator::make(
			$request->all(),
			[
				'email'		=> 'required|email',
				'password'	=> 'required',
			],
			[
				'required'	=> 'Campos obrigatórios',
				'email'		=> 'Informe um e-mail válido',
			]
        );

        if ($validator->fails()) {
			
			$code = 409;
			$response = $validator->messages();

		} else {

			$hashedPassword = md5($request->password);

			$credentials = array(
				'email'		=> $request->email,
				'password'	=> $hashedPassword,
				'is_active'	=> 1,
			);

			if (Auth::guard('drafts')->attempt($credentials)) {

				$draft = Draft::where('email', $request->email)
								->where('is_active', 1);
				if ($draft->exists()) {

					$row = $draft->first();

					# create a session
					$request->session()->put('drafts_id', $row->id);
					$request->session()->put('drafts_name', $row->name);
					$request->session()->put('drafts_email', $row->email);
					$request->session()->put('drafts_phone', $row->phone);

					$code = 200;
					$response = [
						'message_action'	=> 'success',
						'message'			=> 'Login efetuado com sucesso!',
						'redirect'			=> route('web.draft.data'),
					];

				} else {

					$code = 200;
					$response = [
						'message_action'	=> 'success',
						'message'			=> 'Seu cadastro já foi finalizado!<br>Você será redicionado para o painel de acesso das empresas.',
						'redirect'			=> route('control.login'),
					];

				}

			} else {

				$code = 409;
				$response = [
					'message_action'	=> 'danger',
					'message'			=> 'Dados inválidos!'
				];

			}

		}

		return response()->json($response, $code);
	}


	public function create(Request $request)
	{
		array_push($this->arrJs, 'web/js/jquery.maskmoney.min.js');

		$queryDraft = Draft::where('email', $request->session()->get('drafts_email'));
		if ($queryDraft->exists()) {

			$row = $queryDraft->get();

			$segments = Category::where('type', 1)
								  ->where('is_active', 1)
								  ->orderBy('title', 'asc')
								  ->get();
			
			$states = State::all();

			$plans = Plan::all();

			return view('web.drafts.draft', [
				'arrCss'		=> $this->arrCss,
				'arrJs'			=> $this->arrJs,
				'request'		=> $request,
				'affiliatesId'	=> $row[0]->affiliates_id,
				'segments'		=> $segments,
				'states'		=> $states,
				'plans'			=> $plans,
			]);

		} else { return redirect()->route('web.draft.logout'); }
		
	}


	public function store(Request $request)
	{
		$flag = false;

		$validator = Validator::make(
			$request->all(),
			[
				'email'			=> 'required|email',
				'password'		=> 'required',
				'name'			=> 'required',
				'phone'			=> 'required',
				'segment'		=> 'required',
				'type_person'	=> 'required',
				'cell_phone'	=> 'required',
				'fantasy_name'	=> 'required',
				'title'			=> 'required',
				'zip_code'		=> 'required',
				'state'			=> 'required',
				'city'			=> 'required',
				'address'		=> 'required',
				'number'		=> 'required',
				'neighborhood'	=> 'required',
				'plan'			=> 'required',
			],
			[
				'required'	=> 'Campo obrigatório',
				'email'		=> 'Informe um e-mail válido',
			]
        );

        if ($validator->fails()) {
			
			$code = 409;
			$response = $validator->messages();

		} else {

			# new password generated
			$hash = md5($request->password);
			$hashedPassword	= Hash::make($hash);

			# if it is a new segment, register and use the new ID
			if ($request->segment == 'other') {

				$categories = Category::where('title', $request->other)
										->where('type', 1);
				if ($categories->doesntExist()) {

					$category = new Category;
					$category->type			= 1;
					$category->title		= $request->other;
					$category->slug			= str_slug($request->other);
					$category->is_active	= 1;
					$category->save();

					$idSegment = $category->id;

				}

			} else { $idSegment = $request->segment; }			

			$slugCompany = str_slug($request->fantasy_name, '-');

			# register company
			$company = new Company;
			$company->token					= md5(str_random(8));
			$company->type_of_person		= $request->type_person;
			$company->name					= $request->name;
			$company->corporate_name		= $request->company_name;
			$company->fantasy_name			= $request->fantasy_name;
			$company->slug					= $slugCompany;
			$company->id_card				= $request->id_card;
			$company->reg_of_individuals	= $request->reg_of_individuals;
			$company->reg_legal_entity		= $request->reg_legal_entity;
			$company->email					= $request->email;
			$company->phone					= $request->phone;
			$company->cell_phone			= $request->cell_phone;
			$company->description			= $request->description;
			$company->value_delivery		= (($request->value_delivery) ? (($request->value_delivery == '0,00') ? NULL : str_replace(',', '.', str_replace('.', '', $request->value_delivery))) : NULL);
			$company->is_active				= 1;
			$company->categories_id			= $idSegment;
			$company->drafts_id				= $request->drafts_id;
			$company->affiliates_id			= (($request->affiliates_id) ? $request->affiliates_id : NULL);
			$company->plans_id				= $request->plan;
			if ($company->save()) {

				# register primary user
				$companyUser = new CompanyUser;
				$companyUser->name			= $request->name;
				$companyUser->email			= $request->email;
				$companyUser->phone			= $request->phone;
				$companyUser->cell_phone	= $request->whatsapp;
				$companyUser->password		= $hashedPassword;
				$companyUser->is_active		= 1;
				$companyUser->companies_id	= $company->id;
				$companyUser->save();

				# primary address
				$address = new Address;
				$address->title			= $request->title;
				$address->address		= $request->address;
				$address->number		= $request->number;
				$address->complement	= $request->complement;
				$address->neighborhood	= $request->neighborhood;
				$address->is_active		= 1;
				$address->zip_code		= $request->zip_code;
				$address->cities_id		= $request->city;
				$address->states_id		= $request->state;
				$address->companies_id	= $company->id;
				if ($address->save()) {

					// check if the zip code informed already has in our database
					SettingsHelperWeb::checkZipCode($request->zip_code);

				}

				# register message that will be sent by the command
				$message = new Messaging();
				$message->email_from	= SettingsHelperWeb::searchItem('draft-email');
				$message->name_from		= SettingsHelperWeb::searchItem('title-project');
				$message->email_to		= $request->email;
				$message->name_to		= $request->name;
				$message->subject		= SettingsHelperWeb::searchItem('draft-subject');
				$message->view			= SettingsHelperWeb::searchItem('draft-view');
				$message->field			= json_encode([
											'nameTo'		=> $request->name,
											'emailTo'		=> $request->email
										  ]);
				$message->is_active		= 1;
				$message->companies_id	= $company->id;
				$message->save();

				$flag = true;

			} else {
				$flag = false;
			}
		
		}


		if ($flag == true) {

			# delete the session used in DRAFT
			Auth::guard('drafts')->logout();
			$request->session()->forget('drafts_name');
			$request->session()->forget('drafts_email');
			$request->session()->forget('drafts_phone');

			# uses the data entered to access automatically in panel
			$credentials = array(
				'email'		=> $request->email,
				'password'	=> $hash,
				'is_active'	=> 1,
			);

			if (Auth::guard('companies')->attempt($credentials)) {

				# create the new session
				$request->session()->put('company_id', $company->id);
				$request->session()->put('company_name', $request->name);
				$request->session()->put('company_email', $request->email);

				$redirect = route('control.account.edit');

			} else { $redirect = route('control.login'); }

			$code = 201;
			$response = [
				'message_action'	=> 'success',
				'message'			=> 'Seu cadastro foi efetuado com sucesso!<br>Em breve você receberá instruções no e-mail cadastrado.',
				'redirect'			=> $redirect
			];

		} else {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Não foi possível cadastrar a estabelecimento!'
			];

		}

		return response()->json($response, $code);
	}
}
