<?php

namespace App\Http\Controllers\web\search;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Product;
use App\Models\Search;

use DB;
use SettingsHelperWeb;

class SearchController extends Controller
{
	
	protected $arrCss;
	protected $arrJs;

	public function __construct()
	{
		$this->arrCss = SettingsHelperWeb::getCss();
		$this->arrJs = SettingsHelperWeb::getJs();
	}


	public function index(Request $request)
	{
		$searchedTerm = $request->s;
		if ($searchedTerm) {

			# records the search term for statistics
			$search = new Search;
			$search->terms = $searchedTerm;
			if ($search->save()) {

				$term = str_replace(' ', '%', $searchedTerm);

				$companiesCount		= SettingsHelperWeb::getCompanies(NULL, NULL, NULL, $term)->count();
				$companiesSearch	= SettingsHelperWeb::mountPreviewCompanies('normal', NULL, NULL, NULL, $term);

				$productsCount		= SettingsHelperWeb::getProducts(NULL, NULL, NULL, NULL, NULL, $term)->count();
				$productsSearch		= SettingsHelperWeb::mountPreviewProducts('search', NULL, NULL, NULL, NULL, NULL, NULL, $term);
				
				return view('web.search.search', [
					'arrCss'			=> $this->arrCss,
					'arrJs'				=> $this->arrJs,
					'banner'			=> false,
					'searchedTerm'		=> $searchedTerm,
					'companiesCount'	=> $companiesCount,
					'companiesSearch'	=> $companiesSearch,
					'productsCount'			=> $productsCount,
					'productsSearch'	=> $productsSearch,
				]);

			}
		}

	}

}