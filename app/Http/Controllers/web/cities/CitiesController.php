<?php

namespace App\Http\Controllers\web\cities;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\City;
use App\Models\Company;

use SettingsHelperWeb;

class CitiesController extends Controller
{
	public function index()
	{
		//
	}


	public function search(Request $request)
	{
		$cities = City::where('states_id', $request->state_id);
		if ($cities->exists()) {

			return $cities->get()->map->toArray();

		} else { return false; }
	}


	public function zipcode(Request $request)
	{
		// $request->session()->forget('zipcode_string');
		// $request->session()->forget('zipcode_idCity');
		// $request->session()->forget('zipcode_titleCity');
		// $request->session()->forget('zipcode_slugCity');
		// $request->session()->forget('zipcode_idState');
		// $request->session()->forget('zipcode_titleState');
		// $request->session()->forget('zipcode_slugState');
		// $request->session()->forget('zipcode_initialsState');
		
		$searchZipcode = City::where('cities.title', $request->city)
						->join('states', 'states.id', '=', 'cities.states_id')
						->select(
							'cities.id as idCity',
							'cities.title as titleCity',
							'cities.slug as slugCity',
							'states.id as idState',
							'states.title as titleState',
							'states.title as slugState',
							'states.abbreviation as initialsState'
						);
		if ($searchZipcode->exists()) {

			$data = $searchZipcode->first();

			$companies = Address::leftJoin('companies', function ($join) {
										$join->on('companies.id', '=', 'addresses.companies_id');
								  })
								  ->where('addresses.cities_id', $data->idCity)
								  ->where('companies.is_active', 1);
			if ($companies->exists()) {

				// check if the zip code informed already has in our database
				SettingsHelperWeb::insertZipcode($request->zipcode);

				# create a session
				$request->session()->put('zipcode_string', $request->zipcode);
				$request->session()->put('zipcode_idCity', $data->idCity);
				$request->session()->put('zipcode_titleCity', $data->titleCity);
				$request->session()->put('zipcode_slugCity', $data->slugCity);
				$request->session()->put('zipcode_idState', $data->idState);
				$request->session()->put('zipcode_titleState', $data->titleState);
				$request->session()->put('zipcode_slugState', $data->slugState);
				$request->session()->put('zipcode_initialsState', $data->initialsState);

				$code = 200;
				$response = [
					'message_action'	=> 'success',
					'redirect'			=> route('web.home') . '/' . $data->slugCity . '-' . strtolower($data->initialsState)
				];

			} else {

				$code = 200;
				$response = [
					'message_action'	=> 'success',
					'redirect'			=> route('web.home') . '/' . $data->slugCity . '-' . strtolower($data->initialsState)
				];

			}

		} else {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'O CEP informado é inválido!'
			];

		}

		return response()->json($response, $code);
	}

}
