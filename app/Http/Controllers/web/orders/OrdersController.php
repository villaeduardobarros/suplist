<?php

namespace App\Http\Controllers\web\orders;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\AttributeOption;
use App\Models\Category;
use App\Models\City;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\PaymentForm;
use App\Models\Product;
use App\Models\State;

use Cart;
use DB;
use SettingsHelperWeb;

class OrdersController extends Controller
{
	public function index()
	{
		//
	}


	public function store(Request $request)
	{
		$validator = validator($request->all(), [
			'name'			=> 'required',
			'email'			=> 'required',
			'cell_phone'	=> 'required',
			'payment_form'	=> 'required',
			'receive'		=> 'required',
		]);

		if ($validator->fails()) {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Preencha todos os campos obrigatórios!'
			];

		} else {

			if (Cart::count() > 0) {

				$customerId = NULL;

				# creates the look of the order that will be sent via whatsapp
				$html = "*Novo pedido via " . SettingsHelperWeb::searchItem('title-project') . "*%0A%0A";
				
				
				$html .= "*Cliente:* " . $request->name . "%0A
				*E-mail:* " . $request->email . "%0A
				*Celular:* " . $request->cell_phone . "%0A%0A";


				# checks if the email entered has already been registered, if so, search for the ID
				$checkCustomer = Customer::where('email', $request->email);
				if ($checkCustomer->exists()) {

					$rowcus		= $checkCustomer->first();
					$customerId	= $rowcus->id;

				} else {

					$customer = new Customer;
					$customer->name			= $request->name;
					$customer->email		= $request->email;
					$customer->cell_phone	= $request->cell_phone;
					$customer->is_active	= 1;
					if ($customer->save()) {

						$customerId = $customer->id;

					}

				}

				if ($customerId) {

					if ($request->receive == 1) {

						# register the address
						$address = new Address;
						$address->title			= 'Principal';
						$address->address		= $request->address;
						$address->number		= $request->number;
						$address->complement	= (($request->complement) ? $request->complement : NULL);
						$address->neighborhood	= $request->neighborhood;
						$address->zip_code		= $request->zip_code;
						$address->is_active		= 1;
						$address->cities_id		= $request->city;
						$address->states_id		= $request->state;
						$address->customers_id	= $customerId;
						$address->save();

						$city	= City::where('id', $request->city)->first();
						$state	= State::where('id', $request->state)->first();
						
						$html .= "*Endereço de entrega*%0A";
						$html .= $request->address . ", " . $request->number . (($request->complement) ? " " . $request->complement : NULL) . " - " . 
								 $request->neighborhood . ", " . $city->title . "/" . $state->title . "%0A%0A";
			
					}

					# register the order
					$order = new Order;
					$order->amount				= Cart::subtotal();
					$order->value_delivery		= (($request->receive == 2) ? NULL : (($request->value_delivery) ? $request->value_delivery : NULL));
					$order->value_change		= (($request->value_change) ? (($request->value_change == '0,00') ? NULL : str_replace(',', '.', str_replace('.', '', $request->value_change))) : NULL);
					$order->comments			= (($request->comments) ? $request->comments : NULL);
					$order->companies_id		= $request->companies_id;
					$order->customers_id		= $customerId;
					$order->addresses_id		= (($request->receive == 1) ? $address->id : NULL);
					$order->payments_form_id	= $request->payment_form;
					if ($order->save()) {

						$html .= "*Itens*%0A";

						$countItemCart = 1;
						foreach(Cart::content() as $row) {

							$categoryId = $row->options['categories_id'];
		
							$attributeId = NULL;
							if (isset($row->options['attributes_id'])) {

								$attributeId = $row->options['attributes_id'];

							}

							# register the order products
							$orderProduct = new OrderProduct;
							$orderProduct->quantity			= $row->qty;
							$orderProduct->value_unitary	= $row->price;
							$orderProduct->orders_id		= $order->id;
							$orderProduct->products_id		= $row->id;
							if ($attributeId) {

								$orderProduct->attributes_options_id = implode(',', $attributeId);
							
							}
							$orderProduct->save();

							$category = Category::find($categoryId);

							$product = Product::where('id', $row->id)
												->where('companies_id', $request->companies_id)
												->where('is_active', 1)->first();

							$htmlAttribute = NULL;

							# treatment of attributes
							if ($attributeId) {

								$attribute = AttributeOption::whereIn('attributes_options.id', $attributeId)
															  ->leftJoin('attributes', 'attributes.id', '=', 'attributes_options.attributes_id')
															  ->groupBy('attributes.title')
															  ->select(
																  'attributes.title as titleAttribute', 
																  DB::raw('group_concat(attributes_options.title separator \', \') as titleAttributeOption')
															  );
								if ($attribute->exists()) {

									$htmlAttribute .= ' - ';

									$countAttribute = $attribute->count();

									$count = 1;
									foreach ($attribute->get() as $rowAtt) {

										$htmlAttribute .= $rowAtt->titleAttribute . ': ' . $rowAtt->titleAttributeOption . (($count < $countAttribute) ? ' / ' : NULL);

										$count++;

									}

								}

							}

							$html .= $product->title . "%0A";
							$html .= "_" . $category->title . "_" . $htmlAttribute . "%0A";
							$html .= 'R$' . number_format($row->price, 2, ',', '.') . " x " . $row->qty . " = R$" . number_format(($row->qty * $row->price), 2, ',', '.') . "%0A";

							$html .= (($countItemCart < Cart::count()) ? "%0A" : "%0A%0A");

							$countItemCart++;

						}

					}

				}

			
				if ($request->comments) {

					$html .= "*Observações:*%0A";
					$html .= $request->comments . "%0A%0A";

				}

				$paymentForm = PaymentForm::where('id', $request->payment_form)->first();

				$html .= "*Valor dos itens:* R$" . number_format(Cart::subtotal(), 2, ',', '.') . "%0A";

				$html .= "*Forma de pagamento:* " . $paymentForm->title ."%0A";

				if ($request->payment_form == 1) {

					if ($request->value_change) {
					
						$html .= "*Troco para:* R$" . $request->value_change . "%0A";
					
					}

				}

				if ($request->receive == 1) {

					$html .= "*Taxa de entrega:* R$" . number_format($request->value_delivery, 2, ',', '.') . "%0A";

				}

				$html .= "*Total do pedido: R$" . number_format((Cart::subtotal() + $request->value_delivery), 2, ',', '.') . "*%0A%0A";

				$html .= "_Pedido realiazado em " . date('d/m/Y H:i') . "_";

				# delete the shopping cart
				Cart::destroy();

				# kills the zipcode session
				$request->session()->forget('zipcode_string');
				$request->session()->forget('zipcode_idCity');
				$request->session()->forget('zipcode_titleCity');
				$request->session()->forget('zipcode_slugCity');
				$request->session()->forget('zipcode_idState');
				$request->session()->forget('zipcode_titleState');
				$request->session()->forget('zipcode_slugState');
				$request->session()->forget('zipcode_initialsState');

				$code = 201;
				$response = [
					'message_action'	=> 'success',
					'message'			=> 'Pedido realizado com sucesso!',
					'whatsapp'			=> $request->whatsapp,
					'content'			=> $html,
				];

			}

		}

		return response()->json($response, $code);

	}

}