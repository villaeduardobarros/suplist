<?php

namespace App\Http\Controllers\web\cart;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\AttributeOption;
use App\Models\Category;
use App\Models\City;
use App\Models\Company;
use App\Models\CompanyDistances;
use App\Models\PaymentForm;
use App\Models\Product;
use App\Models\ShipmentForm;
use App\Models\State;

use Cart;
use DB;
use URL;
use SettingsHelperWeb;

class CartController extends Controller
{
	public function index()
	{
		//
	}


	public function store(Request $request)
	{
		# product selected
		$product = Product::where('products.id', $request->id)
							->leftJoin('images', 'images.products_id', '=', 'products.id')
							->where('products.is_active', DB::raw("1"))
							->select(
								'products.*',
								'images.file'
							);
		if ($product->exists()) {

			$rowpro = $product->first();

			$arrProduct = array(
				'id'		=> $rowpro->id,
				'name'      => $rowpro->title,
				'qty'       => $request->quantity,
				'price'     => (($rowpro->value_promotional) ? $rowpro->value_promotional : $rowpro->value),
				'options'   => [
					'companies_id' => $rowpro->companies_id,
					'categories_id' => $rowpro->categories_id
				]
			);

			$attributes = Attribute::where('companies_id', $rowpro->companies_id);
			if ($attributes->exists()) {

				foreach ($attributes->get() as $rowatt) {

					$inputName = str_replace('-', '_', $rowatt->slug);
					if ($request->$inputName) {

						// dump($request->$inputName);

						if (is_array($request->$inputName)) {

							foreach ($request->$inputName as $rowinp) {

								$arrProduct['options']['attributes_id'][] = $rowinp;

							}

						} else {

							$arrProduct['options']['attributes_id'][] = $request->$inputName;

						}

					}

				}

			}

			$add = Cart::add($arrProduct);
			if ($add->rowId) {

				$code = 201;
				$response = [
					'message_action'    => 'success',
					'message'           => 'Produto adicionado ao carrinho!',
					'count'				=> Cart::count(),
					'total'				=> Cart::total(),
				];

			} else {

				$code = 409;
				$response = [
					'message_action'    => 'danger',
					'message'           => 'Não foi possível adicionar o produto ao carrinho!'
				];

			}

		} else {

			$code = 409;
			$response = [
				'message_action'    => 'danger',
				'message'           => 'Produto não encontrado!'
			];

		}

		return response()->json($response, $code);
	}


	public function show(Request $request)
	{
		$html = NULL;

		if (Cart::count() > 0) {

			$companyId = 0;

			$html .= '<table class="show-cart table">';

				foreach(Cart::content() as $row) {

					if (isset($row->options['companies_id'])) {
						$companyId = $row->options['companies_id'];
					}

					if (isset($row->options['categories_id'])) {
						$categoryId = $row->options['categories_id'];
					}

					$category = Category::find($categoryId);

					$html .= '<tr>
						<td colspan="4" style="padding:0;border:none;">'  . $category->title . ' - ' . $row->name . '</td>
					</tr>';

					if (isset($row->options['attributes_id'])) {

						$attributeId = $row->options['attributes_id'];

                        $html .='<tr>
							<td colspan="4" style="padding:0;border:none;">';

								$options = AttributeOption::whereIn('attributes_options.id', $attributeId)
															->leftJoin('attributes', 'attributes.id', '=', 'attributes_options.attributes_id')
															->groupBy('attributes.title')
															->select(
																'attributes.title as titleAttribute',
																DB::raw('group_concat(attributes_options.title separator \', \') as titleAttributeOption')
															);
								// dd($options->toSql(), $options->getBindings());
								if ($options->exists()) {

									$html .= ' - ';

									$returnOption = $options->get();

									$countAttribute = $returnOption->count();

									$count = 1;
									foreach ($returnOption as $rowOpt) {

										$html .= $rowOpt->titleAttribute . ': ' . $rowOpt->titleAttributeOption . (($count < $countAttribute) ? ' / ' : NULL);

										$count++;

									}

								}

							$html .= '</td>
						</tr>';

					}
					
					$html .= '<tr style="border-bottom:1px solid #dee2e6;">
						<td style="text-align:center;padding:7px;border:none;" width="25%">R$ ' . number_format($row->price, 2, ',', '.') . '</td>
						<td style="padding:0;border:none;">
							<div class="input-group">
								<button class="minus-item input-group-addon btn btn-sm btn-primary m-0" style="padding:0 1.5rem;height:36px;" onclick="changeQuantity(\'' . $row->rowId . '\', \'less\', ' . $row->qty . ')">-</button>
									<input type="number" min="1" class="item-count form-control item-count-cart" readonly value="' . $row->qty . '" style="height:36px;padding:0;text-align:center;line-height:36px;min-height:auto;">
								<button class="plus-item btn btn-sm btn-primary m-0 input-group-addon" style="padding:0 1.5rem;height:36px;" onclick="changeQuantity(\'' . $row->rowId . '\', \'more\',  ' . $row->qty . ')">+</button>
							</div>
						</td>
						<td style="padding:0;border:none;">
							<button class="delete-item btn btn-sm btn-danger m-0" style="padding:0 .75rem; height:36px;" onclick="removeItem(\'' . $row->rowId . '\')">X</button>
						</td>
						<td style="text-align:center;padding:7px;border:none;" width="25%">R$ ' . number_format(($row->price * $row->qty), 2, ',', '.') . '</td>
					</tr>';

				}

			$html .= '</table>';

			$company = Company::find($companyId);

			$html .= '<div>
				<strong>Total dos Produtos:</strong>
				R$ <span class="total-prod">' . number_format(Cart::subtotal(), 2, ',', '.') . '</span>
			</div>';

			if ($company->delivery == 1) {

				$html .= '<div>
					<strong>Taxa de Entrega:</strong> ';
				
					if ($company->settings_fee == 1) {

						$html .= '<span>R$ ' . (isset($company->value_delivery) ? number_format($company->value_delivery, 2, ',', '.') : 'R$ 0,00') . '</span>';

					} else { $html .= '<span class="value-delivery">A calcular</span>'; }

				$html .= '</div>';

			}

			$html .= '<div>
				<strong>
					Total do Pedido: ';

					$valueDelivery = 0;

					if ($company->delivery == 1) {
						
						if ($company->settings_fee == 1) {

							$valueDelivery = (isset($company->value_delivery) ? $company->value_delivery : 0);

						}

					}
			
					$html .= '<span class="value-total">R$ ' . number_format((Cart::subtotal()+$valueDelivery), 2, ',', '.') . '</span>
				</strong>
			</div>';

		} else {

			$html = '<br>
			<div style="text-align:center;">
				Até o momento não foi encontrado nenhum produto vinculado ao seu pedido.<br><br>
				Vá para a <a href="' . URL::to('/') . '">página inicial</a> ou procure no site pelo produto que procura e adicione ao seu pedido
			</div>';

		}

		return $html;
	}


	public function showForm()
	{
		$html = NULL;

		if (Cart::count() > 0) {

			$companyId = 0;

			$count = 1;
			foreach(Cart::content() as $row) {

				if ($count == 1) {
					$companyId = $row->options['companies_id'];
				}

				$count++;

			}

			$company = Company::leftJoin('addresses', function ($join) {
									$join->on('addresses.companies_id', '=', 'companies.id');
								})
								->leftJoin('postal_codes', function ($join) {
									$join->on('postal_codes.zip_code', '=', 'addresses.zip_code');
								})
								->where('companies.id', $companyId)
								->where('companies.is_active', 1)
								->select(
									'companies.*',
									'postal_codes.latitude',
									'postal_codes.longitude'
								)
								->first();

			$paymentForm = PaymentForm::get();

			$shipmentForm = ShipmentForm::all();

			$states = State::get();

			$html = '<h5 class="mt-4 mb-2">Preencha os dados abaixo:</h5>

			<form class="text-center" action="' . route('web.finish.order') . '" id="formRequest" method="post">
				<input type="hidden" name="_token" id="csrf-token" value="' . Session::token() . '" />
				<input type="hidden" name="companies_id" value="' . $company->id . '">
				<input type="hidden" name="whatsapp" value="' . str_replace(array('(',')','-',' '), '', $company->cell_phone) . '">
				<input type="hidden" name="value_subtotal" value="' . Cart::subtotal() . '">
				<input type="hidden" name="value_total" value="' . (Cart::subtotal() + $company->value_delivery) . '">
				<input type="hidden" name="value_delivery" value="' . $company->value_delivery . '">
				<input type="hidden" name="companies_lat" value="' . $company->latitude . '">
				<input type="hidden" name="companies_lon" value="' . $company->longitude . '">

				<div class="form-group">
					<input type="text" name="name" id="name" class="form-control form-control-sm" placeholder="Informe seu nome *" autocomplete="off" required style="height:36px;line-height:36px;min-height:auto;">
				</div>

				<div class="form-group">
					<input type="email" name="email" id="email" class="form-control form-control-sm" placeholder="Informe seu e-mail *" autocomplete="off" required style="height:36px;line-height:36px;min-height:auto;">
				</div>

				<div class="form-group">
					<input type="text" name="cell_phone" id="cell_phone" class="form-control form-control-sm phone" placeholder="Informe seu celular *" autocomplete="off" required style="height:36px;line-height:36px;min-height:auto;">
				</div>

				<div class="form-group">
					<label for="payment_form">Como você deseja pagar?</label>
					<select name="payment_form" id="payment_form" class="form-control form-control-sm" onchange="selectPaymentsForm(this.value)" required style="height:36px;line-height:36px;padding-top:0;padding-bottom:0;min-height:auto;">
						<option value="">Selecione *</option>';
						foreach ($paymentForm as $payment) {

							$html .= '<option value="' . $payment->id . '">' . $payment->title . '</option>';

						}
					$html .= '</select>
				</div>

				<div id="changeOfMoney" style="display:none;">
					<div class="row mb-3">
						<div class="col-md-12 col-lg-6">
							<input type="text" name="value_change" id="value_change" class="form-control form-control-sm value" placeholder="Troco para" autocomplete="off" style="height:36px;line-height:36px;min-height:auto;">
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="receive">Como deseja receber seu pedido?</label>
					<select name="receive" id="receive" class="form-control form-control-sm" onchange="selectDeliveryForm(this.value)" required style="height:36px;line-height:36px;padding-top:0;padding-bottom:0;min-height:auto;">
						<option value="">Selecione *</option>';
						if ($company->delivery == 1) {
							$html .= '<option value="1">Delivery</option>';
						}

						if ($company->withdraw == 1) {
							$html .= '<option value="2">Retirar no local</option>';
						}
					$html .= '</select>
				</div>

				<div id="changeDeliveryForm" style="display:none;">
					<div class="row mb-3">
						<div class="col-md-6 col-lg-6">
							<input type="text" name="zip_code" id="zip_code" class="form-control form-control-sm zipcode" placeholder="CEP *" autocomplete="off" style="height:36px;line-height:36px;min-height:auto;" value="' . ((Session::has('zipcode_string')) ? Session::get('zipcode_string') : NULL) .'">
						</div>
					</div>

					<div class="row mb-3">
						<div class="col-md-6 col-lg-8">
							<input type="text" name="address" id="address" class="form-control form-control-sm" placeholder="Endereço *" autocomplete="off" style="height:36px;line-height:36px;min-height:auto;">
						</div>
						<div class="col-md-6 col-lg-4">
							<input type="number" name="number" id="number" class="form-control form-control-sm" placeholder="Número *" autocomplete="off" onkeypress="return checkNumber(event)" style="height:36px;line-height:36px;min-height:auto;">
						</div>
					</div>

					<div class="row mb-3">
						<div class="col-md-12 col-lg-6">
							<input type="text" name="complement" id="complement" class="form-control form-control-sm" placeholder="Complemento" autocomplete="off" style="height:36px;line-height:36px;min-height:auto;">
						</div>
						<div class="col-md-12 col-lg-6">
							<input type="text" name="neighborhood" id="neighborhood" class="form-control form-control-sm" placeholder="Bairro *" autocomplete="off" style="height:36px;line-height:36px;min-height:auto;">
						</div>
					</div>

					<div class="row mb-3">
						<div class="col-md-12 col-lg-6">
							<select name="state" id="state" class="form-control form-control-sm" onchange="selectCities(this.value)" style="height:36px;line-height:36px;padding-top:0;padding-bottom:0;min-height:auto;">
								<option value="">Selecione o estado *</option>';
								foreach ($states as $state) {

									$html .= '<option value="' . $state->id . '"' . ((Session::has('zipcode_idState')) ? (($state->id == Session::get('zipcode_idState')) ? ' selected' : NULL) : NULL) . '>' . $state->abbreviation . ' - ' . $state->title . '</option>';

								}
							$html .= '</select>
						</div>
						<div class="col-md-12 col-lg-6">
							<select name="city" id="city" class="form-control form-control-sm" style="height:36px;line-height:36px;padding-top:0;padding-bottom:0;min-height:auto;">
								<option value="">Selecione o estado antes *</option>';
								if (Session::has('zipcode_idState')) {

									$cities = City::where('states_id', Session::get('zipcode_idState'))->get();
									foreach ($cities as $city) {

										$html .= '<option value="' . $city->id . '"' . ((Session::has('zipcode_idCity')) ? (($city->id == Session::get('zipcode_idCity')) ? ' selected' : NULL) : NULL) . '>' . $city->title . '</option>';

									}

								}
							$html .= '</select>
						</div>
					</div>
				</div>

				<div class="form-group">
					<textarea rows="2" type="text" name="comments" id="comments" class="form-control form-control-sm" placeholder="Observações" style="line-height:36px;min-height:auto;"></textarea>
				</div>

				<p class="message" style="margin:0 0 10px;"></p>
			</form>';

		}

		return $html;
	}


	public function checkZipCode(Request $request)
	{
		$distance = 0;

		// check if the zip code informed already has in our database
		$checkPostalCode = SettingsHelperWeb::insertZipCode($request->zip_code);

		if ($checkPostalCode != false) {

			$rowpos = $checkPostalCode->first();

			$distance = SettingsHelperWeb::checkDistance($request->company_id, $request->company_longitude, $request->company_latitude, $rowpos->longitude, $rowpos->latitude);

		}

		return $distance . '|' . (floatval($request->value_subtotal) + floatval($distance));
	}


	public function update(Request $request)
	{
		if ($request->action == 'more') {
			$newQuantity = $request->quantity + 1;
		} else {
			$newQuantity = $request->quantity - 1;
		}

		if ($newQuantity > 0) {
			Cart::update($request->rowId, $newQuantity);
		} else {
			Cart::remove($request->rowId);
		}

		if (Cart::count() > 0) {

			$code = 201;
			$response = [
				'message_action'    => 'success',
				'message'           => 'Quantidade alterada com sucesso!',
			];

		} else {

			$code = 409;
			$response = [
				'message_action'    => 'danger',
				'message'           => 'Carrinho vazio!',
			];

		}

		return response()->json($response, $code);
	}


	public function delete(Request $request)
	{
		Cart::remove($request->rowId);

		if (Cart::count() > 0) {

			$code = 201;
			$response = [
				'message_action'    => 'success',
				'message'           => 'Quantidade alterada com sucesso!',
			];

		} else {

			$code = 409;
			$response = [
				'message_action'    => 'danger',
				'message'           => 'Carrinho vazio!',
			];

		}

		return response()->json($response, $code);
	}


	public function destroy(Request $request)
	{
		dd(Cart::content());
		// Cart::destroy();
	}
}
