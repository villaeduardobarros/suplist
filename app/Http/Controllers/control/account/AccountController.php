<?php

namespace App\Http\Controllers\control\account;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Affiliate;
use App\Models\Category;
use App\Models\City;
use App\Models\Company;
use App\Models\CompanyUser;
use App\Models\ImageMdl;
use App\Models\Newsletter;
use App\Models\Plan;
use App\Models\ShipmentForm;
use App\Models\State;

use Auth;
use Image;
use Redirect;
use SettingsHelperControl;
use SettingsHelperWeb;

class AccountController extends Controller
{
	protected $arrCss;
	protected $arrJs;

	protected $companiesId = NULL;

	# upload image
	protected $folderUpload;
	protected $widthLarge;
	protected $heightLarge;
	protected $widthLargeBanner;
	protected $heightLargeBanner;

	public function __construct()
	{
		$this->arrCss	= SettingsHelperControl::getCss();
		$this->arrJs	= SettingsHelperControl::getJs();

		array_push($this->arrJs, 'control/js/fileinput.js');
		array_push($this->arrJs, 'control/js/jquery.validate.min.js');
		array_push($this->arrJs, 'control/js/jquery/jquery.mask.min.js');
		array_push($this->arrJs, 'control/js/jquery.maskmoney.min.js');
		array_push($this->arrJs, 'control/prop/pattern/pattern.js');
		array_push($this->arrJs, 'control/prop/accounts/accounts.js');

		$this->companiesId = Session::get('company_id');

		# upload image
		$this->folderUpload			= SettingsHelperControl::searchItem('folder-upload-companies');
		$this->widthLarge			= SettingsHelperControl::searchItem('width-companies');
		$this->heightLarge			= SettingsHelperControl::searchItem('height-companies');
		$this->widthLargeBanner		= SettingsHelperControl::searchItem('width-companies-banner');
		$this->heightLargeBanner	= SettingsHelperControl::searchItem('height-companies-banner');
	}


	public function edit()
	{
		$queryCompany = Company::where('id', $this->companiesId);
		if ($queryCompany->exists()) {

			$companySelect = $queryCompany->first();

			$address = Address::where('companies_id', $companySelect->id)
								->first();

			$companyUser = CompanyUser::where('companies_id', $companySelect->id)
										->first();

			$segments = Category::where('type', 1)
								  ->where('is_active', 1)
								  ->orderBy('title', 'asc')
								  ->get();
			
			$states = State::all();

			$cities = City::where('states_id', $address->states_id)
							->get();
			
			$plans = Plan::all();

			$shipments = ShipmentForm::all();

			$imageLogo = ImageMdl::where('type', 1)
								   ->where('companies_id', $companySelect->id)
								   ->first();

			$imageBanner = ImageMdl::where('type', 2)
									 ->where('companies_id', $companySelect->id)
									 ->first();

			return view('control.account.account', [
				'arrCss'			=> $this->arrCss,
				'arrJs'				=> $this->arrJs,
				'companySelect'		=> $companySelect,
				'address'			=> $address,
				'companyUser'		=> $companyUser,
				'segments'			=> $segments,
				'states'			=> $states,
				'cities'			=> $cities,
				'plans'				=> $plans,
				'shipments'			=> $shipments,
				'imageLogo'			=> $imageLogo,
				'imageBanner'		=> $imageBanner,
				'folderUpload'		=> $this->folderUpload,
				'widthLarge'		=> $this->widthLarge,
				'heightLarge'		=> $this->heightLarge,
				'widthLargeBanner'	=> $this->widthLargeBanner,
				'heightLargeBanner'	=> $this->heightLargeBanner
			]);

		} else {

			return Redirect::to(route('control.home'))
							 ->header('Cache-Control', 'no-store, no-cache, must-revalidate');

		}
	}


	public function update(Request $request)
	{
		$validator = Validator::make(
			$request->all(),
			[
				'email'				=> 'required|email',
				'name'				=> 'required',
				'phone'				=> 'required',
				'segment'			=> 'required',
				'type_person'		=> 'required',
				'cell_phone'		=> 'required',
				'fantasy_name'		=> 'required',
				'title'				=> 'required',
				'zip_code'			=> 'required',
				'state'				=> 'required',
				'city'				=> 'required',
				'address'			=> 'required',
				'number'			=> 'required',
				'neighborhood'		=> 'required',
				'plan'				=> 'required',
				'delivery'			=> 'required',
			],
			[
				'required'			=> 'Campo obrigatório',
				'email'				=> 'Informe um e-mail válido',
			]
        );

        if ($validator->fails()) {
			
			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Preencha todos os campos obrigatórios!'
			];

		} else {

			$hashedPassword = NULL;
			if ($request->password) {

				# new password generated
				$hash = md5($request->password);
				$hashedPassword	= Hash::make($hash);

			}

			# adjust the times
			$hoursMonday	= (($request->monday_start && $request->monday_finish) ? ('1|' . $request->monday_start . '|' . $request->monday_finish . '|' . (($request->monday_ctpm) ? $request->monday_ctpm : 0)) : NULL);
			$hoursTuesday	= (($request->tuesday_start && $request->tuesday_finish) ? ('2|' . $request->tuesday_start . '|' . $request->tuesday_finish . '|' . (($request->tuesday_ctpm) ? $request->tuesday_ctpm : 0)) : NULL);
			$hoursWednesday	= (($request->wednesday_start && $request->wednesday_finish) ? ('3|' . $request->wednesday_start . '|' . $request->wednesday_finish . '|' . (($request->wednesday_ctpm) ? $request->wednesday_ctpm : 0)) : NULL);
			$hoursThursday	= (($request->thursday_start && $request->thursday_finish) ? ('4|' . $request->thursday_start . '|' . $request->thursday_finish . '|' . (($request->thursday_ctpm) ? $request->thursday_ctpm : 0)) : NULL);
			$hoursFriday	= (($request->friday_start && $request->friday_finish) ? ('5|' . $request->friday_start . '|' . $request->friday_finish . '|' . (($request->friday_ctpm) ? $request->friday_ctpm : 0)) : NULL);
			$hoursaturday	= (($request->saturday_start && $request->saturday_finish) ? ('6|' . $request->saturday_start . '|' . $request->saturday_finish . '|' . (($request->saturday_ctpm) ? $request->saturday_ctpm : 0)) : NULL);
			$hoursSunday	= (($request->sunday_start && $request->sunday_finish) ? ('0|' . $request->sunday_start . '|' . $request->sunday_finish . '|' . (($request->sunday_ctpm) ? $request->sunday_ctpm : 0)) : NULL);

			$slugCompany = str_slug($request->fantasy_name, '-');

			$updateCompany = Company::findOrFail($request->id);

			# params
			$updateCompany->name				= $request->name;
			$updateCompany->corporate_name		= $request->company_name;
			$updateCompany->fantasy_name		= $request->fantasy_name;
			$updateCompany->slug				= $slugCompany;
			$updateCompany->id_card				= $request->id_card;
			$updateCompany->reg_of_individuals	= $request->reg_of_individuals;
			$updateCompany->reg_legal_entity	= $request->reg_legal_entity;
			$updateCompany->email				= $request->email;
			$updateCompany->phone				= $request->phone;
			$updateCompany->cell_phone			= $request->cell_phone;
			$updateCompany->description			= $request->description;
			$updateCompany->delivery			= (($request->delivery == 0) ? NULL : $request->delivery);
			$updateCompany->value_delivery		= (($request->settings_fee == 1) ? (($request->value_delivery == '0,00') ? NULL : str_replace(',', '.', str_replace('.', '', $request->value_delivery))) : NULL);
			$updateCompany->withdraw			= (($request->withdraw == 0) ? NULL : $request->withdraw);
			$updateCompany->settings_fee		= $request->settings_fee;
			$updateCompany->hours_monday		= $hoursMonday;
			$updateCompany->hours_monday		= $hoursMonday;
			$updateCompany->hours_tuesday		= $hoursTuesday;
			$updateCompany->hours_wednesday		= $hoursWednesday;
			$updateCompany->hours_thursday		= $hoursThursday;
			$updateCompany->hours_friday		= $hoursFriday;
			$updateCompany->hours_saturday		= $hoursaturday;
			$updateCompany->hours_sunday		= $hoursSunday;

			if ($updateCompany->save()) {

				# register primary user
				$updateCompanyUser = CompanyUser::findOrFail($request->company_user_id);
				$updateCompanyUser->name			= $request->name;
				$updateCompanyUser->email			= $request->email;
				$updateCompanyUser->phone			= $request->phone;
				$updateCompanyUser->cell_phone		= $request->cell_phone;
				if ($hashedPassword) {
					$updateCompanyUser->password	= $hashedPassword;
				}
				$updateCompanyUser->is_active		= 1;
				$updateCompanyUser->companies_id	= $request->id;
				$updateCompanyUser->save();

				# primary address
				$updateAddress = Address::findOrFail($request->address_id);
				$updateAddress->address			= $request->address;
				$updateAddress->number			= $request->number;
				$updateAddress->complement		= $request->complement;
				$updateAddress->neighborhood	= $request->neighborhood;
				$updateAddress->is_active		= 1;
				$updateAddress->zip_code		= $request->zip_code;
				$updateAddress->cities_id		= $request->city;
				$updateAddress->states_id		= $request->state;
				$updateAddress->companies_id	= $request->id;
				if ($updateAddress->save()) {
					
					// check if the zip code informed already has in our database
					SettingsHelperWeb::insertZipcode($request->zip_code);

				}

				if ($request->file('image_logo')) {

					if ($request->hasFile('image_logo')) {
		
						# folder
						$pathLogo = $this->folderUpload . $this->companiesId . '/';

						if ($request->image_logo_id) {
		
							# update the images table by "deleting" the image
							$deleteLogoOld = ImageMdl::find($request->image_logo_id);
							if ($deleteLogoOld->exists()) {
		
								$deleteLogoOld->delete();
		
								# delete the image if it exists in the folder "LARGE"
								if (Storage::disk('s3')->exists($pathLogo . $request->image_logo_old)) {
									# delete the S3 file
									Storage::disk('s3')->delete($pathLogo . $request->image_logo_old);
								}
		
							}

						}
	
	
						$file		= $request->file('image_logo');
						$extension	= $request->file('image_logo')->getClientOriginalExtension();
	
						# generates a new name for the image (md5)
						$imageName	= md5($slugCompany . time()) . '.' . $extension;
	
						if ($this->widthLarge && $this->heightLarge) {
							$image = Image::make($file)->resize($this->widthLarge, $this->heightLarge)->encode($extension);
							Storage::disk('s3')->put($pathLogo . $imageName, (string)$image, 'public');
						}
	
						$imageLogo = new ImageMdl;
						$imageLogo->type			= 1;
						$imageLogo->title			= $request->fantasy_name;
						$imageLogo->file			= $imageName;
						$imageLogo->is_active		= 1;
						$imageLogo->companies_id	= $this->companiesId;
						$imageLogo->save();
		
					}
	
				}

				if ($request->file('image_banner')) {

					if ($request->hasFile('image_banner')) {
		
						# folder
						$pathBanner = $this->folderUpload .  $this->companiesId . '/';

						
		
						# update the images table by "deleting" the image
						if ($request->image_banner_id) {

							$deleteBannerOld = ImageMdl::find($request->image_banner_id);
							if ($deleteBannerOld->exists()) {
		
								$deleteBannerOld->delete();
		
								# delete the image if it exists in the folder "LARGE"
								if (Storage::disk('s3')->exists($pathBanner . $request->image_banner_old)) {
									# delete the S3 file
									Storage::disk('s3')->delete($pathBanner . $request->image_banner_old);
								}
		
							}

						}
	
						$file		= $request->file('image_banner');
						$extension	= $request->file('image_banner')->getClientOriginalExtension();
	
						# generates a new name for the image (md5)
						$imageName	= md5($slugCompany . time()) . '.' . $extension;
	
						if ($this->widthLargeBanner && $this->heightLargeBanner) {
							$image = Image::make($file)->resize($this->widthLargeBanner, $this->heightLargeBanner)->encode($extension);
							Storage::disk('s3')->put($pathBanner . $imageName, (string)$image, 'public');
						}
	
						$imageBanner = new ImageMdl;
						$imageBanner->type			= 2;
						$imageBanner->title			= $request->fantasy_name;
						$imageBanner->file			= $imageName;
						$imageBanner->is_active		= 1;
						$imageBanner->companies_id	= $this->companiesId;
						$imageBanner->save();
		
					}
	
				}

				$code = 201;
				$response = [
					'message_action'	=> 'success',
					'message'			=> 'Dados alterados com sucesso!',
					'redirect'			=> route('control.account.edit'),
				];

			} else {

				$code = 409;
				$response = [
					'message_action'	=> 'danger',
					'message'			=> 'Não foi possível alterar os dados!'
				];

			}

		}

		return response()->json($response, $code);
    }
    
}
