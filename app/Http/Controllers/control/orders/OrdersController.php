<?php

namespace App\Http\Controllers\control\orders;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Customer;
use App\Models\City;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\PaymentForm;
use App\Models\State;

use DateTime;
use DB;
use SettingsHelperControl;

class OrdersController extends Controller
{
	protected $arrCss;
	protected $arrJs;
	protected $companiesId	= NULL;
	protected $arrOrder		= NULL;

	public function __construct()
	{
		$this->arrCss	= SettingsHelperControl::getCss();
		$this->arrJs	= SettingsHelperControl::getJs();

		array_push($this->arrJs, 'control/js/bootstrap-switch.min.js');
		array_push($this->arrJs, 'control/js/fileinput.js');
		array_push($this->arrJs, 'control/js/jquery.dataTables.min.js');
		array_push($this->arrJs, 'control/js/dataTables.bootstrap.js');
		array_push($this->arrJs, 'control/js/jquery.validate.min.js');
		array_push($this->arrJs, 'control/js/jquery.maskmoney.min.js');
		array_push($this->arrJs, 'control/prop/pattern/pattern.js');
		array_push($this->arrJs, 'control/prop/products/products.js');

		$this->companiesId	= Session::get('company_id');
		
	}

	public function index()
	{
		$this->arrOrder = $this->arrayOrders();

		return view('control.orders.index', [
			'arrCss'	=> $this->arrCss,
			'arrJs'		=> $this->arrJs,
			'arrOrder'	=> $this->arrOrder,
		]);
	}


	public function detail($id)
	{
		$this->arrOrder = $this->arrayOrders($id);

		return view('control.orders.detail', [
			'arrCss'	=> $this->arrCss,
			'arrJs'		=> $this->arrJs,
			'id'		=> $id,
			'arrOrder'	=> $this->arrOrder,
		]);
	}


	public function print($id)
	{
		$this->arrOrder = $this->arrayOrders($id);

		return view('control.orders.print', [
			'arrCss'	=> $this->arrCss,
			'arrJs'		=> $this->arrJs,
			'id'		=> $id,
			'arrOrder'	=> $this->arrOrder,
		]);
	}


	public function arrayOrders($id=NULL)
	{
		$html = [];

		# orders
		$orders = Order::where('orders.companies_id', $this->companiesId)
						 ->orderBy('orders.id', 'desc');
		if ($id) {
			$orders->where('orders.id', (int)$id);
		}
		if ($orders->exists()) {

			foreach ($orders->get() as $roword) {

				# customer
				$customer = Customer::where('id', $roword->customers_id)->first();
				
				# customer
				$payment = PaymentForm::where('id', $roword->payments_form_id)->first();

				# address
				if ($roword->addresses_id) {

					$addresses = address::where('id', $roword->addresses_id);
					if ($addresses->exists()) {
						
						$row = $addresses->first();

						# city
						$city	= City::where('id', $row->cities_id)->first();
						# state
						$state	= State::where('id', $row->states_id)->first();

						# mount address
						$addressCustomer  = $row->address . ", " . $row->number . " " . (($row->complement) ? $row->complement : NULL) . " " . (($row->zip_code) ? $row->zip_code : NULL);
						$addressCustomer .= "\n". $row->neighborhood . ", " . $city->title . "/" . $state->title;

					}
				
				} else { $addressCustomer = 'Retirar no balcão';}

				$arrProducts = [];

				# products
				$products = OrderProduct::where('orders_id', $roword->id)
										->leftJoin('products', function ($join) {
											$join->on('products.id', '=', 'orders_products.products_id');
										})
										->select(
											'orders_products.id',
											'orders_products.quantity',
											'orders_products.value_unitary',
											'orders_products.comments',
											'products.title'
										);
				if ($products->exists()) {

					foreach ($products->get() as $rowpro) {

						$arrProducts[$rowpro->id] = array(
							'title'			=> $rowpro->title,
							'quantity'		=> $rowpro->quantity,
							'value_unitary'	=> number_format($rowpro->value_unitary, 2, ',', '.'),
							'comments'		=> $rowpro->comments,
						);

					}

				}

				$data = new DateTime($roword->create_at);

				$html[$roword->id] = array(
					'customer'				=> $customer->name,
					'customer_email'		=> $customer->email,
					'customer_phone'		=> $customer->phone,
					'customer_cellphone'	=> $customer->cell_phone,
					'payment_form_id'		=> $payment->id,
					'payment_form'			=> $payment->title,
					'value_amount'			=> number_format($roword->amount, 2, ',', '.'),
					'value_delivery'		=> (($roword->value_delivery) ? number_format($roword->value_delivery, 2, ',', '.') : NULL),
					'value_change'			=> (($roword->value_change) ? number_format($roword->value_change, 2, ',', '.') : NULL),
					'address'				=> $roword->addresses_id,
					'addressComplete'		=> (($addressCustomer) ? $addressCustomer : NULL),
					'comments'				=> $roword->comments,
					'date'					=> $data->format('d/m/Y H:i'),
					'products'				=> (($arrProducts) ? $arrProducts : NULL),
				);

			}

		}

		return json_encode($html);
	}
}
