<?php

namespace App\Http\Controllers\control\login;

use App\Http\Controllers\Controller;
use App\Models\CompanyUser;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

use Auth;
use SettingsHelperControl;

class LoginController extends Controller
{
	protected $arrCss;
	protected $arrJs;

	public function __construct()
	{
		$this->arrCss = array(
			'control/css/bootstrap/bootstrap.min.css',
			'control/css/skin.css',
			'control/css/font-awesome.min.css',
			'control/css/ionicons.min.css',
			'control/css/pattern.css',
		);

		$this->arrJs = array(
			'control/js/jquery/jquery.min.js',
			'control/js/bootstrap/bootstrap.min.js',
			'control/js/app.min.js',
			'control/plugins/slimscroll/jquery.slimscroll.min.js',
			'control/prop/login/login.js',
		);
	}

	public function index()
	{
		return view('control.login.index', [
			'arrCss'	=> $this->arrCss,
			'arrJs'		=> $this->arrJs
		]);
	}


	public function authenticate(Request $request)
	{
		$validator = Validator::make(
			$request->all(),
			[
				'email'		=> 'required|email',
				'password'	=> 'required',
			],
			[
				'required'	=> 'Campos obrigatórios',
				'email'		=> 'Informe um e-mail válido',
			]
        );

        if ($validator->fails()) {
			
			$code = 409;
			$response = $validator->messages();

		} else {

			$hashedPassword = md5($request->password);

			$credentials = array(
				'email'		=> $request->email,
				'password'	=> $hashedPassword,
				'is_active'	=> 1,
			);

			if (Auth::guard('companies')->attempt($credentials)) {

				$user = CompanyUser::where('email', $request->email)
									 ->where('is_active', 1)->get();

				# cria a sessão
				$request->session()->put('company_id', $user[0]->companies_id);
				$request->session()->put('company_name', $user[0]->name);
				$request->session()->put('company_email', $user[0]->email);

				$code = 200;
				$response = [
					'message_action'	=> 'success',
					'message'			=> 'Redirecionando...',
					'redirect'			=> route('control.home'),
				];

			} else {

				$code = 409;
				$response = [
					'message_action'	=> 'danger',
					'danger'			=> 'Dados inválidos!'
				];

			}

		}

		return response()->json($response, $code);
	}


	public function logout(Request $request)
	{
		Auth::guard('companies')->logout();

		$request->session()->forget('company_id');
		$request->session()->forget('company_name');
		$request->session()->forget('company_email');

		return redirect()->route('control.login');
	}
}
