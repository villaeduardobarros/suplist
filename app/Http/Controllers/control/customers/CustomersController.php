<?php

namespace App\Http\Controllers\control\customers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Order;

use DB;
use SettingsHelperControl;

class CustomersController extends Controller
{
	protected $arrCss;
	protected $arrJs;
	protected $companiesId = NULL;

	public function __construct()
	{
		$this->arrCss	= SettingsHelperControl::getCss();
		$this->arrJs	= SettingsHelperControl::getJs();

		array_push($this->arrJs, 'control/js/bootstrap-switch.min.js');
		array_push($this->arrJs, 'control/js/fileinput.js');
		array_push($this->arrJs, 'control/js/jquery.dataTables.min.js');
		array_push($this->arrJs, 'control/js/dataTables.bootstrap.js');
		array_push($this->arrJs, 'control/js/jquery.validate.min.js');
		array_push($this->arrJs, 'control/js/jquery.maskmoney.min.js');
		array_push($this->arrJs, 'control/prop/pattern/pattern.js');
		array_push($this->arrJs, 'control/prop/customers/customers.js');

		$this->companiesId = Session::get('company_id');
	}

	public function index()
	{
		# customers
		$customers = DB::select("select 
							customers.*, 
							(select date_format(o2.created_at, '%d/%m/%Y %H:%i:%s') 
								from orders o2 
								where o2.customers_id = o1.customers_id 
								order by o2.created_at desc 
								limit 1) last_wish
						from orders o1 
						inner join customers on customers.id = o1.customers_id
						where 
							o1.companies_id = " . $this->companiesId . " 
						group by o1.customers_id 
						order by last_wish desc");

		return view('control.customers.index', [
			'arrCss'	=> $this->arrCss,
			'arrJs'		=> $this->arrJs,
			'customers'	=> $customers,
		]);
	}
}
