<?php

namespace App\Http\Controllers\control\products;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Http\Controllers\Controller;
use App\Models\Category;

use DB;
use Redirect;
use SettingsHelperControl;

class CategoriesController extends Controller
{
	protected $arrCss;
	protected $arrJs;
	protected $typeCategory = 2;

	protected $companiesId = NULL;

	public function __construct()
	{
		$this->arrCss	= SettingsHelperControl::getCss();
		$this->arrJs	= SettingsHelperControl::getJs();

		array_push($this->arrJs, 'control/js/bootstrap-switch.min.js');
		array_push($this->arrJs, 'control/js/fileinput.js');
		array_push($this->arrJs, 'control/js/jquery.dataTables.min.js');
		array_push($this->arrJs, 'control/js/dataTables.bootstrap.js');
		array_push($this->arrJs, 'control/js/jquery.validate.min.js');
		array_push($this->arrJs, 'control/js/jquery.maskmoney.min.js');
		array_push($this->arrJs, 'control/prop/pattern/pattern.js');
		array_push($this->arrJs, 'control/prop/products/products-categories.js');

		$this->companiesId = Session::get('company_id');
	}
	
	public function index()
    {		
		# categories
		$categories = Category::where('type', $this->typeCategory)
								->where('companies_id', $this->companiesId)
								->orderBy('title', 'asc')
								->get();

		return view('control.products.categories', [
			'arrCss'		=> $this->arrCss,
			'arrJs'			=> $this->arrJs,
			'action'		=> 'add',
			'categories'	=> $categories,
		]);
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
	{		
        $validator = validator($request->all(), [
			'title' => 'required',
		]);

        if ($validator->fails()) {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Preencha todos os campos obrigatórios!'
			];
			
		}

		$categorySlug = str_slug(request()->title, '-');
		
		$categories = Category::where('type', $this->typeCategory)
								->where('slug', $categorySlug)
								->where('companies_id', $this->companiesId);
		if ($categories->exists()) {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Este título já esta sendo utilizado!'
			];

		} else {

			$category = new Category;
			$category->type				= $this->typeCategory;
			$category->title			= $request->title;
			$category->slug				= $categorySlug;
			$category->description		= $request->description;
			$category->is_featured		= (($request->is_featured == 1) ? 1 : NULL);
			$category->is_active		= (($request->is_active == 1) ? 1 : NULL);
			$category->companies_id		= $this->companiesId;
			
			if ($category->save()) {

				$code = 201;
				$response = [
					'message_action'	=> 'success',
					'message'			=> 'Categoria cadastrada com sucesso!',
					'redirect'			=> route('control.products.categories'),
				];

			} else {

				$code = 409;
				$response = [
					'message_action'	=> 'danger',
					'message'			=> 'Não foi possível cadastrar a categoria!'
				];

			}

		}

		return response()->json($response, $code);
	}


	public function edit($id)
	{
		# categoria selecionado
		$category = Category::where('id', $id)
							  ->where('type', $this->typeCategory)
							  ->where('companies_id', $this->companiesId);
		if ($category->exists()) {

			$categorySelect = $category->first();

			# categories
			$categories = Category::where('type', $this->typeCategory)
									->where('companies_id', $this->companiesId)
									->orderBy('title', 'asc')
									->get();

			return view('control.products.categories', [
				'arrCss'			=> $this->arrCss,
				'arrJs'				=> $this->arrJs,
				'action'			=> 'edit',
				'categories'		=> $categories,
				'categorySelect'	=> $categorySelect,
			]);

		} else { return redirect()->route('control.products.categories'); }
	}


	public function update(Request $request)
	{		
        $validator = validator($request->all(), [
			'title' => 'required',
		]);

        if ($validator->fails()) {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Preencha todos os campos obrigatórios!'
			];

		}

		$categorySlug = str_slug($request->title, '-');

		$update = Category::findOrFail($request->id);

		# params
		$update->type			= $this->typeCategory;
		$update->title			= $request->title;
		$update->slug			= $categorySlug;
		$update->description	= $request->description;
		$update->is_featured	= (($request->is_featured == 1) ? 1 : NULL);
		$update->is_active		= (($request->is_active == 1) ? 1 : NULL);
		$update->companies_id	= $this->companiesId;

		if ($update->save()) {

			$code = 201;
			$response = [
				'message_action'	=> 'success',
				'message'			=> 'Categoria cadastrada com sucesso!',
				'redirect'			=> route('control.products.categories'),
			];

		} else {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Não foi possível cadastrar a categoria!'
			];

		}

		return response()->json($response, $code);
	}


	public function destroy($id)
	{
		$category = Category::find($id);
		if ($category) {

			$destroy = Category::destroy($id);

		}

		return Redirect::to(route('control.products.categories'))
						 ->header('Cache-Control', 'no-store, no-cache, must-revalidate');
		
	}

}