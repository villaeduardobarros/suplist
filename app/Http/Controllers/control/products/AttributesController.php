<?php

namespace App\Http\Controllers\control\products;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

use App\Http\Controllers\Controller;
use App\Models\Attribute;

use DB;
use Redirect;
use SettingsHelperControl;

class AttributesController extends Controller
{
	protected $arrCss;
	protected $arrJs;
	protected $companiesId = NULL;

	public function __construct()
	{
		$this->arrCss	= SettingsHelperControl::getCss();
		$this->arrJs	= SettingsHelperControl::getJs();

		array_push($this->arrJs, 'control/js/bootstrap-switch.min.js');
		array_push($this->arrJs, 'control/js/fileinput.js');
		array_push($this->arrJs, 'control/js/jquery.dataTables.min.js');
		array_push($this->arrJs, 'control/js/dataTables.bootstrap.js');
		array_push($this->arrJs, 'control/js/jquery.validate.min.js');
		array_push($this->arrJs, 'control/js/jquery.maskmoney.min.js');
		array_push($this->arrJs, 'control/prop/pattern/pattern.js');
		array_push($this->arrJs, 'control/prop/products/products-attibutes.js');

		$this->companiesId = Session::get('company_id');
	}
	
	public function index()
    {		
		# attributes
		$attributes = Attribute::where('companies_id', $this->companiesId)
								 ->get();

		return view('control.products.attributes', [
			'arrCss'		=> $this->arrCss,
			'arrJs'			=> $this->arrJs,
			'action'		=> 'add',
			'attributes'	=> $attributes,
		]);
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
	{		
        $validator = Validator($request->all(), [
			'title' => 'required',
		]);

        if ($validator->fails()) {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Preencha todos os campos obrigatórios!'
			];
			
		}

		$attibuteSlug = str_slug($request->title, '-');
		
		$attributes = Attribute::where('slug', $attibuteSlug)
								 ->where('companies_id', $this->companiesId);
		if ($attributes->exists()) {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Este título já está sendo utilizado!'
			];

		} else {

			$attibute = new Attribute;
			$attibute->title		= $request->title;
			$attibute->slug			= $attibuteSlug;
			$attibute->quantity		= $request->quantity;
			$attibute->is_active	= (($request->is_active == 1) ? 1 : NULL);
			$attibute->companies_id	= $this->companiesId;

			if ($attibute->save()) {

				$code = 201;
				$response = [
					'message_action'	=> 'success',
					'message'			=> 'Atributo cadastrado com sucesso!',
					'redirect'			=> route('control.products.attributes'),
				];

			} else {

				$code = 409;
				$response = [
					'message_action'	=> 'danger',
					'message'			=> 'Não foi possível cadastrar o atributo!'
				];

			}

		}

		return response()->json($response, $code);
	}


	public function edit($id)
	{
		# attribute selected
		$attribute = Attribute::where('id', $id)
								->where('companies_id', $this->companiesId);
		if ($attribute->exists()) {

			$attributeSelect = $attribute->first();

			# attributes
			$attributes = Attribute::where('companies_id', $this->companiesId)
									 ->get();

			return view('control.products.attributes', [
				'arrCss'			=> $this->arrCss,
				'arrJs'				=> $this->arrJs,
				'action'			=> 'edit',
				'attributes'		=> $attributes,
				'attributeSelect'	=> $attributeSelect,
			]);

		} else {

			return Redirect::to(route('control.products.attributes'))
							 ->header('Cache-Control', 'no-store, no-cache, must-revalidate');
							 
		}
	}


	public function update(Request $request)
	{		
        $validator = Validator($request->all(), [
			'title' => 'required',
		]);

        if ($validator->fails()) {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Preencha todos os campos obrigatórios!'
			];

		}

		$attibuteSlug = str_slug($request->title, '-');

		$update = Attribute::findOrFail($request->id);

		# params
		$update->title			= $request->title;
		$update->slug			= $attibuteSlug;
		$update->quantity		= $request->quantity;
		$update->is_active		= (($request->is_active == 1) ? 1 : NULL);
		$update->companies_id	= $this->companiesId;

		if ($update->save()) {

			$code = 201;
			$response = [
				'message_action'	=> 'success',
				'message'			=> 'Atributo alterado com sucesso!',
				'redirect'			=> route('control.products.attributes'),
			];

		} else {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Não foi possível alterar o attribute!'
			];

		}

		return response()->json($response, $code);
	}


	public function destroy($id)
	{
		$attribute = Attribute::find($id);
		if ($attribute->exists()) {

			$attribute->delete();

		}

		return Redirect::to(route('control.products.attributes'))
						 ->header('Cache-Control', 'no-store, no-cache, must-revalidate');
		
	}

}
