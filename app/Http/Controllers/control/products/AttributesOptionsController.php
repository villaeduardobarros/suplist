<?php

namespace App\Http\Controllers\control\products;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\AttributeOption;

use DB;
use Redirect;
use SettingsHelperControl;

class AttributesOptionsController extends Controller
{
	protected $arrCss;
	protected $arrJs;
	protected $companiesId = NULL;
	protected $attributeId = NULL;

	public function __construct()
	{
		$this->arrCss	= SettingsHelperControl::getCss();
		$this->arrJs	= SettingsHelperControl::getJs();

		array_push($this->arrJs, 'control/js/bootstrap-switch.min.js');
		array_push($this->arrJs, 'control/js/fileinput.js');
		array_push($this->arrJs, 'control/js/jquery.dataTables.min.js');
		array_push($this->arrJs, 'control/js/dataTables.bootstrap.js');
		array_push($this->arrJs, 'control/js/jquery.validate.min.js');
		array_push($this->arrJs, 'control/js/jquery.maskmoney.min.js');
		array_push($this->arrJs, 'control/prop/pattern/pattern.js');
		array_push($this->arrJs, 'control/prop/products/products-attibutes.js');

		$this->companiesId = Session::get('company_id');
	}
	
	public function index($attribute_id)
    {
		# attributes
		$attributes = Attribute::where('companies_id', $this->companiesId)
								 ->get();

		# attributes options
		$attributesOptions = AttributeOption::where('attributes_id', $attribute_id)
											  ->get();

		return view('control.products.attributes-options', [
			'arrCss'			=> $this->arrCss,
			'arrJs'				=> $this->arrJs,
			'action'			=> 'add',
			'attributes'		=> $attributes,
			'attributeId'		=> $attribute_id,
			'attributesOptions'	=> $attributesOptions,
		]);
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
	{		
        $validator = Validator($request->all(), [
			'title' => 'required',
		]);

        if ($validator->fails()) {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Preencha todos os campos obrigatórios!'
			];
			
		}

		$attibuteOptionSlug = str_slug($request->title, '-');
		
		$options = AttributeOption::where('slug', $attibuteOptionSlug)
									->where('attributes_id', $request->attribute_id);
		if ($options->exists()) {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Este título já está sendo utilizado!'
			];

		} else {

			$attributesOptions = new AttributeOption;
			$attributesOptions->title			= $request->title;
			$attributesOptions->slug			= $attibuteOptionSlug;
			$attributesOptions->description		= $request->description;
			$attributesOptions->is_active		= (($request->is_active == 1) ? 1 : NULL);
			$attributesOptions->attributes_id	= $request->attribute_id;

			if ($attributesOptions->save()) {

				$code = 201;
				$response = [
					'message_action'	=> 'success',
					'message'			=> 'Opção cadastrada com sucesso!',
					'redirect'			=> route('control.products.attributes.options', $request->attribute_id),
				];

			} else {

				$code = 409;
				$response = [
					'message_action'	=> 'danger',
					'message'			=> 'Não foi possível cadastrar a opção!'
				];

			}

		}

		return response()->json($response, $code);
	}


	public function edit($attribute_id, $id)
	{
		# attribute selected
		$options = AttributeOption::where('id', $id)
									->where('attributes_id', $attribute_id);
		if ($options->exists()) {

			$optionsSelect = $options->first();

			# attributes
			$attributes = Attribute::where('companies_id', $this->companiesId)
									 ->get();

			# attributes options
			$attributesOptions = AttributeOption::where('attributes_id', $attribute_id)
												  ->get();

			return view('control.products.attributes-options', [
				'arrCss'			=> $this->arrCss,
				'arrJs'				=> $this->arrJs,
				'action'			=> 'edit',
				'optionsSelect'		=> $optionsSelect,
				'attributes'		=> $attributes,
				'attributeId'		=> $attribute_id,
				'attributesOptions'	=> $attributesOptions,
			]);

		} else {
			
			return Redirect::to(route('control.products.attributes.options', $attribute_id))
						 	 ->header('Cache-Control', 'no-store, no-cache, must-revalidate');
							
		}
	}


	public function update(Request $request)
	{		
        $validator = Validator($request->all(), [
			'title' => 'required',
		]);

        if ($validator->fails()) {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Preencha todos os campos obrigatórios!'
			];

		}

		$attibuteOptionSlug = str_slug($request->title, '-');

		# params
		$update = AttributeOption::findOrFail($request->id);
		$update->title			= $request->title;
		$update->slug			= $attibuteOptionSlug;
		$update->description	= $request->description;
		$update->is_active		= (($request->is_active == 1) ? 1 : NULL);
		$update->attributes_id	= $request->attribute_id;
		if ($update->save()) {

			$code = 201;
			$response = [
				'message_action'	=> 'success',
				'message'			=> 'Opção alterada com sucesso!',
				'redirect'			=> route('control.products.attributes.options', $request->attribute_id),
			];

		} else {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Não foi possível alterar a opção!'
			];

		}	

		return response()->json($response, $code);
	}


	public function destroy($attribute_id, $id)
	{
		$options = AttributeOption::find($id);
		if ($options->exists()) {

			$options->delete();

		}

		return Redirect::to(route('control.products.attributes.options', $attribute_id))
						 ->header('Cache-Control', 'no-store, no-cache, must-revalidate');
		
	}

}
