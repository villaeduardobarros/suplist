<?php

namespace App\Http\Controllers\control\products;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\AttributeOption;
use App\Models\Category;
use App\Models\ImageMdl;
use App\Models\Product;
use App\Models\Variation;
use App\Models\VariationOption;

use DB;
use Image;
use Redirect;
use SettingsHelperControl;

class ProductsController extends Controller
{
	protected $arrCss;
	protected $arrJs;
	protected $typeCategory = 2;

	# upload image
	protected $folderUpload;
	protected $widthLarge;
	protected $heightLarge;
	protected $widthMedium;
	protected $heightMedium;
	protected $widthThumb;
	protected $heightThumb;

	protected $companiesId = NULL;

	public function __construct()
	{
		$this->arrCss		= SettingsHelperControl::getCss();
		$this->arrJs		= SettingsHelperControl::getJs();

		# upload image
		$this->folderUpload = SettingsHelperControl::searchItem('folder-upload-products');
		$this->widthLarge	= SettingsHelperControl::searchItem('width-large-products');
		$this->heightLarge	= SettingsHelperControl::searchItem('height-large-products');
		$this->widthMedium	= SettingsHelperControl::searchItem('width-medium-products');
		$this->heightMedium	= SettingsHelperControl::searchItem('height-medium-products');
		$this->widthThumb	= SettingsHelperControl::searchItem('width-thumb-products');
		$this->heightThumb	= SettingsHelperControl::searchItem('height-thumb-products');

		$this->companiesId = Session::get('company_id');
	}

	public function index()
	{
		array_push($this->arrJs, 'control/js/jquery.dataTables.min.js');
		array_push($this->arrJs, 'control/js/dataTables.bootstrap.js');
		array_push($this->arrJs, 'control/prop/products/products.js');

		# products
		$products = Product::leftJoin('categories as cat', function ($join) {
								$join->on('cat.id', '=', 'products.categories_id');
								$join->on('cat.type', '=', DB::raw($this->typeCategory));
								$join->on('cat.is_active', '=', DB::raw('1'));
							 })
							 ->leftJoin('images', function ($join) {
								$join->on('images.products_id', '=', 'products.id');
								$join->where('images.type', DB::raw("3"));
							 })
							 ->where('products.companies_id', $this->companiesId)
							 ->select(
								'products.*',
								'images.file',
								'cat.title as titleCategory',
								'cat.slug as slugCategory'
							 )
							 ->get();
		//dd($products->toSql(), $products->getBindings());

		# categories
		$categories = Category::where('type', $this->typeCategory)
								->where('companies_id', $this->companiesId)
								->orderBy('title', 'asc')
								->get();

		return view('control.products.index', [
			'arrCss'		=> $this->arrCss,
			'arrJs'			=> $this->arrJs,
			'folderUpload'	=> $this->folderUpload,
			'products'		=> $products,
			'categories'	=> $categories,
		]);
	}


	public function create()
	{
		array_push($this->arrJs, 'control/js/bootstrap-switch.min.js');
		array_push($this->arrJs, 'control/js/fileinput.js');
		array_push($this->arrJs, 'control/js/jquery.validate.min.js');
		array_push($this->arrJs, 'control/js/jquery.maskmoney.min.js');
		array_push($this->arrJs, 'control/prop/pattern/pattern.js');
		array_push($this->arrJs, 'control/prop/products/products.js');

		# categories
		$categories = Category::where('type', $this->typeCategory)
								->where('companies_id', $this->companiesId)
								->orderBy('title', 'asc')
								->get();

		# attributes
		$attributes = Attribute::where('is_active', 1)
								 ->where('companies_id', $this->companiesId)
								 ->orderBy('title', 'asc')
								 ->get();

		return view('control.products.form', [
			'arrCss'		=> $this->arrCss,
			'arrJs'			=> $this->arrJs,
			'action'		=> 'add',
			'folderUpload'	=> $this->folderUpload,
			'widthLarge'	=> $this->widthLarge,
			'heightLarge'	=> $this->heightLarge,
			'attributes'	=> $attributes,
			'categories'	=> $categories,
		]);
	}


	public function store(Request $request)
	{
		$validator = Validator($request->all(), [
			'category'		=> 'required',
			'title'			=> 'required',
			'value'			=> 'required',
			'description'	=>	'required',
		]);

		if ($validator->fails()) {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Preencha todos os campos obrigatórios!'
			];
		}

		$productSlug = str_slug($request->title, '-');

		$checkOnly = Product::where('slug', $productSlug)
							  ->where('companies_id', $this->companiesId);
		if ($checkOnly->exists()) {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Este título já esta sendo utilizado!'
			];

		} else {

			$product = new Product;
			$product->type					= $request->type;
			$product->title					= $request->title;
			$product->slug					= $productSlug;
			$product->description			= $request->description;
			$product->description_packing	= $request->description_packing;
			$product->value					= (($request->value) ? (($request->value == '0,00') ? NULL : str_replace(',', '.', str_replace('.', '', $request->value))) : NULL);
			$product->value_promotional		= (($request->value_promotional) ? (($request->value_promotional == '0,00') ? NULL : str_replace(',', '.', str_replace('.', '', $request->value_promotional))) : NULL);
			$product->is_featured			= (($request->is_featured == 1) ? 1 : NULL);
			$product->is_active				= (($request->is_active == 1) ? 1 : NULL);
			$product->companies_id			= $this->companiesId;
			$product->categories_id			= $request->category;

			if ($product->save()) {

				$redirect = (($request->type == 1) ? route('control.products') : route('control.products.edit', ['id' => $product->id]));

				if ($request->file('image')) {

					if ($request->hasFile('image')) {
		
						# folders
						$pathLarge	= $this->folderUpload . 'large/' . $this->companiesId . '/' . $product->id . '/';
						$pathMedium	= $this->folderUpload . 'medium/' . $this->companiesId . '/' . $product->id . '/';
						$pathThumb	= $this->folderUpload . 'thumb/' . $this->companiesId . '/' . $product->id . '/';

						$file		= $request->file('image');
						$extension	= $request->file('image')->getClientOriginalExtension();
	
						# generates a new name for the image (md5)
						$imageName	= md5($productSlug . time()) . '.' . $extension;
	
						if ($this->widthLarge && $this->heightLarge) {
							$large = Image::make($file)->resize($this->widthLarge, $this->heightLarge)->encode($extension);
							Storage::disk('s3')->put($pathLarge . $imageName, (string)$large, 'public');
						}
	
						if ($this->widthMedium && $this->heightMedium) {
							$medium = Image::make($file)->resize($this->widthMedium, $this->heightMedium)->encode($extension);
							Storage::disk('s3')->put($pathMedium . $imageName, (string)$medium, 'public');
						}
	
						if ($this->widthThumb && $this->heightThumb) {
							$thumbnail = Image::make($file)->resize($this->widthThumb, $this->heightThumb)->encode($extension);
							Storage::disk('s3')->put($pathThumb . $imageName, (string)$thumbnail, 'public');
						}
	
						$image = new ImageMdl;
						$image->type		= 3;
						$image->title		= $request->title;
						$image->file		= $imageName;
						$image->is_active	= 1;
						$image->products_id	= $product->id;
						
						if ($image->save()) {
	
							$code = 201;
							$response = [
								'message_action'	=> 'success',
								'message'			=> 'Produto cadastrado com sucesso!',
								'redirect'			=> $redirect,
							];
	
						} else {
	
							$code = 409;
							$response = [
								'message_action'	=> 'danger',
								'message'			=> 'Não foi possível cadastrar a imagem!'
							];
	
						}
		
					}
	
				} else {
	
					$code = 201;
					$response = [
						'message_action'	=> 'success',
						'message'			=> 'Produto cadastrado com sucesso!',
						'redirect'			=> $redirect,
					];
	
				}

			} else {

				$code = 409;
				$response = [
					'message_action'	=> 'danger',
					'message'			=> 'Não foi possível cadastrar o produto!'
				];
				
			}
		}

		return response()->json($response, $code);
	}


	public function show($id)
	{
		//
	}


	public function edit($id)
	{
		array_push($this->arrJs, 'control/js/bootstrap-switch.min.js');
		array_push($this->arrJs, 'control/js/fileinput.js');
		array_push($this->arrJs, 'control/js/jquery.validate.min.js');
		array_push($this->arrJs, 'control/js/jquery.maskmoney.min.js');
		array_push($this->arrJs, 'control/prop/pattern/pattern.js');
		array_push($this->arrJs, 'control/prop/products/products.js');
		array_push($this->arrJs, 'control/prop/modal/modal-products.js');

		# categoria selecionado
		$product = Product::leftJoin('images', 'images.products_id', '=', 'products.id')
							->where('products.id', $id)
							->where('products.companies_id', $this->companiesId)
							->select(
								'products.*',
								'images.id as idImage',
								'images.file'
							);
		if ($product->exists()) {

			$productSelect = $product->first();

			# products
			$products = Product::leftJoin('categories as cat', function ($join) {
									$join->on('cat.id', '=', 'products.categories_id');
									$join->on('cat.type', '=', DB::raw($this->typeCategory));
									$join->on('cat.is_active', '=', DB::raw("1"));
								 })
								 ->leftJoin('images', function ($join) {
									$join->on('images.products_id', '=', 'products.id');
									$join->where('images.type', DB::raw("3"));
								 })
								 ->where('products.companies_id', $this->companiesId)
								 ->where('products.is_active', 1)
								 ->select(
									'products.*',
									'images.file',
									'cat.title as titleCategory',
									'cat.slug as slugCategory'
								 )
								 ->get();

			# categories
			$categories = Category::where('type', $this->typeCategory)
									->where('companies_id', $this->companiesId)
									->orderBy('title', 'asc')
									->get();

			# attributes
			$attributes = Attribute::where('is_active', 1)
									 ->where('companies_id', $this->companiesId)
									 ->orderBy('title', 'asc')
									 ->get();
			# attributes
			$variations = VariationOption::select(
												'attributes_id',
												DB::raw('GROUP_CONCAT(attributes_options_id) AS Users')
										   )
										   ->groupBy('attributes_id')
										   ->where('products_id', $id)
										   ->get();

			return view('control.products.form', [
				'arrCss'		=> $this->arrCss,
				'arrJs'			=> $this->arrJs,
				'action'		=> 'edit',
				'folderUpload'	=> $this->folderUpload,
				'widthLarge'	=> $this->widthLarge,
				'heightLarge'	=> $this->heightLarge,
				'productSelect'	=> $productSelect,
				'products'		=> $products,
				'categories'	=> $categories,
				'attributes'	=> $attributes,
				'variations'	=> $variations,				
			]);

		} else {
			
			return Redirect::to(route('control.products'))
							 ->header('Cache-Control', 'no-store, no-cache, must-revalidate');
							
		}
		
	}


	public function showVariations(Request $request)
	{
		$attribute = Attribute::where('is_active', 1)
								->where('id', $request->attribute_id)
								->where('companies_id', $this->companiesId)
								->first();

		$attributesOptions = AttributeOption::where('attributes_id', $request->attribute_id)
											  ->get();
		
		$html = '<div class="form-group">
			<div class="row">
				<div class="col-md-5">
					<h4>' . $attribute->title . '</h4>
				</div>

				<div class="col-md-2" style="text-align:center;">
					<span style="margin:7px 0 0;float:left;">Quantidade de opções</span>
					<input type="text" class="form-control" style="width:36%;float:right;" maxlength="2">
				</div>

				<div class="col-md-5">
					<button type="button" class="btn btn-red btn-sm pull-right" onclick="">x</button>
				</div>
			</div>
		</div>';

		foreach ($attributesOptions as $option) {

			$html .= '<div class="form-group">
				<div class="row">
					<div class="col-md-4">
						<label><strong>' . $option->title . '</strong></label>
					</div>
					
					<div class="col-md-2">
						<label for="">Preço (R$)</label>
						<input type="text" class="form-control value">
					</div>
					
					<div class="col-md-2">
						<label for="">Preço Promocional (R$)</label>
						<input type="text" class="form-control value">
					</div>
					
					<div class="col-md-2">
						<label for="">SKU (Código Interno)</label>
						<input type="text" class="form-control">
					</div>
					
					<div class="col-md-2">
						<label for="">Observação</label>
						<input type="text" class="form-control">
					</div>
				</div>
			</div>';

		}

		$html .= '<hr>';

		return $html;
	}


	public function update(Request $request)
	{
		$validator = Validator($request->all(), [
			'category'		=> 'required',
			'title'			=> 'required',
			'value'			=> 'required',
			'description'	=> 'required',
			'type'			=> 'required',
		]);

		if ($validator->fails()) {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Preencha todos os campos obrigatórios!'
			];

		}

		$productSlug = str_slug($request->title, '-');

		$update = Product::findOrFail($request->id);

		# params
		$update->type					= $request->type;
		$update->title					= $request->title;
		$update->slug					= $productSlug;
		$update->description			= $request->description;
		$update->description_packing	= $request->description_packing;
		$update->value					= (($request->value) ? (($request->value == '0,00') ? NULL : str_replace(',', '.', str_replace('.', '', $request->value))) : NULL);
		$update->value_promotional		= (($request->value_promotional) ? (($request->value_promotional == '0,00') ? NULL : str_replace(',', '.', str_replace('.', '', $request->value_promotional))) : NULL);
		$update->is_featured			= (($request->is_featured == 1) ? 1 : NULL);
		$update->is_active				= (($request->is_active == 1) ? 1 : NULL);
		$update->companies_id			= $this->companiesId;
		$update->categories_id			= $request->category;

		if ($update->save()) {

			if ($request->file('image')) {

				if ($request->hasFile('image')) {
	
					# folders
					$pathLarge	= $this->folderUpload . 'large/' . $this->companiesId . '/' . $request->id . '/';
					$pathMedium	= $this->folderUpload . 'medium/' . $this->companiesId . '/' . $request->id . '/';
					$pathThumb	= $this->folderUpload . 'thumb/' . $this->companiesId . '/' . $request->id . '/';
	
					# update the images table by "deleting" the image
					$deleteOld = ImageMdl::where('file', $request->image_old);
					if ($deleteOld->exists()) {

						$deleteOld->delete();

						# delete the image if it exists in the folder "LARGE"
						if (Storage::disk('s3')->exists($pathLarge . $request->image_old)) {
							# delete the S3 file
							Storage::disk('s3')->delete($pathLarge . $request->image_old);
						}

						# delete the image if it exists in the folder "MEDIUM"
						if (Storage::disk('s3')->exists($pathMedium . $request->image_old)) {
							# delete the S3 file
							Storage::disk('s3')->delete($pathMedium . $request->image_old);
						}

						# delete the image if it exists in the folder "THUMB"
						if (Storage::disk('s3')->exists($pathThumb . $request->image_old)) {
							# delete the S3 file
							Storage::disk('s3')->delete($pathThumb . $request->image_old);
						}

					}


					$file		= $request->file('image');
					$extension	= $request->file('image')->getClientOriginalExtension();

					# generates a new name for the image (md5)
					$imageName	= md5($productSlug . time()) . '.' . $extension;

					if ($this->widthLarge && $this->heightLarge) {
						$large = Image::make($file)->resize($this->widthLarge, $this->heightLarge)->encode($extension);
						Storage::disk('s3')->put($pathLarge . $imageName, (string)$large, 'public');
					}

					if ($this->widthMedium && $this->heightMedium) {
						$medium = Image::make($file)->resize($this->widthMedium, $this->heightMedium)->encode($extension);
						Storage::disk('s3')->put($pathMedium . $imageName, (string)$medium, 'public');
					}

					if ($this->widthThumb && $this->heightThumb) {
						$thumbnail = Image::make($file)->resize($this->widthThumb, $this->heightThumb)->encode($extension);
						Storage::disk('s3')->put($pathThumb . $imageName, (string)$thumbnail, 'public');
					}

					// ************************************
					// $file = $request->file('image');

					// # generates a new name for the image (md5)
					// $imageName = md5($productSlug) . '.' . $request->image->extension();

					// # add the S3 file
					// Storage::disk('s3')->putFileAs($path, $file, $imageName, 'public');
					// ************************************

					$image = new ImageMdl;
					$image->type		= 3;
					$image->title		= $request->title;
					$image->file		= $imageName;
					$image->is_active	= 1;
					$image->products_id	= $request->id;

					if ($image->save()) {

						$code = 201;
						$response = [
							'message_action'	=> 'success',
							'message'			=> 'Produto alterado com sucesso!',
							'redirect'			=> route('control.products'),
						];

					} else {

						$code = 409;
						$response = [
							'message_action'	=> 'danger',
							'message'			=> 'Não foi possível alterar a imagem!'
						];

					}
	
				}

			} else {

				$code = 201;
				$response = [
					'message_action'	=> 'success',
					'message'			=> 'Produto alterado com sucesso!',
					'redirect'			=> route('control.products'),
				];

			}

		} else {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Não foi possível alterar o produto!'
			];

		}

		return response()->json($response, $code);
	}


	public function destroy($id)
	{
		$product = Product::find($id);
		if ($product->exists()) {

			# folders
			$pathLarge	= $this->folderUpload . 'large/' . $this->companiesId . '/' . $id . '/';
			$pathMedium	= $this->folderUpload . 'medium/' . $this->companiesId . '/' . $id . '/';
			$pathThumb	= $this->folderUpload . 'thumb/' . $this->companiesId . '/' . $id . '/';

			# delete the image
			$image = ImageMdl::where('products_id', $id)->first();
			if ($image->exists()) {

				# delete the image if it exists in the folder "LARGE"
				if (Storage::disk('s3')->exists($pathLarge . $image->file)) {
					# delete the S3 file
					Storage::disk('s3')->delete($pathLarge . $image->file);
				}

				# delete the image if it exists in the folder "MEDIUM"
				if (Storage::disk('s3')->exists($pathMedium . $image->file)) {
					# delete the S3 file
					Storage::disk('s3')->delete($pathMedium . $image->file);
				}

				# delete the image if it exists in the folder "THUMB"
				if (Storage::disk('s3')->exists($pathThumb . $image->file)) {
					# delete the S3 file
					Storage::disk('s3')->delete($pathThumb . $image->file);
				}

				# delete the record
				$image->delete();

			}

			# delete the record
			Product::destroy($id);
			
			return Redirect::to(route('control.products'))
							 ->header('Cache-Control', 'no-store, no-cache, must-revalidate');

		}
		
	}

}