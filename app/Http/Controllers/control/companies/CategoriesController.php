<?php

namespace App\Http\Controllers\control\companies;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Http\Controllers\Controller;
use App\Models\Category;

class CategoriesController extends Controller
{
	protected $arrCss;
	protected $arrJs;
	protected $type = 1;

	public function __construct()
	{
		$this->arrCss = array(
			'control/css/bootstrap/bootstrap.min.css',
			'control/css/skin.css',
			'control/css/font-awesome.min.css',
			'control/css/ionicons.min.css',
			'control/css/print.css',
			'control/css/agrichem.css',
			'control/css/pattern.css'
		);

		$this->arrJs = array(
			'control/js/jquery/jquery.min.js',
			'control/js/bootstrap/bootstrap.min.js',
			'control/js/app.min.js',
			'control/plugins/slimscroll/jquery.slimscroll.min.js'
		);
	}
	
	public function index()
	{
		if (!Auth::check()) {
			return redirect()->route('control.logout');
		}

		$data['arrJs']		= $this->arrJs;
		$data['arrCss']		= $this->arrCss;
		$data['action']		= 'add';

		# listagem
		$data['categories'] = Category::where('type', $this->type)->get();

		return view('control.companies.categories', $data);
	}


	public function create()
	{
		//
	}


	public function store(Request $request)
	{		
        $validator = validator($request->all(), [
			'title' => 'required',
		]);

        if ($validator->fails()) {
            return redirect()->back()
							 ->with('message-action', 'danger')
							 ->with('message', 'Preencha todos os campos obrigatórios!');
		}

		$slug = Str::slug(request()->title, '-');
		
		$categories = Category::where('type', $this->type)
								->where('slug', $slug);
		if ($categories->exists()) {

			return redirect()->back()
							 ->with('message-action', 'danger')
							 ->with('message', 'Categoria já cadastrada!');

		} else {

			$category = new Category;
			$category->type			= $this->type;
			$category->title		= $request->title;
			$category->slug			= $slug;
			$category->description	= $request->description;
			$category->is_featured	= (($request->is_featured == 1) ? 1 : NULL);
			$category->is_active	= (($request->is_active == 1) ? 1 : NULL);
			if ($category->save()) {

				return redirect()->back()
								->with('message-action', 'success')
								->with('message', 'Categoria cadastrada com sucesso!');

			} else {

				return redirect()->back()
								->with('message-action', 'danger')
								->with('message', 'Categoria não encontrada!');

			}

		}
	}


	public function edit(Request $request, $id)
	{
		$data['arrJs']		= $this->arrJs;
		$data['arrCss']		= $this->arrCss;
		$data['action']		= 'edit';

		# listagem
		$data['categories'] = Category::where('type', $this->type)->get();

		$category = Category::where('id', $id);
		if ($category->exists()) {

			# dados para preencher o form
			$data['category'] = $category->first();

			return view('control.companies.categories', $data);

		} else {

			return redirect()->back()
							->with('message-action', 'danger')
							->with('message', 'Categoria não encontrada!');

		}
	}


	public function update(Request $request, $id)
	{		
        $validator = validator($request->all(), [
			'title' => 'required',
		]);

        if ($validator->fails()) {
            return redirect()->back()
							 ->with('message-action', 'danger')
							 ->with('message', 'Preencha todos os campos obrigatórios!');
		}

		$slug = Str::slug(request()->title, '-');

		$update = Category::where('id', $id)->update([
			'type'			=> $this->type,
			'title'			=> $request->title,
			'slug'			=> $slug,
			'description'	=> $request->description,
			'is_featured'	=> (($request->is_featured == 1) ? 1 : NULL),
			'is_active'		=> (($request->is_active == 1) ? 1 : NULL),
		]);

		if ($update) {

			return redirect()->back()
							 ->with('message-action', 'success')
							 ->with('message', 'Categoria alterada com sucesso!');

		} else {

			return redirect()->back()
							 ->with('message-action', 'danger')
							 ->with('message', 'Não foi possível alterar a categoria!');
		
		}
	}


	public function destroy($id)
	{
		$category = Category::where('id', $id);
		if ($category) {

			$destroy = Category::where('id', $id)->update([
				'is_excluded' => 1,
			]);

		}

		if ($destroy) {

			return redirect()->back()
							 ->with('message-action', 'danger')
							 ->with('message', 'Categoria apagada com sucesso!');
		
		}else{
		
			return redirect()->back()
							 ->with('message-action', 'danger')
							 ->with('message', 'Não foi possível apagar a categoria!');
		
		}
	}

}
