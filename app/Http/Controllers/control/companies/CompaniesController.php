<?php

namespace App\Http\Controllers\control\companies;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{

    public function __construct()
    {
        #$this->middleware('auth');
    }

    public function index()
    {
        return view('companies/index');
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
    
}
