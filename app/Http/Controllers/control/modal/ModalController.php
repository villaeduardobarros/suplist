<?php

namespace App\Http\Controllers\control\modal;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\AttributeOption;
use App\Models\CompanyDistances;
use App\Models\Product;
use App\Models\VariationOption;

use DB;

class ModalController extends Controller
{
	protected $companiesId = NULL;

	public function __construct()
	{
		$this->companiesId = Session::get('company_id');
	}


	public function showAttributesOptions(Request $request)
	{
		# attributes
		$attributes = Attribute::where('companies_id', $this->companiesId)
								 ->orderBy('title', 'asc')
								 ->get();

		$product = Product::find($request->products_id);

		return view('control.modal.modal-variations-options', [
			'action'		=> 'add',
			'product'		=> $product,
			'attributes'	=> $attributes
		]);
	}


	public function changeAttributesOptions(Request $request)
	{
		$html = '<input type="hidden" name="product_id" value="' . $request->products_id . '">';

		$arrSel = NULL;
		$optionsSelected = VariationOption::where('products_id', $request->products_id)
											->where('attributes_id', $request->attributes_id)
											->select(DB::raw('group_concat(attributes_options_id) options'));
		if ($optionsSelected->exists()) {
		
			$rowopt = $optionsSelected->first();
			$arrSel = explode(',', $rowopt->options);

			$html .= '<div class="col-md-12">';

				$attributesOptions = AttributeOption::where('attributes_id', $request->attributes_id)->get();
				foreach ($attributesOptions as $option) {

					$value = $request->products_id . '|' . $request->attributes_id . '|' . $option->id;
					$html .= '<div class="checkbox">
						<label>
							<input type="checkbox" name="option[]" value="' . $value .'" ' . (($arrSel) ? ((in_array($option->id, $arrSel)) ? 'checked' : NULL) : NULL) . '> ' . $option->title . '
						</label>
					</div>';

				}

				$html .= '<label for="option[]" class="error" style="display:none;">Selecione pelo menos uma opção</label>
			</div>';

		} else {

			$html .= '<div class="col-md-12">
				<div class="checkbox">
					<i>Nenhuma opção foi encontrada para o atributo selecionado</i>
				</div>
			</div>';

		}

		return $html;
	}


	public function storeAttributesOptions(Request $request)
	{
		$flag = false;

		# deletes before linking the new options
		$options = VariationOption::where('products_id', $request->product_id)
										  ->where('attributes_id', $request->attribute_id);
		if ($options->exists()) {
		
			foreach ($options->get() as $rowdel) {
				$destroy = VariationOption::destroy($rowdel->id);
			}

			# perform the selected options and record
			foreach ($request->option as $option) {
							
				list($productsId, $attributesId, $attributesOptionsId) = explode('|', $option);

				$variationOption = new VariationOption;
				$variationOption->products_id			= $productsId;
				$variationOption->attributes_id			= $attributesId;
				$variationOption->attributes_options_id	= $attributesOptionsId;
				if ($variationOption->save()) {
					$flag = true;
				}

			}

			if ($flag == true) {

				$code = 201;
				$response = [
					'message_action'	=> 'success',
					'message'			=> 'Atributo vinculado com sucesso!'
				];

			} else {

				$code = 409;
				$response = [
					'message_action'	=> 'danger',
					'message'			=> 'Não foi possível vincular o artibuto!'
				];

			}
		
		} else {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Nenhuma opção vinculada ao artibuto selecionado!'
			];

		}

		return response()->json($response, $code);
	}


	# **** **************************************************************


	public function showVariations(Request $request)
	{
		# attributes
		$attributes = Attribute::where('companies_id', $this->companiesId)
								 ->orderBy('title', 'asc')
								 ->get();

		$product = Product::find($request->products_id);

		return view('control.modal.modal-variations-options', [
			'action'		=> 'add',
			'product'		=> $product,
			'attributes'	=> $attributes
		]);
	}


	# **** **************************************************************


	public function showDynamicRate()
	{
		return view('control.modal.modal-dynamic-rate');
	}


	public function listDynamicRate()
	{
		$html = '<table class="table table-bordered" width="100%">
			<thead>
				<tr>
					<th>Distância (km)</th>
					<th>Taxa de entrega</th>
					<th>Tempo de entrega</th>
					<th></th>
				</tr>
			</thead>
			<tbody>';

				$distances = CompanyDistances::where('companies_id', $this->companiesId);
				if ($distances->exists()) {

					foreach ($distances->get() as $rowdis) {

						$html .= '<tr>
							<td align="center" width="30%">' . $rowdis->start_distance . ' a ' . $rowdis->end_distance . '</td>
							<td align="center" width="30%">R$ ' . number_format($rowdis->value, 2, ',', '.') . '</td>
							<td align="center" width="30%">' . (($rowdis->time) ? $rowdis->time : '-') . '</td>
							<td align="center" width="10%">
								<a onclick="deleteDynamicRate(' . $rowdis->id . ')" class="cursor:pointer;">
									<i class="entypo-trash"></i>&nbsp;
								</a>
							</td>
						</tr>';

					}

				} else {
					
					$html .= '<tr>
						<td align="center" colspan="4">Nenhuma área de entrega cadastrada até o momento.</td>
					</tr>';

				}

			$html .= '</tbody>
		</table>';

		echo $html;
	}


	public function storeDynamicRate(Request $request)
	{
		$flag = false;

		# deletes before linking the new options
		// $distances = CompanyDistances::where('companies_id', $this->companiesId)
		// 							   ->where('distance', $request->distance);
		// if ($distances->exists()) {
		
		// 	$code = 409;
		// 	$response = [
		// 		'message_action'	=> 'danger',
		// 		'message'			=> 'Valor já adicionado para a distância informada, caso deseja continuar é preciso remover o registro existente!'
		// 	];

		// } else {

			$companyDistances = new CompanyDistances;
			$companyDistances->start_distance	= $request->start_distance;
			$companyDistances->end_distance		= $request->end_distance;
			$companyDistances->value			= str_replace(',', '.', str_replace('.', '', $request->value_fee));
			$companyDistances->time				= (($request->time) ? $request->time : NULL);
			$companyDistances->companies_id		= $this->companiesId;
			if ($companyDistances->save()) {

				$code = 201;
				$response = [
					'message_action'	=> 'success',
					'message'			=> 'Taxa de entrega vinculado com sucesso!'
				];

			} else {

				$code = 409;
				$response = [
					'message_action'	=> 'danger',
					'message'			=> 'Não foi possível vincular a taxa de entrega!'
				];

			}

		// }

		return response()->json($response, $code);
	}


	public function destroyDynamicRate(Request $request)
	{
		$distancy = CompanyDistances::find($request->id);
		if ($distancy->exists()) {

			# delete the record
			$distancy->delete();

			$code = 201;
			$response = [
				'message_action'	=> 'success',
				'message'			=> 'Taxa de entrega desvinculada com sucesso!'
			];

		} else {

			$code = 409;
			$response = [
				'message_action'	=> 'danger',
				'message'			=> 'Não foi possível desvincular a taxa de entrega!'
			];

		}

		return response()->json($response, $code);
		
	}

}