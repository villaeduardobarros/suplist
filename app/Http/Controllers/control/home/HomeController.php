<?php

namespace App\Http\Controllers\control\home;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use SettingsHelperControl;

class HomeController extends Controller
{
	protected $arrCss;
	protected $arrJs;

	public function __construct()
	{
		$this->arrCss = SettingsHelperControl::getCss();
		$this->arrJs = SettingsHelperControl::getJs();
	}

	public function index()
	{
		# add JS 
		array_push($this->arrJs, 'control/prop/login/login.js');
		
		return view('control.home.index', [
			'arrCss' => $this->arrCss,
			'arrJs'	 => $this->arrJs
		]);
	}
}
