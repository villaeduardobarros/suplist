<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipmentForm extends Model
{
    protected $table = 'shipments_form';
}
