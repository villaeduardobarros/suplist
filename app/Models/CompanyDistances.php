<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyDistances extends Model
{
    protected $table = 'companies_distances';
}
