<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentForm extends Model
{
    protected $table = 'payments_form';
}
