<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Mail\CronSend;
use App\Models\Messaging;

use Exception;
use Mail;

class cronEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:messages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks every three minutes if you have any messages to send';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $message = Messaging::where('is_active', 1);
        if ($message->exists()) {

            $row = $message->first();

            try {
                
                Mail::to($row->email_to)->send(new CronSend($row));
                if (count(Mail::failures()) == 0) {

                    $update = Messaging::find($row->id);
                    $update->is_active = NULL;
                    $update->save();
                
                }

            } catch (Exception $e) {
                dd($e);
            }

        }
    }
}
