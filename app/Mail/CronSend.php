<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CronSend extends Mailable
{
	use Queueable, SerializesModels;

	public $inputs;

	public function __construct($input)
	{
		$this->inputs = $input;
	}


    public function build()
    {
		return $this->subject($this->inputs->subject)
					->view($this->inputs->view, [
						'field' => json_decode($this->inputs->field, true)
					]);
    }
}