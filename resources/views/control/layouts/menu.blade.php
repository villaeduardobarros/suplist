<header class="logo-env">
	<div class="logo">
		<a href="{{ route('control.home') }}">
			<img src="control/images/logo.png" alt="" width="25%" />
		</a>
	</div>

	<div class="sidebar-collapse">
		<a href="#" class="sidebar-collapse-icon with-animation">
			<i class="entypo-menu"></i>
		</a>
	</div>

	<div class="sidebar-mobile-menu visible-xs">
		<a href="#" class="with-animation">
			<i class="entypo-menu"></i>
		</a>
	</div>
</header>


<ul id="main-menu" class="">
	<li>
		<a href="{{ route('control.home') }}">
			<i class="entypo-gauge"></i><span>Dashboard</span>
		</a>
	</li>

	<li>
		<a href="#"><i class="entypo-layout"></i><span>Produtos</span></a>
		<ul>
			<li><a href="{{ route('control.products.categories') }}"><span>Categorias</span></a></li>
			<li><a href="{{ route('control.products.attributes') }}"><span>Atributos</span></a></li>
			<li><a href="{{ route('control.products') }}"><span>Listagem</span></a></li>
			<li><a href="{{ route('control.products.create') }}"><span>Cadastrar</span></a></li>
		</ul>
	</li>

	<li>
		<a href="{{ route('control.customers') }}">
			<i class="entypo-layout"></i><span>Clientes</span>
		</a>
	</li>

	<li>
		<a href="{{ route('control.orders') }}">
			<i class="entypo-layout"></i><span>Pedidos</span>
		</a>
	</li>

<li>
	<a href="#"><i class="entypo-layout"></i><span>Configurações</span></a>
	<ul>
		<li><a href="{{ route('control.account.edit') }}"><span>Editar Conta</span></a></li>
	</ul>
</li>
</ul>