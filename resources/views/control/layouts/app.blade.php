<?php
	header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
	date_default_timezone_set('America/Sao_Paulo');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<base href="{{ URL::to('/') }}">
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title></title>
	<link rel="canonical" href="{{ URL::current() }}" />
	<link rel="shortcut icon" href="images/favicon.png" type="image/png">
	<link rel="icon" href="images/favicon.png" type="image/png">
	<?php
		# exibe CSS controller
		if ($arrCss) {
			foreach ($arrCss as $css) {
	?>
				{!! HTML::style($css) !!}
	<?php
			}
		}
	?>

	<script src="control/js/jquery-1.10.2.min.js"></script>
	<script>var baseUrl = '{{ URL::to('/') }}';</script>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>
<body class="page-body">

	<div id="preloader" style="display:none;">
		<img src="control/images/loading.gif" width="140px">
	</div>

	<div class="page-container">	

		<div class="sidebar-menu">

			<?php # menu ?>
			@include('control.layouts.menu')

		</div>

		<div class="main-content">

			<?php # header ?>
			@include('control.layouts.header')

			<?php # conteudo ?>
			@yield('content')

			<?php # footer ?>
			<footer class="main-footer">
				<strong>SupList</strong> &copy; 2020 - Todos os direitos reservados.
				<div class="pull-right hidden-xs"><b>Versão</b> 1.0.0</div>
			</footer>

		</div>

	</div>

	<?php # standard modal ?>
	<div id="getModal"></div>

	<?php
		# exibe JS controller
		if ($arrJs) {
			foreach ($arrJs as $js) {
	?>
				{!! HTML::script($js) !!}
	<?php
			}
		}
	?>

</body>
</html>