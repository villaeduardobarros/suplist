<?php
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
	date_default_timezone_set('America/Sao_Paulo');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<base href="">
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title></title>
	<link rel="canonical" href="" />
	<link rel="shortcut icon" href="control/images/favicon.png" type="image/png">
	<link rel="icon" href="control/images/favicon.png" type="image/png">
	<?php
		# exibe CSS controller
		if ($arrCss) {
			foreach ($arrCss as $css) {
	?>
				{!! HTML::style($css) !!}
	<?php
			}
		}
	?>

	<script>
		var baseUrl = '';
	</script>
	<script src="js/jquery/jquery.min.js"></script>
	<!--[if lt IE 9]>
		<script src="control/js/html5shiv.min.js"></script>
		<script src="control/js/respond.min.js"></script>
	<![endif]-->
</head>

<body class="hold-transition login-page">


	<?php # conteudo ?>
	@yield('content')
	
	<?php
		# exibe JS controller
		if ($arrJs) {
			foreach ($arrJs as $js) {
	?>
				{!! HTML::script($js) !!}
	<?php
			}
		}
	?>

</body>
</html>