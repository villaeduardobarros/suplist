@extends('control.layouts.app')

@section('content')

<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="{{ route('control.home') }}"><i class="entypo-home"></i> Home</a></li>
		<li class="active"><i class="entypo-layout"></i> Pedidos</li>
	</ol>
	<section class="content">
		<div class="row">

			<div class="col-md-12">

				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered datatable dataTable">
							<thead>
								<tr>
									<th width="8%">Número</th>
									<th width="16%">Cliente</th>
									<th width="10%">Telefone / Celular</th>
									<th width="16%">Endereço</th>
									<th width="10%">Qtd. Produtos</th>
									<th width="10%">Valor Total</th>
									<th width="10%">Valor Entrega</th>
									<th width="10%">Valor Troco</th>
									<th width="10%">Data Pedido</th>
								</tr>
							</thead>
							<tbody>

								@if ($arrOrder)

									@foreach (json_decode($arrOrder) as $key => $roword)

										<tr>
											<td align="center">
												{{ '#' . $key }}
												<a class="pull-right" title="Visualizar Pedido" href="{{ route('control.orders.detail', $key) }}">
													<i class="entypo-eye"></i>
												</a>
											</td>

											<td>
												{{ $roword->customer }}
											</td>

											<td align="center">
												@if ($roword->customer_phone && $roword->customer_cellphone)
												
													{{ $roword->customer_phone . ' / ' . $roword->customer_cellphone  }}
												
												@elseif ($roword->customer_phone)

													{{ $roword->customer_phone  }}

												@elseif ($roword->customer_cellphone)

													{{ $roword->customer_cellphone  }}

												@else
													{{ '-' }}
												@endif
											</td>

											<td align="center">
												{!! (($roword->address) ? nl2br(e($roword->address)) : '-') !!}
											</td>

											<td align="center">
												{{ count((array)$roword->products) }}	
											</td>

											<td align="center">
												{{ (($roword->value_amount) ? 'R$ ' . $roword->value_amount : '-') }}
											</td>

											<td align="center">
												{{ (($roword->value_delivery) ? 'R$ ' . $roword->value_delivery : '-') }}
											</td>

											<td align="center">
												{{ (($roword->value_change) ? 'R$ ' . $roword->value_change : '-') }}
											</td>

											<td align="center">
												{{ $roword->date }}
											</td>
										</tr>

									@endforeach

								@endif

							</tbody>
						</table>

					</div>
				</div>
			</div>

		</div>
	</section>
</div>

@endsection