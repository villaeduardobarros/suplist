
@foreach (json_decode($arrOrder) as $key => $roword)

	<table border="1" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center" width="60%"></td>
			<td align="center" width="40%">
				<h3>Pedido #{{ $key }}</h3>
				<span>{{ $roword->date }}</span>
			</td>
		</tr>
	</table>

	<hr class="margin">

	<table border="1" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td width="25%">
				<h4>Dados do Cliente</h4>
				{{ $roword->customer }}

				<br>
				
				{{ $roword->customer_email }}

				<br>

				@if ($roword->customer_phone && $roword->customer_cellphone)
												
					{{ $roword->customer_phone . ' / ' . $roword->customer_cellphone  }}
				
				@elseif ($roword->customer_phone)

					{{ $roword->customer_phone  }}

				@elseif ($roword->customer_cellphone)

					{{ $roword->customer_cellphone  }}

				@else
					{{ '-' }}
				@endif
			</td>
			<td width="25%">
				<h4>Dados da Entrega</h4>
				{!! nl2br(e($roword->addressComplete)) !!}
			</td>
			<td width="25%">
				<h4>Observações</h4>
				{!! nl2br(e($roword->comments)) !!}
			</td>
			<td width="25%">
				<h4>Detalhes do Pagamento</h4>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="50%" align="right"><strong>Forma de Pagamento:</strong></td>
						<td width="50%" align="center">{{ $roword->payment_form }}</td>
					</tr>

					@if ($roword->address == NULL)
						@if ($roword->value_delivery)
							<tr>
								<td align="right"><strong>Taxa de entrega:</strong></td>
								<td align="center">R$ {{ $roword->value_delivery }}</td>
							</tr>
						@endif
					@endif
					
					@if ($roword->payment_form == 1)
						@if (!empty($roword->value_change))
							<tr>
								<td align="right"><strong>Troco para:</strong></td>
								<td align="center">R$ {{ $roword->value_change }}</td>
							</tr>
						@endif
					@endif

					<tr>
						<td align="right"><strong>Total do pedido:</strong></td>
						<td align="center">R$ {{ $roword->value_amount }}</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<hr class="margin">

	<table border="1" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<th class="text-center">#</th>
			<th width="60%">Produto</th>
			<th>Quantidade</th>
			<th>Valor Unitário</th>
		</tr>

		@php
			$count=1;
		@endphp
		
		@foreach ($roword->products as $rowpro)

			<tr>
				<td class="text-center">{{ $count }}</td>
				<td>{{ $rowpro->title }}</td>
				<td>{{ $rowpro->quantity }}</td>
				<td class="text-right">R$ {{ $rowpro->value_unitary }}</td>
			</tr>

			@php
				$count++;
			@endphp

		@endforeach
	</table>

@endforeach

<script>print();</script>