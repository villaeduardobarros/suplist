@extends('control.layouts.app')

@section('content')

@foreach (json_decode($arrOrder) as $key => $roword)

	<div class="content-wrapper">
		<ol class="breadcrumb">
			<li><a href="{{ route('control.home') }}"><i class="entypo-home"></i> Home</a></li>
			<li><a href="{{ route('control.orders') }}"><i class="entypo-layout"></i> Pedidos</a></li>
			<li class="active"><i class="entypo-layout"></i> Detalhes do Pedido</li>
		</ol>
		<section class="content">
			<div class="row">

				<div class="col-md-12">

					<div class="invoice">
						<div class="row">
							<div class="col-sm-6 invoice-left"></div>
							<div class="col-sm-6 invoice-right">
								<h3>Pedido #{{ $key }}</h3>
								<span>{{ $roword->date }}</span>
							</div>
						</div>

						<hr class="margin">

						<div class="row">
							<div class="col-sm-3 invoice-left">
								<h4>Dados do Cliente</h4>
								{{ $roword->customer }}
								
								<br>
								
								{{ $roword->customer_email }}
								
								<br>
								
								@if ($roword->customer_phone && $roword->customer_cellphone)
												
									{{ $roword->customer_phone . ' / ' . $roword->customer_cellphone  }}
								
								@elseif ($roword->customer_phone)

									{{ $roword->customer_phone  }}

								@elseif ($roword->customer_cellphone)

									{{ $roword->customer_cellphone  }}

								@else
									{{ '-' }}
								@endif
							</div>
							
							<div class="col-sm-3 invoice-left">
								<h4>Dados da Entrega</h4>
								{!! nl2br(e($roword->addressComplete)) !!}
							</div>

							<div class="col-sm-3 invoice-left">
								<h4>Observações</h4>
								{!! nl2br(e($roword->comments)) !!}
							</div>

							<div class="col-md-3 invoice-left">
								<h4>Detalhes do Pagamento</h4>
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td width="50%" align="right"><strong>Forma de Pagamento:</strong></td>
										<td width="50%" align="center">{{ $roword->payment_form }}</td>
									</tr>

									@if ($roword->address == NULL)
										@if ($roword->value_delivery)
											<tr>
												<td align="right"><strong>Taxa de entrega:</strong></td>
												<td align="center">R$ {{ $roword->value_delivery }}</td>
											</tr>
										@endif
									@endif
									
									@if ($roword->payment_form == 1)
										@if (!empty($roword->value_change))
											<tr>
												<td align="right"><strong>Troco para:</strong></td>
												<td align="center">R$ {{ $roword->value_change }}</td>
											</tr>
										@endif
									@endif

									<tr>
										<td align="right"><strong>Total do pedido:</strong></td>
										<td align="center">R$ {{ $roword->value_amount }}</td>
									</tr>
								</table>
							</div>
						</div>

						<div class="margin"></div>

						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th width="60%">Produto</th>
									<th>Quantidade</th>
									<th>Valor Unitário</th>
								</tr>
							</thead>
							<tbody>

								@php $count=1; @endphp
								@foreach ($roword->products as $rowpro)

									<tr>
										<td class="text-center">{{ $count }}</td>
										<td>{{ $rowpro->title }}</td>
										<td>{{ $rowpro->quantity }}</td>
										<td class="text-right">R$ {{ $rowpro->value_unitary }}</td>
									</tr>

									@php $count++; @endphp
								@endforeach
							</tbody>
						</table>
					</div>

				</div>

			</div>

			<div class="row" style="margin:40px 0;">
				<div class="pull-right">
					<a class="btn btn-blue btn-icon icon-left btn-sm" href="{{ route('control.orders.print', $key) }}" target="_blank">
						<i class="entypo-print"></i> Imprimir
					</a>
				</div>
			</div>
		</section>

	</div>

@endforeach

@endsection