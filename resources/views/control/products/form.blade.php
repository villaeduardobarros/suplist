@extends('control.layouts.app')

@section('content')

<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="{{ route('control.home') }}"><i class="entypo-home"></i> Home</a></li>
		<li><a href="{{ route('control.products') }}"><i class="entypo-layout"></i> Produtos</a></li>
		<li class="active"><i class="entypo-layout"></i> Formulário</li>
	</ol>
	<section class="content">

		@if($action == 'edit')
			<form action="{{ route('control.products.update') }}" id="formProducts" method="post" role="form" enctype="multipart/form-data">
		@else
			<form action="{{ route('control.products.store') }}" id="formProducts" method="post" role="form" enctype="multipart/form-data">
		@endif

			@csrf
			<input type="hidden" name="action" value="{{ $action }}">
			@if ($action == 'edit')
				<input type="hidden" name="_method" value="put">
				<input type="hidden" name="id" value="{{ $productSelect->id }}">
				<input type="hidden" name="image_id" value="{{ $productSelect->idImage }}">
				<input type="hidden" name="image_old" value="{{ $productSelect->file }}">
			@endif

			<div class="row">
				<div class="col-md-12">
					<h3>{{ (($action == 'add') ? 'Cadastrar novo produto' : 'Editar produto #' . $productSelect->id) }}</h3>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div id="messageAlert"></div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-7">
					<div class="panel panel-gray" data-collapsed="0">
						
						<div class="panel-heading">
							<div class="panel-title">
								<h4><strong>Dados do Produto</strong></h4>
							</div>
							<hr>
						</div>
						
						<div class="panel-body">

							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="type">Tipo de Produto <small style="color:#900;">*</small></label>
										<select class="form-control" id="type" name="type" style="background-color:#ffffff;">
											<option value="">Selecione</option>
											<option value="1"{{ (($action == 'edit' && $productSelect->type == 1) ? ' selected' : NULL) }}>Produto Simples</option>
											<option value="2"{{ (($action == 'edit' && $productSelect->type == 2) ? ' selected' : NULL) }}>Produto Variável</option>
											<!-- <option value="3"{{ (($action == 'edit' && $productSelect->type == 3) ? ' selected' : NULL) }}>Produto Composto</option> -->
										</select>
									</div>

									@if ($action == 'edit')

										<div id="attributesOptions" style="{{ (($productSelect->type == 1) ? 'display:none;' : NULL) }}">
											<div class="col-md-3" style="text-align:center;">
												<button class="btn btn-blue btn-icon icon-left" type="button" onclick="addAttributesOptions({{ $productSelect->id }})" style="margin:23px 0 0;">
													<i class="entypo-link"></i> Vincular atributos
												</button>
											</div>

											<!-- <div class="col-md-3" style="text-align:center;">
												<button class="btn btn-blue btn-icon icon-left" type="button" onclick="addVariations({{ $productSelect->id }})" style="margin:24px 0 0;">
													<i class="entypo-link"></i> Montar Variações
												</button>
											</div> -->
										</div>

									@endif
								</div>
							</div>

							<div class="form-group">
								<label for="title">Categorias <small style="color:#900;">*</small></label>
								<select class="form-control" id="category" name="category" style="background-color:#ffffff;">
									<option value="">Selecione</option>
									@foreach ($categories as $category)
										<option value="{{ $category->id }}" {{ (($action == 'edit' && ($category->id == $productSelect->categories_id)) ? 'selected' : NULL) }}>{{ $category->title }}</option>
									@endforeach
								</select>
							</div>

							<div class="form-group">
								<label for="title">Título <small style="color:#900;">*</small></label>
								<input type="text" class="form-control" id="title" name="title" value="{{ (($action == 'edit') ? $productSelect->title : NULL) }}" autocomplete="off">
							</div>

							<div class="form-group">
								<label for="description">Descrição do Produto</label>
								<textarea class="form-control" id="description" name="description" rows="5">{{ (($action == 'edit') ? $productSelect->description : NULL) }}</textarea>
							</div>

							<div class="form-group">
								<label for="description_packing">Descrição da Embalagem</label>
								<textarea class="form-control" id="description_packing" name="description_packing" rows="5">{{ (($action == 'edit') ? $productSelect->description_packing : NULL) }}</textarea>
							</div>]

							<div class="form-group" id="productSimple">
								<div class="row">
									<div class="col-md-6">
										<label for="value">Preço (R$)</label>
										<input type="text" class="form-control value" id="value" name="value" value="{{ (($action == 'edit' && $productSelect->value) ? number_format($productSelect->value, 2, ',', '.') : NULL) }}" autocomplete="off">
									</div>

									<div class="col-md-6">
										<label for="value_promotional">Preço Promocional (R$)</label>
										<input type="text" class="form-control value" id="value_promotional" name="value_promotional" value="{{ (($action == 'edit' && $productSelect->value_promotional) ? $productSelect->value_promotional : NULL) }}" autocomplete="off">
									</div>
								</div>
							</div>

							<div class="row" style="margin:16px 0 !important;">
								<div class="form-group">

									<div class="col-md-6">
										<label class="col-md-5 control-label" style="padding:6px !important;">Destaque</label>
										<div class="col-md-5">
											<div class="make-switch switch-small has-switch" data-on="success" data-on-label="<i class='entypo-check'></i>" data-off="danger" data-off-label="<i class='entypo-cancel'></i>">
												<input id="is_featured" name="is_featured" type="checkbox" value="1" {{ (($action == 'edit') ? (($productSelect->is_featured == 1) ? 'checked' : NULL) : NULL) }}>
											</div>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-5 control-label" style="padding:6px !important;">Situação</label>
											<div class="col-md-5">
												<div class="make-switch switch-small has-switch" data-on="success" data-on-label="<i class='entypo-check'></i>" data-off="danger" data-off-label="<i class='entypo-cancel'></i>">
													<input id="is_active" name="is_active" type="checkbox" value="1" {{ (($action == 'edit') ? (($productSelect->is_active == 1) ? 'checked' : NULL) : 'checked') }}>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</div>

				<div class="col-md-5">
					<div class="panel panel-gray" data-collapsed="0">
						<div class="panel-heading">
							<div class="panel-title">
								<h4><strong>Imagem</strong></h4>
							</div>
							<hr>
						</div>

						<div class="panel-body">

							<div class="row">
								<div class="form-group">
									<div class="col-md-12">

										<div class="fileinput fileinput-new" data-provides="fileinput">
											<div class="fileinput-new thumbnail" style="width:100%" data-trigger="fileinput">
												@if ($action == 'edit')
													
														@if (empty($productSelect->file))
															<img src="http://placehold.it/{{ $widthLarge }}x{{ $heightLarge }}">
														@else
															<img src="{{ Storage::disk('s3')->url($folderUpload . 'large/' . $productSelect->companies_id . '/' . $productSelect->id . '/' . $productSelect->file) }}">
														@endif

												@else
													<!-- <img src="https://ipsumimage.appspot.com/{{ $widthLarge }}x{{ $heightLarge }},CCCCCC?s=50&f=888888&l=Tamanho+ideal:+{{ $widthLarge }}x{{ $heightLarge }}|Tamanho+mínimo:+768x768"> -->
													<img src="http://placehold.it/{{ $widthLarge }}x{{ $heightLarge }}">
												@endif
											</div>
											<div class="fileinput-preview fileinput-exists thumbnail" style="max-width:{{ $widthLarge }}px;max-height:{{ $heightLarge }}px;"></div>
											<div>
												<span class="btn btn-white btn-file">

													<span class="fileinput-new">Selecione Imagem</span>
													<span class="fileinput-exists">Alterar Imagem</span>
													<input type="file" name="image" accept="image/*">
												</span>
												<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remover Imagem</a>
											</div>
										</div>

									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="pull-right">
						<a class="btn btn-red btn-icon icon-left btn-sm" href="{{ route('control.products') }}" style="margin-right:15px;"><i class="entypo-cancel"></i> Cancelar</a>
						<button class="btn btn-green btn-icon icon-left btn-sm" type="submit" id="buttonForm"><i class="entypo-download"></i> Salvar</button>
					</div>
				</div>
			</div>

		</form>

	</section>
</div>

@endsection