@extends('control.layouts.app')

@section('content')

<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="{{ route('control.home') }}"><i class="entypo-home"></i> Home</a></li>
		<li><a href="{{ route('control.products') }}"><i class="entypo-layout"></i> Produtos</a></li>
		<li class="active"><i class="entypo-layout"></i> Categorias</li>
	</ol>
	<section class="content">
		<div class="row">

			<div class="col-md-8">
				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered datatable dataTable">
							<thead>
								<tr>
									<th width="80%">Título</th>
									<th width="10%">Destaque</th>
									<th width="10%">Situação</th>
								</tr>
							</thead>
							<tbody>

								@foreach ($categories as $rowcat)

								<tr>
									<td>
										<a class="pull-right" title="Excluir categoria" onclick="return confirm('Deseja realmente excluir esta categoria?')" href="{{ route('control.products.categories.destroy', $rowcat->id) }}">
											<i class="entypo-trash"></i>&nbsp;
										</a>
										<a class="pull-right" title="Editar categoria" href="{{ route('control.products.categories.edit', $rowcat->id) }}">
											<i class="entypo-pencil"></i>&nbsp;
										</a>
										{{ $rowcat->title }}
									</td>
									<td align="center">
										<span class="label label-<?php echo ($rowcat->is_featured == 1) ? 'success' : 'danger'; ?>">
											<?php echo ($rowcat->is_featured == 1) ? 'Ativo' : 'Inativo'; ?>
										</span>
									</td>
									<td align="center">
										<span class="label label-<?php echo ($rowcat->is_active == 1) ? 'success' : 'danger'; ?>">
											<?php echo ($rowcat->is_active == 1) ? 'Ativo' : 'Inativo'; ?>
										</span>
									</td>
								</tr>

								@endforeach

							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="panel panel-gray" data-collapsed="0">
					<div class="panel-heading">
						<div class="panel-title">
							<h3>{{ (($action == 'add') ? 'Cadastrar nova categoria' : 'Editar categoria #' . $categorySelect->id) }}</h3>
						</div>

						<hr>

						<div class="panel-body">

							@if($action == 'edit')
								<form action="{{ route('control.products.categories.update') }}" id="formCategories" method="post" role="form" enctype="multipart/form-data">
							@else
								<form action="{{ route('control.products.categories.store') }}" id="formCategories" method="post" role="form" enctype="multipart/form-data">
							@endif

								@csrf
								<input type="hidden" name="action" value="{{ $action }}">
								@if ($action == 'edit')
									<input type="hidden" name="_method" value="put">
									<input type="hidden" name="id" value="{{ $categorySelect->id }}">
								@endif

								<div class="row">
									<div class="col-md-12">
										<div id="messageAlert"></div>
									</div>
								</div>

								<div class="form-group">
									<label for="title">Título <small style="color:#900;">*</small></label>
									<input type="text" class="form-control" id="title" name="title" value="{{ (($action == 'edit') ? $categorySelect->title : NULL) }}" autocomplete="off">
								</div>

								<div class="form-group">
									<label for="description">Descrição</label>
									<textarea class="form-control" id="description" name="description" rows="3">{{ (($action == 'edit') ? $categorySelect->description : NULL) }}</textarea>
								</div>

								<div class="row" style="margin:16px 0 !important;">
									<div class="form-group">

										<div class="col-md-6">
											<label class="col-sm-5 control-label" style="padding:6px !important;">Destaque</label>
											<div class="col-sm-5">
												<div class="make-switch switch-small has-switch" data-on="success" data-on-label="<i class='entypo-check'></i>" data-off="danger" data-off-label="<i class='entypo-cancel'></i>">
													<input id="is_featured" name="is_featured" type="checkbox" value="1" {{ (($action == 'edit') ? (($categorySelect->is_featured == 1) ? 'checked' : NULL) : NULL) }}>
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="col-sm-5 control-label" style="padding:6px !important;">Situação</label>
												<div class="col-sm-5">
													<div class="make-switch switch-small has-switch" data-on="success" data-on-label="<i class='entypo-check'></i>" data-off="danger" data-off-label="<i class='entypo-cancel'></i>">
														<input id="is_active" name="is_active" type="checkbox" value="1" {{ (($action == 'edit') ? (($categorySelect->is_active == 1) ? 'checked' : NULL) : 'checked') }}>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>

								<div class="row">
									<div class="col-md-12">
										<div class="pull-right">
											<a class="btn btn-default" href="{{ route('control.products.categories') }}" style="margin-right:15px;"><i class="entypo-cancel"></i> Cancelar</a>
											<button type="submit" id="buttonForm" class="btn btn-success"><i class="entypo-download"></i> Salvar</button>
										</div>
									</div>
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>

		</div>
	</section>
</div>

@endsection