@extends('control.layouts.app')

@section('content')

<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="{{ route('control.home') }}"><i class="entypo-home"></i> Home</a></li>
		<li class="active"><i class="entypo-layout"></i> Produtos</li>
	</ol>
	
	<section class="content">
		<div class="row">

			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<a class="btn btn-blue btn-sm pull-right" href="{{ route('control.products.create') }}" style="margin:0 0 0 10px;">Cadastrar Produtos</a>
						<a class="btn btn-blue btn-sm pull-right" href="{{ route('control.products.categories') }}">Cadastrar Categoria</a>
					</div>
				</div>

				<br>

				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered datatable dataTable" width="100%">
							<thead>
								<tr>
									<th width="35%">Título</th>
									<th width="15%">Categoria</th>
									<th width="10%">Tipo</th>
									<th width="10%">Imagem</th>
									<th width="10%">Preço Venda</th>
									<th width="10%">Destaque</th>
									<th width="10%">Situação</th>
								</tr>
							</thead>
							<tbody>

								@foreach ($products as $rowpro)

									<tr>
										<td>
											<a class="pull-right" title="Excluir produto" onclick="return confirm('Deseja realmente excluir este produto?')" href="{{ route('control.products.destroy', ['id' => $rowpro->id]) }}">
												<i class="entypo-trash"></i>&nbsp;
											</a>
											<a class="pull-right" title="Editar produto" href="{{ route('control.products.edit', $rowpro->id) }}">
												<i class="entypo-pencil"></i>&nbsp;
											</a>
											{{ $rowpro->title }}
										</td>
										<td align="center">
											{{ $rowpro->titleCategory }}
										</td>
										<td align="center">
											{{ ($rowpro->type == 1) ? 'Produto Simples' : 'Produto Variável' }}
										</td>
										<td align="center">
											@if ($rowpro->file)
												<img src="{{ Storage::disk('s3')->url($folderUpload . 'large/' . $rowpro->companies_id . '/' . $rowpro->id . '/' . $rowpro->file) }}" width="30%">
											@else - @endif
										</td>
										<td align="center">
											@if ($rowpro->value_promotional)

												de <span style="text-decoration:line-through;">R$ {{ number_format($rowpro->value, 2, ',', '.') }}</span>
												<br>
												por <strong>R$ {{ number_format($rowpro->value_promotional, 2, ',', '.') }}</strong>

											@else
												R$ {{ number_format($rowpro->value, 2, ',', '.') }}
											@endif
										</td>
										<td align="center">
											<span class="label label-<?php echo ($rowpro->is_featured == 1) ? 'success' : 'danger'; ?>">
												<?php echo ($rowpro->is_featured == 1) ? 'Ativo' : 'Inativo'; ?>
											</span>
										</td>
										<td align="center">
											<span class="label label-<?php echo ($rowpro->is_active == 1) ? 'success' : 'danger'; ?>">
												<?php echo ($rowpro->is_active == 1) ? 'Ativo' : 'Inativo'; ?>
											</span>
										</td>
									</tr>

								@endforeach

							</tbody>
						</table>

					</div>
				</div>
			</div>

		</div>
	</section>
</div>

@endsection