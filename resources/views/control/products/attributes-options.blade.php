@extends('control.layouts.app')

@section('content')

<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="{{ route('control.home') }}"><i class="entypo-home"></i> Home</a></li>
		<li><a href="{{ route('control.products') }}"><i class="entypo-layout"></i> Produtos</a></li>
		<li><a href="{{ route('control.products.attributes') }}"><i class="entypo-layout"></i> Atributos</a></li>
		<li class="active"><i class="entypo-layout"></i> Opções do Atributo</li>
	</ol>
	<section class="content">
		<div class="row">

			<div class="col-md-8">
				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered datatable dataTable">
							<thead>
								<tr>
									<th width="90%">Título</th>
									<th width="10%">Situação</th>
								</tr>
							</thead>
							<tbody>

								@foreach ($attributesOptions as $rowopt)

								<tr>
									<td>
										<a class="pull-right" title="Excluir atributo" onclick="return confirm('Deseja realmente excluir esta opção de atributo?')" href="{{ route('control.products.attributes.options.destroy', ['attribute_id'=>$attributeId, 'id'=>$rowopt->id]) }}">
											<i class="entypo-trash"></i>&nbsp;
										</a>

										<a class="pull-right" title="Editar atributo" href="{{ route('control.products.attributes.options.edit', ['attribute_id'=>$attributeId, 'id'=>$rowopt->id]) }}">
											<i class="entypo-pencil"></i>&nbsp;
										</a>

										{{ $rowopt->title }}
									</td>
									<td align="center">
										<span class="label label-<?php echo ($rowopt->is_active == 1) ? 'success' : 'danger'; ?>">
											<?php echo ($rowopt->is_active == 1) ? 'Ativo' : 'Inativo'; ?>
										</span>
									</td>
								</tr>

								@endforeach

							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="panel panel-gray" data-collapsed="0">
					<div class="panel-heading">
						<div class="panel-title">
							<h3>{{ (($action == 'add') ? 'Cadastrar nova opção' : 'Editar opção #' . $optionsSelect->id) }}</h3>
						</div>
						<hr>
					</div>

					<div class="panel-body">

						@if($action == 'edit')
							<form action="{{ route('control.products.attributes.options.update') }}" id="formAttibutesOptions" method="post" role="form">
						@else
							<form action="{{ route('control.products.attributes.options.store') }}" id="formAttibutesOptions" method="post" role="form">
						@endif

							@csrf
							<input type="hidden" name="action" value="{{ $action }}">
							@if ($action == 'edit')
								<input type="hidden" name="_method" value="put">
								<input type="hidden" name="id" value="{{ $optionsSelect->id }}">
							@endif

							<div class="row">
								<div class="col-md-12">
									<div id="messageAlert"></div>
								</div>
							</div>

							<div class="form-group">
								<label for="attribute_id">Atributo <small style="color:#900;">*</small></label>
								<select class="form-control" id="attribute_id" name="attribute_id" readonly tabindex="-1" aria-disabled="true" style="background-color:#ffffff;pointer-events:none;touch-action:none;">
									@foreach($attributes as $rowatt)

										<option value="{{ $rowatt->id }}" {{ ($rowatt->id == $attributeId) ? 'selected' : NULL }}>{{ $rowatt->title }}</option>

									@endforeach
								</select>
							</div>

							<div class="form-group">
								<label for="title">Título <small style="color:#900;">*</small></label>
								<input type="text" class="form-control" id="title" name="title" value="{{ (($action == 'edit') ? $optionsSelect->title : NULL) }}" autocomplete="off">
							</div>

							<div class="form-group">
								<label for="description">Descrição</label>
								<textarea class="form-control" id="description" name="description" rows="3">{{ (($action == 'edit') ? $optionsSelect->description : NULL) }}</textarea>
							</div>

							<div class="row" style="margin:16px 0 !important;">
								<div class="form-group">

									<div class="col-md-6">
										<div class="form-group">
											<label class="col-sm-5 control-label" style="padding:6px !important;">Situação</label>
											<div class="col-sm-5">
												<div class="make-switch switch-small has-switch" data-on="success" data-on-label="<i class='entypo-check'></i>" data-off="danger" data-off-label="<i class='entypo-cancel'></i>">
													<input id="is_active" name="is_active" type="checkbox" value="1" {{ (($action == 'edit') ? (($optionsSelect->is_active == 1) ? 'checked' : NULL) : 'checked') }}>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="pull-right">
										<a class="btn btn-default" href="{{ route('control.products.attributes.options', $attributeId) }}" style="margin-right:15px;"><i class="entypo-cancel"></i> Cancelar</a>
										<button type="submit" id="buttonForm" class="btn btn-success"><i class="entypo-download"></i> Salvar</button>
									</div>
								</div>
							</div>
						</form>

					</div>
				</div>
			</div>

		</div>
	</section>
</div>

@endsection