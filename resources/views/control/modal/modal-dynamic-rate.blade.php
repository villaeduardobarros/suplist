<div class="modal fade" id="dynamicRateModal" tabindex="-1" role="dialog" aria-labelledby="dynamicRateModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

			<div class="modal-header">
				<h3 class="modal-title" id="exampleModalLabel">Vincular área de entrega</h3>
			</div>

			<form action="{{ route('control.modal.dynamic.rate.store') }}" id="formDinamicRate" method="post" role="form">
				@csrf
				
				<div class="modal-body">
					<div class="form-group">
						<div id="messageAlert"></div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-md-3">
								<label for="start_distance">Dist. Inicial (Km)<small style="color:#900;">*</small></label>
								<input type="number" name="start_distance" id="start_distance" class="form-control value" placeholder="0" autocomplete="off" onkeypress="return checkNumber(event)">
							</div>

							<div class="col-md-3">
								<label for="end_distance">Dist. Final (Km)<small style="color:#900;">*</small></label>
								<input type="number" name="end_distance" id="end_distance" class="form-control value" placeholder="2" autocomplete="off" onkeypress="return checkNumber(event)">
							</div>

							<div class="col-md-3">
								<label for="value_fee">Taxa de entrega<small style="color:#900;">*</small></label>
								<input type="text" name="value_fee" id="value_fee" class="form-control value" placeholder="3,00" autocomplete="off">
							</div>
							
							<div class="col-md-3">
								<label for="time">Tempo de entrega</label>
								<input type="text" name="time" id="time" class="form-control hours" placeholder="00:00" autocomplete="off">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12 list-distances"></div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-success">Salvar</button>
				</div>

			</form>

		</div>
	</div>
</div>