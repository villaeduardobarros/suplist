<div class="modal fade" id="attributesModal" tabindex="-1" role="dialog" aria-labelledby="attributesModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

			<div class="modal-header">
				<h3 class="modal-title" id="exampleModalLabel">Vincular Atributos</h3>
			</div>

			<form action="{{ route('control.modal.attributes.options.store') }}" id="formAttributesOptions" method="post" role="form">
				@csrf
				
				<div class="modal-body">
					<h4><strong>Produto:</strong> {{ $product->title }}</h4>

					<div class="form-group">
						<div id="messageAlert"></div>
					</div>

					<div class="form-group">
						<label for="attribute_id">Atributo <small style="color:#900;">*</small></label>
						<select class="form-control" id="attribute_id" name="attribute_id">
							<option value="">Selecione o atributo</option>
							@foreach($attributes as $rowatt)
								<option value="{{ $rowatt->id }}">{{ $rowatt->title }}</option>
							@endforeach
						</select>
					</div>

					<div class="row list-options">
						<hr class="mb-1">
						<div id="imgPreloader" style="text-align:center;width:100%;display:none;">
							<img src="web/img/preloader.gif">
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-success">Salvar</button>
				</div>

			</form>

		</div>
	</div>
</div>