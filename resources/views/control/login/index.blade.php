@extends('control.layouts.login')

@section('content')

	<div class="login-box">
		<div class="login-logo">
			<div class="login-logo"><img src="images/logo.png" width="30%"></div>
		</div>
		<div class="login-box-body">
			<p><strong>Seja bem-vindo,</strong></p>

			<form action="{{ route('control-authenticate') }}" id="loginForm" method="post" role="form">
				@csrf
				<div class="form-group has-feedback">
					<input type="text" id="email" name="email" class="form-control" placeholder="E-mail" autocomplete="off">
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>

				<div class="form-group has-feedback">
					<input type="password" id="password" name="password" class="form-control" placeholder="Senha">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>

				<div class="row" style="height:34px;">
					<div class="col-md-8 col-xs-12">
						<div id="messageAlert" style="padding:7px !important;" class="callout callout-warning">
							<i class="fa fa-exclamation-circle"></i> Informe os dados
						</div>
					</div>
					<div class="col-md-4 col-xs-12">
						<button type="submit" id="buttonForm" class="btn btn-block btn-primary"><i class="fa fa-send"></i> Entrar</button>
					</div>
				</div>
			</form>
		</div>
		
		<!-- <br>

		<p style="text-align:center;">
			<a href="login/esqueceu-a-senha" style="color:#cc3d3d !important;">
				<strong>Esqueceu sua senha?</strong>
			</a>
		</p> -->
	</div>

@endsection