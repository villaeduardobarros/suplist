@extends('control.layouts.app')

@section('content')

<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="home"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="Companies"><i class="fa fa-cubes"></i> Empresas</a></li>
		<li class="active"><i class="fa fa-list-ul"></i> Categorias</li>
	</ol>

	<section class="content">
		<div class="row">

			<div class="col-md-8">
				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="80%">Título</th>
									<th width="10%">Destaque</th>
									<th width="10%">Situação</th>
								</tr>
							</thead>
							<tbody>

								@foreach ($categories as $rowcat)

									<tr>
										<td>
											<a onclick="return confirm('Deseja realmente excluir está categoria?')" href="{{ route('control.companies.categories.destroy', $rowcat->id) }}">
												<i class="fa fa-trash pull-right" style="color:#900;cursor:pointer;"></i>
											</a>
											<a href="{{ route('control.companies.categories.edit', $rowcat->id) }}" target="_self">
												{{ $rowcat->title }}
											</a>
										</td>
										<td align="center">
											<span class="label label-{{ (($rowcat->is_featured == 1) ? 'success' : 'danger') }}">
												{{ (($rowcat->is_featured == 1) ? 'Ativo' : 'Inativo') }}
											</span>
										</td>
										<td align="center">
											<span class="label label-{{ (($rowcat->is_active == 1) ? 'success' : 'danger') }}">
												{{ (($rowcat->is_active == 1) ? 'Ativo' : 'Inativo') }}
											</span>
										</td>
									</tr>

								@endforeach

							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="box box-widget">
					<div class="box-body">

						@if($action == 'edit')
							<form action="{{ route('control.companies.categories.update') }}" method="put" role="form">
						@else
							<form action="{{ route('control.companies.categories.store') }}" method="post" role="form">
						@endif
						
							@csrf
							<input type="hidden" name="action" value="{{ $action }}">
							@if ($action == 'edit')
								<input type="hidden" name="id" value="{{ $category->id }}">
							@endif

							<div class="box box-widget">
								<div class="box-header with-border">
									<h3 class="box-title">
										{{ (($action == 'add') ? 'Cadastrar nova categoria' : 'Editar categoria #' . $category->id) }}
									</h3>
								</div>
								<div class="box-body">

									<div class="row">
										<div class="col-md-12">
											<div id="messageAlert"></div>
										</div>
									</div>

									@if (session()->has('message'))
										<div class="alert alert-{{ session()->get('message-action') }}">
											{{ session()->get('message') }}
										</div>
									@endif

									<div class="form-group">
										<label for="title">Título <small style="color:#900;">*</small></label>
										<input type="text" class="form-control" id="title" name="title" value="{{ (($action == 'edit') ? $category->title : NULL) }}" autocomplete="off">
									</div>

									<div class="form-group">
										<label for="slug">Slug</label>
										<input type="text" class="form-control" id="slug" name="slug" value="{{ (($action == 'edit') ? $category->slug : NULL) }}" autocomplete="off" readonly>
									</div>

									<div class="form-group">
										<label for="description">Descrição</label>
										<textarea class="form-control" id="description" name="description" rows="3">{{ (($action == 'edit') ? $category->description : NULL) }}</textarea>
									</div>

									<div class="row">
										<div class="col-md-6">
										<div class="form-group">
												<label class="control-label">Destaque</label>
												<div class="material-switch">
													<input id="is_featured" name="is_featured" type="checkbox" value="1" {{ (($action == 'edit') ? (($category->is_featured == 1) ? 'checked' : NULL) : 'checked') }}>
													<label for="is_featured" class="label-success"></label>
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Situação</label>
												<div class="material-switch">
													<input id="is_active" name="is_active" type="checkbox" value="1" {{ (($action == 'edit') ? (($category->is_active == 1) ? 'checked' : NULL) : 'checked') }}>
													<label for="is_active" class="label-success"></label>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="pull-right">
										<a class="btn btn-default" href="posts/categories" style="margin-right:15px;"><i class="fa fa-remove"></i> Cancelar</a>
										<button type="submit" id="buttonForm" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
									</div>
								</div>
							</div>
						</form>

					</div>
				</div>
			</div>

		</div>
	</section>
</div>

@endsection