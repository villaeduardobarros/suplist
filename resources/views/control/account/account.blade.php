@extends('control.layouts.app')

@section('content')

<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="{{ route('control.home') }}"><i class="entypo-home"></i> Home</a></li>
		<li class="active"><i class="entypo-layout"></i> Configurações</li>
		<li class="active"><i class="entypo-layout"></i> Editar Dados</li>
	</ol>
	<section class="content">

		<form action="{{ route('control.account.update') }}" id="formAccount" method="post" role="form" enctype="multipart/form-data">
			@csrf
			<input type="hidden" name="_method" value="put">
			<input type="hidden" name="id" value="{{ $companySelect->id }}">
			<input type="hidden" name="address_id" value="{{ $address->id }}">
			<input type="hidden" name="company_user_id" value="{{ $companyUser->id }}">
			@if ($imageLogo)
				<input type="hidden" name="image_logo_id" value="{{ $imageLogo->id }}">
				<input type="hidden" name="image_logo_old" value="{{ $imageLogo->file }}">
			@endif

			@if ($imageBanner)
				<input type="hidden" name="image_banner_id" value="{{ $imageBanner->id }}">
				<input type="hidden" name="image_banner_old" value="{{ $imageBanner->file }}">
			@endif

			<div class="row">
				<div class="col-md-12">
					<h3>Editar dados da empresa</h3>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div id="messageAlert"></div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-gray" data-collapsed="0">
						<div class="panel-heading">
							<div class="panel-title">
								<h4><strong>Dados de Acesso</strong></h4>
							</div>
							<hr>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="value">E-mail *</label>
										<input type="text" class="form-control" id="email" name="email" value="{{ (($companySelect->email) ? $companySelect->email : NULL) }}" autocomplete="off">
									</div>

									<div class="col-md-6">
										<label for="value_promotional">Nova senha</label>
										<input type="text" class="form-control" id="password" name="password">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-gray" data-collapsed="0">
						<div class="panel-heading">
							<div class="panel-title">
								<h4><strong>Dados Pessoais</strong></h4>
							</div>
							<hr>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="name">Nome do responsável *</label>
										<input type="text" class="form-control" id="name" name="name" value="{{ (($companySelect->name) ? $companySelect->name : NULL) }}" autocomplete="off">
									</div>

									<div class="col-md-6">
										<label for="phone">Telefone *</label>
										<input type="text" class="form-control phone" id="phone" name="phone" value="{{ (($companySelect->phone) ? $companySelect->phone : NULL) }}" autocomplete="off">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-gray" data-collapsed="0">
						<div class="panel-heading">
							<div class="panel-title">
								<h4><strong>Dados da Empresa</strong></h4>
							</div>
							<hr>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="segment">Segmento</label>
										<select name="segment" id="segment" class="form-control" style="background-color:#ffffff;pointer-events:none;touch-action:none;" readonly aria-disabled="true">
											<option value="">Selecione o segmento *</option>
											@foreach ($segments as $rowseg)
												<option value="{{ $rowseg->id }}" {{ (($companySelect->categories_id) ? (($rowseg->id == $companySelect->categories_id) ? 'selected' : NULL) : NULL) }}>{{ $rowseg->title }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="type_person">Tipo de empresa</label>
										<select name="type_person" id="type_person" class="form-control" style="background-color:#ffffff;pointer-events:none;touch-action:none;" readonly aria-disabled="true">
											<option value="">Selecione o tipo de empresa *</option>
											<option value="1" {{ (($companySelect->type_of_person) ? (($companySelect->type_of_person == 1) ? 'selected' : NULL) : NULL) }}>Pessoa Física</option>
											<option value="2" {{ (($companySelect->type_of_person) ? (($companySelect->type_of_person == 2) ? 'selected' : NULL) : NULL) }}>Pessoa Jurídica</option>
										</select>
									</div>

									<div class="col-md-6">
										<label for="cell_phone">WhatsApp *</label>
										<input type="text" name="cell_phone" id="cell_phone" class="form-control phone" value="{{ (($companySelect->cell_phone) ? $companySelect->cell_phone : NULL) }}" autocomplete="off">
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<label for="type_person">Nome da Empresa *</label>
										<input type="text" name="fantasy_name" id="fantasy_name" class="form-control" value="{{ (($companySelect->fantasy_name) ? $companySelect->fantasy_name : NULL) }}" autocomplete="off">
										<small><i>Obs: Este nome será exibido para os usuários</i></small>
									</div>
								</div>
							</div>

							@if ($companySelect->type_of_person == 1)
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<label for="type_person">CPF *</label>
											<input type="text" name="reg_of_individuals" id="reg_of_individuals" class="form-control cpf" value="{{ (($companySelect->reg_of_individuals) ? $companySelect->reg_of_individuals : NULL) }}" autocomplete="off">
										</div>

										<div class="col-md-6">
											<label for="type_person">RG</label>
											<input type="text" name="id_card" id="id_card" class="form-control" value="{{ (($companySelect->id_card) ? $companySelect->id_card : NULL) }}" autocomplete="off">
										</div>
									</div>
								</div>
							@endif

							@if ($companySelect->type_of_person == 2)
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<label for="corporate_name">Razão Social *</label>
											<input type="text" name="corporate_name" id="corporate_name" class="form-control cpf" value="{{ (($companySelect->corporate_name) ? $companySelect->corporate_name : NULL) }}" autocomplete="off">
										</div>

										<div class="col-md-6">
											<label for="reg_legal_entity">CNPJ *</label>
											<input type="text" name="reg_legal_entity" id="reg_legal_entity" class="form-control" value="{{ (($companySelect->reg_legal_entity) ? $companySelect->reg_legal_entity : NULL) }}" autocomplete="off">
										</div>
									</div>
								</div>
							@endif
						</div>
					</div>

					<div class="panel panel-gray" data-collapsed="0">
						<div class="panel-heading">
							<div class="panel-title">
								<h4><strong>Descrição da empresa</strong></h4>
							</div>
							<hr>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<textarea name="description" id="description" class="form-control" rows="4">{{ (($companySelect->description) ? $companySelect->description : NULL) }}</textarea>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-gray" data-collapsed="0">
						<div class="panel-heading">
							<div class="panel-title">
								<h4><strong>Imagens</strong></h4>
							</div>
							<hr>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="row">
									<div class="col-md-6"><strong>Logo</strong></div>
									<div class="col-md-6"><strong>Banner</strong></div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<div class="fileinput fileinput-new" data-provides="fileinput">
											<div class="fileinput-new thumbnail" style="width:100%" data-trigger="fileinput">
												@if ($imageLogo)
													<img src="{{ Storage::disk('s3')->url($folderUpload . $companySelect->id . '/' . $imageLogo->file) }}">
												@else
													<img src="http://placehold.it/{{ $widthLarge }}x{{ $heightLarge }}">
												@endif
											</div>
											<div class="fileinput-preview fileinput-exists thumbnail" style="max-width:{{ $widthLarge }}px;max-height:{{ $heightLarge }}px;"></div>
											<div>
												<span class="btn btn-white btn-file">
													<span class="fileinput-new">Selecione Imagem</span>
													<span class="fileinput-exists">Alterar Imagem</span>
													<input type="file" name="image_logo" accept="image/*">
												</span>
												<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remover Imagem</a>
											</div>
										</div>
									</div>

									<div class="col-md-6">
										<div class="fileinput fileinput-new" data-provides="fileinput">
											<div class="fileinput-new thumbnail" style="width:100%" data-trigger="fileinput">
												@if ($imageBanner)
													<img src="{{ Storage::disk('s3')->url($folderUpload . $companySelect->id . '/' . $imageBanner->file) }}">
												@else
													<img src="http://placehold.it/{{ $widthLargeBanner }}x{{ $heightLargeBanner }}">
												@endif
											</div>
											<div class="fileinput-preview fileinput-exists thumbnail" style="max-width:{{ $widthLargeBanner }}px;max-height:{{ $heightLargeBanner }}px;"></div>
											<div>
												<span class="btn btn-white btn-file">
													<span class="fileinput-new">Selecione Imagem</span>
													<span class="fileinput-exists">Alterar Imagem</span>
													<input type="file" name="image_banner" accept="image/*">
												</span>
												<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remover Imagem</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="panel panel-gray" data-collapsed="0">
						<div class="panel-heading">
							<div class="panel-title">
								<h4><strong>Endereço</strong></h4>
							</div>
							<hr>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="corporate_name">Título *</label>
										<input type="text" name="title" id="title" class="form-control" readonly value="{{ (($address->title) ? $address->title : NULL) }}" style="background-color:#ffffff;" autocomplete="off">
									</div>

									<div class="col-md-6">
										<label for="corporate_name">CEP *</label>
										<input type="text" name="zip_code" id="zip_code" class="form-control zipcode" value="{{ (($address->zip_code) ? $address->zip_code : NULL) }}" autocomplete="off">
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="corporate_name">Estado *</label>
										<select name="state" id="state" class="form-control" style="background-color:#ffffff;" onchange="selectCities(this.value)">
											<option value="">Selecione</option>
											@foreach ($states as $rowsta)
												<option value="{{ $rowsta->id }}" {{ (($address->states_id) ? (($rowsta->id == $address->states_id) ? 'selected' : NULL) : NULL) }}>{{ $rowsta->abbreviation . ' - ' . $rowsta->title }}</option>
											@endforeach
										</select>
									</div>

									<div class="col-md-6">
										<label for="corporate_name">Cidade *</label>
										<select name="city" id="city" class="form-control" style="background-color:#ffffff;">
											<option value="">Selecione</option>
											@foreach ($cities as $rowcit)
												<option value="{{ $rowcit->id }}" {{ (($address->cities_id) ? (($rowcit->id == $address->cities_id) ? 'selected' : NULL) : NULL) }}>{{ $rowcit->title }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-8">
										<label for="corporate_name">Endereço *</label>
										<input type="text" name="address" id="address" class="form-control" value="{{ (($address->address) ? $address->address: NULL) }}" autocomplete="off">
									</div>

									<div class="col-md-4">
										<label for="corporate_name">Número *</label>
										<input type="number" name="number" id="number" class="form-control" value="{{ (($address->number) ? $address->number: NULL) }}" autocomplete="off" onkeypress="return checkNumber(event)">
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="corporate_name">Complemento</label>
										<input type="text" name="complement" id="complement" class="form-control" value="{{ (($address->complement) ? $address->complement : NULL) }}" autocomplete="off">
									</div>
									
									<div class="col-md-6">
										<label for="corporate_name">Bairro *</label>
										<input type="text" name="neighborhood" id="neighborhood" class="form-control" value="{{ (($address->neighborhood) ? $address->neighborhood : NULL) }}" autocomplete="off">
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- <div class="panel panel-gray" data-collapsed="0">
						<div class="panel-heading">
							<div class="panel-title">
								<h4><strong>Valor da Entrega</strong></h4> 
							</div>
							<hr>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="row">
									<div class="col-md-3">
										<input type="text" name="value_delivery" id="value_delivery" class="form-control value" placeholder="5,00" value="{{ (($companySelect->value_delivery) ? number_format($companySelect->value_delivery, 2, ',', '.') : NULL) }}" autocomplete="off">
									</div>

									<div class="col-md-9">
										<i>Inform o valor fixo que será cobrado pela entrega da mercadoria e caso o valor não sejá informado, aparecerá como entrega gratuita</i>
									</div>
								</div>
							</div>
						</div>
					</div> -->

					<div class="panel panel-gray" data-collapsed="0">
						<div class="panel-heading">
							<div class="panel-title">
								<h4><strong>Delivery e Retirada</strong></h4> 
							</div>
							<hr>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="row">
									<div class="col-md-3">
										<label for="delivery">Delivery</label>
										<select name="delivery" id="delivery" class="form-control" style="background-color:#ffffff;">
											<option value="">Selecione</option>
											<option value="0" {{ (($companySelect->delivery == 0) ? 'selected' : NULL) }}>Não</option>
											<option value="1" {{ (($companySelect->delivery == 1) ? 'selected' : NULL) }}>Sim</option>
										</select>
									</div>

									<div class="col-md-3">
										<label for="withdraw">Retirar no local</label>
										<select name="withdraw" id="withdraw" class="form-control" style="background-color:#ffffff;">
											<option value="">Selecione</option>
											<option value="0" {{ (($companySelect->withdraw == 0) ? 'selected' : NULL) }}>Não</option>
											<option value="1" {{ (($companySelect->withdraw == 1) ? 'selected' : NULL) }}>Sim</option>
										</select>
									</div>
								</div>
							</div>

							<div class="form-group" id="settings_delivery" style="{{ (is_null($companySelect->delivery) ? 'display:none;' : NULL) }}">
								<div class="row" style="margin-bottom:6px;">
									<div class="col-md-12">
										<strong>Configurações Delivery</strong>
									</div>
								</div>

								<div class="row">
									<div class="col-md-4">
										<label for="settings_fee">Configurar Taxas</label>
										<select name="settings_fee" id="settings_fee" class="form-control" style="background-color:#ffffff;">
											<option value="">Selecione</option>
											<option value="1" {{ (($companySelect->settings_fee == 1) ? 'selected' : NULL) }}>Taxa de entrega única</option>
											<option value="2" {{ (($companySelect->settings_fee == 2) ? 'selected' : NULL) }}>Taxa de entrega dinâmicas</option>
										</select>
									</div>

									<div class="col-md-3" id="onetime_fee" style="{{ (($companySelect->settings_fee == 1) ? NULL : 'display:none;') }}">
										<label for="value_delivery">Valor entrega única</label>
										<input type="text" name="value_delivery" id="value_delivery" class="form-control value" placeholder="5,00" value="{{ (($companySelect->value_delivery) ? number_format($companySelect->value_delivery, 2, '.', ',') : NULL) }}" autocomplete="off">
									</div>

									<div class="col-md-3" id="dynamic_rate" style="{{ (($companySelect->settings_fee == 2) ? NULL : 'display:none;') }}">
										<button class="btn btn-blue btn-icon icon-left" type="button" onclick="addDynamicRate()" style="margin:23px 0 0;">
											<i class="entypo-link"></i> Vincular área de entrega
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-gray" data-collapsed="0">
						<div class="panel-heading">
							<div class="panel-title">
								<h4><strong>Plano Escolhido</strong></h4>
							</div>
							<hr>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<select name="plan" id="plan" class="form-control" style="background-color:#ffffff;">
											<option value="">Selecione o plano *</option>
											@foreach ($plans as $plan)
												<option value="{{ $plan->id }}" {{ (($companySelect->plans_id) ? (($plan->id == $companySelect->plans_id) ? 'selected' : NULL) : NULL) }}>{{ $plan->title . ' (R$ ' . number_format($plan->value, 2, ',', '.') . ')' }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-gray" data-collapsed="0">
						<div class="panel-heading">
							<div class="panel-title">
								<h4><strong>Horários de Funcionamento</strong></h4>
								<small>Caso o horário de encerramento do estabelcimento ultrapasse às 23h59m é preciso marcar a opção "CTPM"</small>
							</div>
							<hr>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="row">
									<div class="col-md-6"><strong>Dia da semana</strong></div>
									<div class="col-md-2 text-center"><strong>Abertura</strong></div>
									<div class="col-md-2 text-center"><strong>Fechamento</strong></div>
									<div class="col-md-2 text-center"><strong>CTPM</strong></div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<?php (($companySelect->hours_monday) ? list($dayMo, $hourIMo, $hourEMo, $ctpmMo) = explode('|', $companySelect->hours_monday) : NULL); ?>
									<div class="col-md-6">Segunda-Feira</div>
									<div class="col-md-2">
										<input type="text" name="monday_start" id="monday_start" class="form-control hours" placeholder="08:00" value="{{ (isset($hourIMo) ? $hourIMo : NULL) }}" autocomplete="off">
									</div>
									<div class="col-md-2">
										<input type="text" name="monday_finish" id="monday_finish" class="form-control hours" placeholder="18:00" value="{{ (isset($hourEMo) ? $hourEMo : NULL) }}" autocomplete="off">
									</div>
									<div class="col-md-2 text-center">
										<input type="checkbox" name="monday_ctpm" id="monday_ctpm" value="1" {{ (isset($ctpmMo) ? (($ctpmMo == 1) ? 'checked' : NULL) : NULL) }}>
									</div>
								</div>

								<div class="row">
								<?php (($companySelect->hours_tuesday) ? list($dayTu, $hourITu, $hourETu, $ctpmTu) = explode('|', $companySelect->hours_tuesday) : NULL); ?>
									<div class="col-md-6">Terça-Feira</div>
									<div class="col-md-2">
										<input type="text" name="tuesday_start" id="tuesday_start" class="form-control hours" placeholder="08:00" value="{{ (isset($hourITu) ? $hourITu : NULL) }}" autocomplete="off">
									</div>
									<div class="col-md-2">
										<input type="text" name="tuesday_finish" id="tuesday_finish" class="form-control hours" placeholder="18:00" value="{{ (isset($hourETu) ? $hourETu : NULL) }}" autocomplete="off">
									</div>
									<div class="col-md-2 text-center">
										<input type="checkbox" name="tuesday_ctpm" id="tuesday_ctpm" value="1" {{ (isset($ctpmTu) ? (($ctpmTu == 1) ? 'checked' : NULL) : NULL) }}>
									</div>
								</div>

								<div class="row">
								<?php (($companySelect->hours_wednesday) ? list($dayWe, $hourIWe, $hourEWe, $ctpmWe) = explode('|', $companySelect->hours_wednesday) : NULL); ?>
									<div class="col-md-6">Quarta-Feira</div>
									<div class="col-md-2">
										<input type="text" name="wednesday_start" id="wednesday_start" class="form-control hours" placeholder="08:00" value="{{ (isset($hourIWe) ? $hourIWe : NULL) }}" autocomplete="off">
									</div>
									<div class="col-md-2">
										<input type="text" name="wednesday_finish" id="wednesday_finish" class="form-control hours" placeholder="18:00" value="{{ (isset($hourEWe) ? $hourEWe : NULL) }}" autocomplete="off">
									</div>
									<div class="col-md-2 text-center">
										<input type="checkbox" name="wednesday_ctpm" id="wednesday_ctpm" value="1" {{ (isset($ctpmWe) ? (($ctpmWe == 1) ? 'checked' : NULL) : NULL) }}>
									</div>
								</div>

								<div class="row">
								<?php (($companySelect->hours_thursday) ? list($dayTh, $hourITh, $hourETh, $ctpmTh) = explode('|', $companySelect->hours_thursday) : NULL); ?>
									<div class="col-md-6">Quinta-Feira</div>
									<div class="col-md-2">
										<input type="text" name="thursday_start" id="thursday_start" class="form-control hours" placeholder="08:00" value="{{ (isset($hourITh) ? $hourITh : NULL) }}" autocomplete="off">
									</div>
									<div class="col-md-2">
										<input type="text" name="thursday_finish" id="thursday_finish" class="form-control hours" placeholder="18:00" value="{{ (isset($hourETh) ? $hourETh : NULL) }}" autocomplete="off">
									</div>
									<div class="col-md-2 text-center">
										<input type="checkbox" name="thursday_ctpm" id="thursday_ctpm" value="1" {{ (isset($ctpmTh) ? (($ctpmTh == 1) ? 'checked' : NULL) : NULL) }}>
									</div>
								</div>

								<div class="row">
								<?php (($companySelect->hours_friday) ? list($dayFr, $hourIFr, $hourEFr, $ctpmFr) = explode('|', $companySelect->hours_friday) : NULL); ?>
									<div class="col-md-6">Sexta-Feira</div>
									<div class="col-md-2">
										<input type="text" name="friday_start" id="friday_start" class="form-control hours" placeholder="08:00" value="{{ (isset($hourIFr) ? $hourIFr : NULL) }}" autocomplete="off">
									</div>
									<div class="col-md-2">
										<input type="text" name="friday_finish" id="friday_finish" class="form-control hours" placeholder="18:00" value="{{ (isset($hourEFr) ? $hourEFr : NULL) }}" autocomplete="off">
									</div>
									<div class="col-md-2 text-center">
										<input type="checkbox" name="friday_ctpm" id="friday_ctpm" value="1" {{ (isset($ctpmFr) ? (($ctpmFr == 1) ? 'checked' : NULL)  : NULL) }}>
									</div>
								</div>

								<div class="row">
								<?php (($companySelect->hours_saturday) ? list($daySa, $hourISa, $hourESa, $ctpmSa) = explode('|', $companySelect->hours_saturday) : NULL); ?>
									<div class="col-md-6">Sábado</div>
									<div class="col-md-2">
										<input type="text" name="saturday_start" id="saturday_start" class="form-control hours" placeholder="08:00" value="{{ (isset($hourISa) ? $hourISa : NULL) }}" autocomplete="off">
									</div>
									<div class="col-md-2">
										<input type="text" name="saturday_finish" id="saturday_finish" class="form-control hours" placeholder="18:00" value="{{ (isset($hourESa) ? $hourESa : NULL) }}" autocomplete="off">
									</div>
									<div class="col-md-2 text-center">
										<input type="checkbox" name="saturday_ctpm" id="saturday_ctpm" value="1" {{ (isset($ctpmSa) ? (($ctpmSa == 1) ? 'checked' : NULL)  : NULL) }}>
									</div>
								</div>

								<div class="row">
								<?php (($companySelect->hours_sunday) ? list($daySu, $hourISu, $hourESu, $ctpmSu) = explode('|', $companySelect->hours_sunday) : NULL); ?>
									<div class="col-md-6">Domingo</div>
									<div class="col-md-2">
										<input type="text" name="sunday_start" id="sunday_start" class="form-control hours" placeholder="08:00" value="{{ (isset($hourISu) ? $hourISu : NULL) }}" autocomplete="off">
									</div>
									<div class="col-md-2">
										<input type="text" name="sunday_finish" id="sunday_finish" class="form-control hours" placeholder="18:00" value="{{ (isset($hourESu) ? $hourESu : NULL) }}" autocomplete="off">
									</div>
									<div class="col-md-2 text-center">
										<input type="checkbox" name="sunday_ctpm" id="sunday_ctpm" value="1" {{ (isset($ctpmSu) ? (($ctpmSu == 1) ? 'checked' : NULL)  : NULL) }}>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="pull-right">
						<a class="btn btn-red btn-icon icon-left btn-sm" href="{{ route('control.account.edit') }}" style="margin-right:15px;"><i class="entypo-cancel"></i> Cancelar</a>
						<button class="btn btn-green btn-icon icon-left btn-sm" type="submit" id="buttonForm"><i class="entypo-download"></i> Salvar</button>
					</div>
				</div>
			</div>

		</form>

	</section>
</div>

@endsection