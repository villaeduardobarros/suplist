@extends('control.layouts.app')

@section('content')

<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="{{ route('control.home') }}"><i class="entypo-home"></i> Home</a></li>
		<li class="active"><i class="entypo-layout"></i> Clientes</li>
	</ol>
	<section class="content">
		<div class="row">

			<div class="col-md-12">
				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered datatable dataTable">
							<thead>
								<tr>
									<th width="30%">Nome</th>
									<th width="30%">E-mail</th>
									<th width="10%">Último Pedido</th>
								</tr>
							</thead>
							<tbody>

								@foreach ($customers as $rowcus)

									<tr>
										<td>
											{{ $rowcus->name }}
										</td>

										<td align="center">
											{{ $rowcus->email }}
										</td>

										<td align="center">
											{{ $rowcus->last_wish }}
										</td>
									</tr>

								@endforeach

							</tbody>
						</table>

					</div>
				</div>
			</div>

		</div>
	</section>
</div>

@endsection