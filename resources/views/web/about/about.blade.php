@extends('web.layouts.app')

@section('content')

<div class="container my-5 py-5 ">
	<section class="px-md-5 mx-md-5 text-center dark-grey-text">
		<div class="row">

			<div class="col-md-6 mb-4 mb-md-0">
				<h1 class="font-weight-bold mt-5">SUPList - Seu catalogo de produtos digital</h1>

				<p class="text-muted">Faça já seu catalogo de produtos online e disponibilize para seus clientes façam pedidos direto no whatsapp, sem intermediários.</p>

				<a class="btn btn-amber btn-md ml-0" href="{{ route('web.register') }}" role="button">
					Teste Grátis por {{ SettingsHelperWeb::searchItem('test-days') }} dias <i class="fa fa-gem ml-2"></i>
				</a>

				<hr class="my-3">

				<p class="font-weight-bold">Acompanhe nas redes sociais:</p>

				<a href="{{ SettingsHelperWeb::searchItem('link-facebook') }}" target="_blank" class="mx-1" role="button"><i class="fab fa-facebook-f"></i></a>
				<a href="{{ SettingsHelperWeb::searchItem('link-instagram') }}" target="_blank" class="mx-1" role="button"><i class="fab fa-twitter"></i></a>
				<a href="#" target="_blank" class="mx-1" role="button"><i class="fab fa-linkedin-in"></i></a>
				<a href="#" target="_blank" class="mx-1" role="button"><i class="fab fa-instagram"></i></a>
			</div>

			<div class="col-md-5 mb-4 mb-md-0">
				<img src="web/img/about.jpg" class="img-fluid" alt="">
			</div>

		</div>
	</section>
</div>

<section class=" indigo lighten-5 ">
	<div class="container mt-5 py-5 ">
		<section class="dark-grey-text">

			<h2 class="text-center font-weight-bold mb-4 pb-2">Crie seu catálogo em minutos e comece a receber pedidos via WhatsApp</h2>

			<div class="row">
				<div class="col-lg-5 text-center text-lg-left">
					<img class="img-fluid" src="web/img/about2.jpg" alt="Sample image">
				</div>

				<div class="col-lg-7">
					<div class="row mb-3">
						<div class="col-1">
							<i class="fas fa-share fa-lg indigo-text"></i>
						</div>

						<div class="col-xl-10 col-md-11 col-10">
							<h5 class="font-weight-bold mb-3">Catalogo de produtos</h5>
							<p class="grey-text">Ofereça aos seus clientes uma experiência de compra mobile simples e rápida.</p>
						</div>
					</div>

					<div class="row mb-3">
						<div class="col-1">
							<i class="fas fa-share fa-lg indigo-text"></i>
						</div>

						<div class="col-xl-10 col-md-11 col-10">
							<h5 class="font-weight-bold mb-3">Pedidos via Whatsapp</h5>
							<p class="grey-text">Lista de pedidos enviados para o WhatsApp da sua loja.</p>
						</div>
					</div>

					<div class="row">
						<div class="col-1">
							<i class="fas fa-share fa-lg indigo-text"></i>
						</div>

						<div class="col-xl-10 col-md-11 col-10">
							<h5 class="font-weight-bold mb-3">Sem taxas sobre transações</h5>
							<p class="grey-text mb-0">Acompanhe suas vendas de forma simples e prática, sem pagar a mais por isso.</p>
						</div>
					</div>
				</div>
			</div>

		</section>
	</div>
</section>

<section class=" deep-purple lighten-2 z-depth-1  my-0">
	<div class="container my-0 py-5 ">
		<section class="text-center white-text  pt-5 px-lg-5">

			<h3 class="font-weight-bold mt-3 mb-4 pb-2">Ecolha o seu plano</h3>

			<p>Sem contrato mínimo, cancele quando quiser e pague apenas enquanto estiver ativo.</p>

			<div class="row mb-5">

				@foreach($plans as $plan)

					<div class="col-lg-4 col-md-12 mb-4">
						<div class="pricing-card">
							<div class="card-body">
								<p class="display-3"><span class="h5 font-weight-normal pr-2">R$</span>{{ number_format((($plan->value_promotional) ? $plan->value_promotional : $plan->value), 2, ',', '.') }}</p>

								<h5 class="text-uppercase font-weight-normal mb-4">{{ $plan->title }}</h5>

								<p class="mb-4 pb-2">Menos de R$2,00 por dia</p>

								<a href="{{ route('web.register') }}" class="btn btn-outline-white btn-md btn-rounded btn-block">EU QUERO</a>
							</div>
						</div>
					</div>

				@endforeach

			</div>

		</section>
	</div>
</section>

@endsection
