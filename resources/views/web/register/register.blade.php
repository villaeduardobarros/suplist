@extends('web.layouts.app')

@section('content')

<div class="container my-5 py-5 ">
	<section class="px-md-5 mx-md-5 text-center text-lg-left dark-grey-text">

		<div class="row d-flex justify-content-center">
			<div class="col-md-10 col-lg-8">

				<form class="text-center" action="{{ route('web.register.store') }}" id="formRegisterDraft" method="post" enctype="multipart/form-data">
					@csrf
					<input type="hidden" name="affiliate_id" value="{{ $tokenAffiliateId }}">

					<h1 class="h3 mt-4 mb-2">CADASTRE SUA EMPRESA GRATUITAMENTE</h1>

					<p class="mb-5">Crie já seu catalogo de produtos online e receba pedidos via Whatsapp.</p>

					@if (Session::has('message'))
						<p class="message mb-4" style="color:#ff5d5d;margin:0 0 20px;">{{ Session::get('message') }}</p>
					@else
						<p class="message mb-4" style="margin:0 0 20px;"></p>
					@endif

					<div class="row mb-4 text-left">
						<div class="col-md-12 col-lg-12">
							<input type="text" name="name" id="name" class="form-control" placeholder="Nome responsável" autocomplete="off" required>
						</div>
					</div>

					<div class="row mb-4 text-left">
						<div class="col-md-12 col-lg-12">
							<input type="email" name="email" id="email" class="form-control" placeholder="E-mail" autocomplete="off" required>
						</div>
					</div>

					<div class="row mb-4 text-left">
						<div class="col-md-12 col-lg-12">
							<input type="phone" name="phone" id="phone" class="form-control phone" placeholder="Telefone" autocomplete="off" required>
						</div>
					</div>

					<div class="row mb-3">
						<div class="col-md-12 col-lg-12">
							<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="news" name="news" value="1" checked>
								<label class="custom-control-label" for="news"> Desejo receber novidades por e-mail</label>
							</div>
						</div>
					</div>

					<button class="btn btn-success my-5 btn-block" type="submit">CADASTRAR</button>

					<hr>

					<p>Clicando em <strong><em>Cadastrar</em></strong> você está concordando com os <a href="termos-de-uso" target="_blank">termos de serviços</a>.</p>
				</form>

			</div>
		</div>

	</section>
</div>

@endsection