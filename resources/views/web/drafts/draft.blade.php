@extends('web.layouts.app-simple')

@section('content')

<div class="container my-5 py-5 ">
	<section class="px-md-5 mx-md-5 text-center text-lg-left dark-grey-text">

		<div class="row d-flex justify-content-center">
			<div class="col-md-10 col-lg-8">

				<form class="text-center" action="{{ route('web.draft.store') }}" id="formCompleteDraft" method="post" enctype="multipart/form-data">
					@csrf
					<input type="hidden" name="affiliates_id" value="{{ $affiliatesId }}">

					<h1 class="h3 mt-4 mb-2">COMPLETE SEU CADASTRO</h1>

					<p class="mb-5">
						Informe os dados obrigatórios para finalizar ser cadastro e liberar o acesso ao painel aonde poderá criar seu catalogo de produtos online e visualizar os pedidos recebidos via Whatsapp.
					</p>

					<p class="message mb-4" style="margin:0 0 20px;"></p>

					<p style="font-weight:bold;text-align:left;">Dados de Acesso</p>

					<div class="row mb-4">
						<div class="col-md-12 col-lg-6">
							<input type="email" name="email" id="email" class="form-control" placeholder="E-mail" autocomplete="off" required value="{{ ((Session::has('drafts_email')) ? Session::get('drafts_email') : NULL) }}">
						</div>
						<div class="col-md-12 col-lg-6">
							<input type="password" name="password" id="password" class="form-control" placeholder="Nova senha *" required>
						</div>
					</div>

					<p style="font-weight:bold;text-align:left;">Dados do Responsável</p>

					<div class="row mb-4">
						<div class="col-md-12 col-lg-6">
							<input type="text" name="name" id="name" class="form-control" placeholder="Nome responsável" autocomplete="off" required value="{{ ((Session::has('drafts_name')) ? Session::get('drafts_name') : NULL) }}">
						</div>
						<div class="col-md-12 col-lg-6">
							<input type="phone" name="phone" id="phone" class="form-control phone" placeholder="Telefone *" autocomplete="off" required value="{{ ((Session::has('drafts_phone')) ? Session::get('drafts_phone') : NULL) }}">
						</div>
					</div>

					<p style="font-weight:bold;text-align:left;">Dados da Empresa</p>

					<div class="row mb-4">
						<div class="col-md-12 col-lg-6">
							<select name="segment" id="segment" class="form-control" required>
								<option value="">Selecione o segmento *</option>
								@foreach ($segments as $segment)
									<option value="{{ $segment->id }}">{{ $segment->title }}</option>
								@endforeach
								<option value="other">Outros</option>
							</select>
						</div>
						<div class="col-md-12 col-lg-6">
							<input type="text" name="other" id="other" class="form-control" placeholder="Novo segmento" autocomplete="off" style="display:none;">
						</div>
					</div>

					<div class="row mb-4">
						<div class="col-md-12 col-lg-6">
							<select name="type_person" id="type_person" class="form-control" required>
								<option value="">Selecione o tipo de empresa *</option>
								<option value="1">Pessoa Física</option>
								<option value="2">Pessoa Jurídica</option>
							</select>
						</div>
						<div class="col-md-12 col-lg-6">
							<input type="text" name="cell_phone" id="cell_phone" class="form-control phone" placeholder="WhatsApp *" autocomplete="off" required>
						</div>
					</div>

					<div class="row mb-4">
						<div class="col-md-12 col-lg-12">
							<input type="text" name="fantasy_name" id="fantasy_name" class="form-control" placeholder="Nome da Empresa * (este nome será exibido para os usuários)" autocomplete="off" required>
						</div>
					</div>

					<div id="individuals" style="display:none;">
						<div class="row mb-4">
							<div class="col-md-12 col-lg-6">
								<input type="text" name="reg_of_individuals" id="reg_of_individuals" class="form-control cpf" placeholder="CPF *" autocomplete="off">
							</div>
							<div class="col-md-12 col-lg-6">
								<input type="text" name="id_card" id="id_card" class="form-control" placeholder="RG" autocomplete="off">
							</div>
						</div>
					</div>

					<div id="companies" style="display:none;">
						<div class="row mb-4">
							<div class="col-md-12 col-lg-12">
								<input type="text" name="company_name" id="company_name" class="form-control" placeholder="Razão Social *" autocomplete="off">
							</div>
						</div>

						<div class="row mb-4">
							<div class="col-md-12 col-lg-6">
								<input type="text" name="reg_legal_entity" id="reg_legal_entity" class="form-control cnpj" placeholder="CNPJ *" autocomplete="off">
							</div>
						</div>
					</div>

					<p style="font-weight:bold;text-align:left;">Descrição</p>

					<div class="row mb-4">
						<div class="col-md-12 col-lg-12">
							<textarea name="description" id="description" class="form-control" rows="3" placeholder="Descrição da empresa"></textarea>
						</div>
					</div>

					<p style="font-weight:bold;text-align:left;">Endereço</p>

					<div class="row mb-4">
						<div class="col-md-6 col-lg-8">
							<input type="text" name="title" id="title" class="form-control" placeholder="Título *" autocomplete="off" required value="Principal">
						</div>
						<div class="col-md-6 col-lg-4">
							<input type="text" name="zip_code" id="zip_code" class="form-control zipcode" placeholder="CEP *" autocomplete="off" required>
						</div>
					</div>
					
					<div class="row mb-4">
						<div class="col-md-12 col-lg-6">
							<select name="state" id="state" class="form-control" onchange="selectCities(this.value)">
								<option value="">Selecione o estado *</option>
								@foreach ($states as $state)
									<option value="{{ $state->id }}">{{ $state->abbreviation . ' - ' . $state->title }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-12 col-lg-6">
							<select name="city" id="city" class="form-control">
								<option value="">Selecione o estado antes *</option>
							</select>
						</div>
					</div>
					
					<div class="row mb-4">
						<div class="col-md-6 col-lg-8">
							<input type="text" name="address" id="address" class="form-control" placeholder="Endereço *" autocomplete="off" required>
						</div>
						<div class="col-md-6 col-lg-4">
							<input type="number" name="number" id="number" class="form-control" placeholder="Número *" autocomplete="off" required onkeypress="return checkNumber(event)">
						</div>
					</div>

					<div class="row mb-4">
						<div class="col-md-12 col-lg-6">
							<input type="text" name="complement" id="complement" class="form-control" placeholder="Complemento" autocomplete="off">
						</div>
						<div class="col-md-12 col-lg-6">
							<input type="text" name="neighborhood" id="neighborhood" class="form-control" placeholder="Bairro *" autocomplete="off" required>
						</div>
					</div>

					<p style="font-weight:bold;text-align:left;">Valor da Entrega</p>

					<div class="row mb-4">
						<div class="col-md-6 col-lg-3">
							<input type="text" name="value_delivery" id="value_delivery" class="form-control value" placeholder="5,00" autocomplete="off">
						</div>
						<div class="col-md-6 col-lg-9" style="padding:7px !important;text-align:left;">
							* caso o valor não sejá informado, aparecerá como gratuita
						</div>
					</div>

					<!-- <p style="font-weight:bold;text-align:left;">Horários de Funcionamento</p>

					<div class="row mb-4">
						<div class="col-md-12 col-lg-6" style="padding:7px !important;">Segunda-Feira</div>
						<div class="col-md-6 col-lg-3">
							<input type="text" name="monday_start" id="monday_start" class="form-control hours" placeholder="08:00" autocomplete="off">
						</div>
						<div class="col-md-6 col-lg-3">
							<input type="text" name="monday_finish" id="monday_finish" class="form-control hours" placeholder="18:00" autocomplete="off">
						</div>
					</div>

					<div class="row mb-4">
						<div class="col-md-12 col-lg-6" style="padding:7px !important;">Terça-Feira</div>
						<div class="col-md-6 col-lg-3">
							<input type="text" name="tuesday_start" id="tuesday_start" class="form-control hours" placeholder="08:00" autocomplete="off">
						</div>
						<div class="col-md-6 col-lg-3">
							<input type="text" name="tuesday_finish" id="tuesday_finish" class="form-control hours" placeholder="18:00" autocomplete="off">
						</div>
					</div>

					<div class="row mb-4">
						<div class="col-md-12 col-lg-6" style="padding:7px !important;">Quarta-Feira</div>
						<div class="col-md-6 col-lg-3">
							<input type="text" name="wednesday_start" id="wednesday_start" class="form-control hours" placeholder="08:00" autocomplete="off">
						</div>
						<div class="col-md-6 col-lg-3">
							<input type="text" name="wednesday_finish" id="wednesday_finish" class="form-control hours" placeholder="18:00" autocomplete="off">
						</div>
					</div>

					<div class="row mb-4">
						<div class="col-md-12 col-lg-6" style="padding:7px !important;">Quinta-Feira</div>
						<div class="col-md-6 col-lg-3">
							<input type="text" name="thursday_start" id="thursday_start" class="form-control hours" placeholder="08:00" autocomplete="off">
						</div>
						<div class="col-md-6 col-lg-3">
							<input type="text" name="thursday_finish" id="thursday_finish" class="form-control hours" placeholder="18:00" autocomplete="off">
						</div>
					</div>

					<div class="row mb-4">
						<div class="col-md-12 col-lg-6" style="padding:7px !important;">Sexta-Feira</div>
						<div class="col-md-6 col-lg-3">
							<input type="text" name="friday_start" id="friday_start" class="form-control hours" placeholder="08:00" autocomplete="off">
						</div>
						<div class="col-md-6 col-lg-3">
							<input type="text" name="friday_finish" id="friday_finish" class="form-control hours" placeholder="18:00" autocomplete="off">
						</div>
					</div>

					<div class="row mb-4">
						<div class="col-md-12 col-lg-6" style="padding:7px !important;">Sábado</div>
						<div class="col-md-6 col-lg-3">
							<input type="text" name="saturday_start" id="saturday_start" class="form-control hours" placeholder="08:00" autocomplete="off">
						</div>
						<div class="col-md-6 col-lg-3">
							<input type="text" name="saturday_finish" id="saturday_finish" class="form-control hours" placeholder="18:00" autocomplete="off">
						</div>
					</div>

					<div class="row mb-4">
						<div class="col-md-12 col-lg-6" style="padding:7px !important;">Domingo</div>
						<div class="col-md-6 col-lg-3">
							<input type="text" name="sunday_start" id="sunday_start" class="form-control hours" placeholder="08:00" autocomplete="off">
						</div>
						<div class="col-md-6 col-lg-3">
							<input type="text" name="sunday_finish" id="sunday_finish" class="form-control hours" placeholder="18:00" autocomplete="off">
						</div>
					</div> -->

					<p style="font-weight:bold;text-align:left;">Escolha seu plano</p>

					<div class="row mb-4">
						<div class="col-md-6 col-lg-6">
							<select name="plan" id="plan" class="form-control">
								<option value="">Selecione o plano *</option>
								@foreach ($plans as $plan)
									<option value="{{ $plan->id }}">{{ $plan->title . ' (R$ ' . number_format($plan->value, 2, ',', '.') . ')' }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<!-- <p style="font-weight:bold;text-align:left;">Logo da Empresa</p>

					<div class="row mb-4">
						<div class="col-md-12 col-lg-5">
							<button type="button" class="btn btn-default btn-block" id="btnSelectFile" style="padding:10px !important;">Selecione a imagem</button>
							<input type="file" name="image" id="image" class="form-control" accept="image/*" style="display:none;height:0;width:0;">
						</div>
						<div class="col-md-12 col-lg-7">
							<input type="text" id="fileSelected" class="form-control" disabled placeholder="" style="background-color:transparent !important;">
						</div>
					</div> -->

					<input type="hidden" name="drafts_id" <?php echo (($request->session()->exists('drafts_id')) ? 'value="' . $request->session()->get('drafts_id') . '"' : NULL); ?>>
					<button class="btn btn-success my-5 btn-block" type="submit">Cadastrar</button>
				</form>

			</div>
		</div>

	</section>
</div>

@endsection