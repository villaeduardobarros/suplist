@extends('web.layouts.app-simple')

@section('content')

<div class="container my-5 py-5 ">
	<section class="px-md-5 mx-md-5 text-center text-lg-left dark-grey-text">

		<div class="row d-flex justify-content-center">
			<div class="col-md-10 col-lg-8">

				<form class="text-center" action="{{ route('web.draft.authenticate') }}" id="formLoginDraft" method="post">
					@csrf
					<h1 class="h3 mt-4 mb-2">LOGIN DE ACESSO</h1>

					<p class="mb-5">Informe os dados recebidos por e-mail para fazer a autenticação e finalizar seu cadastro.</p>

					<p class="message mb-4" style="margin:0 0 20px;"></p>

					<div class="row mb-4 text-left">
						<div class="col-md-12 col-lg-12">
							<input type="email" name="email" id="email" class="form-control" placeholder="E-mail" autocomplete="off" required>
						</div>
					</div>

					<div class="row mb-3 text-left">
						<div class="col-md-12 col-lg-12">
							<input type="password" name="password" id="password" class="form-control" placeholder="Senha temporária" autocomplete="off" required>
						</div>
					</div>

					<button class="btn btn-success my-5 btn-block" type="submit">ENTRAR</button>
				</form>

			</div>
		</div>

	</section>
</div>

@endsection