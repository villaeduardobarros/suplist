@extends('web.layouts.app-token')

@section('content')

<section>
	<div class="views zoom z-depth-1 rounded mb-4 " id="singleProd">

		<img src="{{ $imageBanner }}" class="img-fluid banners" alt="{{ $company->fantasy_name }}">

		<div class="mask rgba-black-gradient d-md-flex align-items-end justify-content-center single">
			<div class="text-bottom white-text p-4">
				<div class="container">
					<div class="row">

                        <div class="onlydesktop col-lg-2">
                            <img src="{{ $imageLogo }}" alt="{{ $company->fantasy_name }}" class="img-fluid">
						</div>

                        <div class="col-12 col-lg-10">
							<span class="badge badge-{{ (($openAndClose == TRUE) ? 'default' : 'danger') }} mr-2">{{ (($openAndClose == TRUE) ? 'Aberto' : 'Fechado') }}</span>

							<h1 class="font-weight-bold amber-text darken-1" style="font-size: 2rem;">{{ $company->fantasy_name }}</h1>

							<p class="font-weight-normal mb-2">Confira nossos produtos abaixo. Clique no produto para adicionar ao carrinho, clique em meu pedido, informe seus dados e finalize o pedido enviando como mensagem para o nosso whatsapp.</p>

							<p>
								<small>{{ $titleSegment }}</small>

								<!-- <span class="badge badge-warning" style="margin:0 0 0 10px;">
									{{ (($company->value_delivery) ? 'Taxa de Entrega: R$ ' . number_format($company->value_delivery, 2, ',', '.') : 'Entrega Grátis') }}
								</span> -->
							</p>
						</div>

                    </div>
				</div>
			</div>
		</div>
	</div>
</section>

@if ($openAndClose)

	<button class="alert alert-success text-center" role="alert" id="cartresume" onclick="showShoppingCart()">
		Meu pedido <strong>(<span class="total-count">{{ ((Cart::count()) ? Cart::count() : 0 )}}</span> itens)</strong>
	</button>

@endif


@if ($arrProducts)

	<div id="pagination">
		<div class="container">
			<nav>
				<ul class="pagination pg-amber smooth-scroll d-flex mb-0" style="flex-wrap:wrap;">
					<li class="page-item disabled"><a class="page-link">Filtre por</a></li>

					<?php $count = 0; ?>
					@foreach ($arrProducts as $key => $arr)

						<li class="page-item {{ (($count == 0) ? 'active' : NULL) }}">
							<a class="page-link " href="token/{{ $token . '#' . str_slug($key) }}">{{ $key }}</a>
						</li>

						<?php $count++; ?>

					@endforeach
				</ul>
			</nav>
		</div>
	</div>

	<div id="filterm">
		<div class="container">
			<div class="dropdown">
				<button class="btn btn-amber dropdown-toggle w-100" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Filtrar por</button>
				<div class="dropdown-menu dropdown-primary w-100">
					@foreach ($arrProducts as $key => $arr)

						<a class="dropdown-item w-100" href="token/{{ $token . '#' . str_slug($key) }}">{{ $key }}</a>
					
					@endforeach
				</div>
			</div>
		</div>
	</div>

	@foreach ($arrProducts as $key => $arr)

		<div class="container my-5 pt-5 " id="{{ str_slug($key) }}">
			<section>
				<div class="row ">

					<div class="col-12" id="teste">
						<h4 class="text-center font-weight-bold dark-grey-text mb-5">
							<strong>{{ $key }}</strong>
						</h4>
					</div>

					@foreach ($arr as $rowpro)

						<div class="col-lg-4 col-md-6 col-12">
							<div class="card hoverable mb-4"  itemscope itemtype="https://schema.org/Product">
								<div class="card-body">

									<a class="row align-items-center" title="Visualizar detalhes do produto" onclick="{{ (($openAndClose == TRUE) ? 'viewDetails(' . $rowpro['id'] . ')' : 'return alert("Este estabelecimento encontra-se fechado no momento!")') }}">

										<div class="col-5 px-0">
											<img itemprop="image" src="{{ $rowpro['image'] }}" class="img-fluid">
										</div>

										<div class="col-7">
											<strong itemprop="name">{{ $rowpro['title'] }}</strong>

											<p itemprop="description">{{ str_limit($rowpro['description'], 50) }}</p>

											<h6 class="h6-responsive font-weight-bold dark-grey-text" itemscope itemtype="https://schema.org/Offer">
												@if ($rowpro['value_promotional'])

													<strong itemprop="lowprice">R$ {{ number_format($rowpro['value_promotional'], 2, ',', '.') }}</strong>
													<span class="grey-text">
														<small itemprop="price"><s>R$ {{ number_format($rowpro['value'], 2, ',', '.') }}</s></small>
													</span>

												@else
													<strong itemprop="price">R$ {{ number_format($rowpro['value'], 2, ',', '.') }}</strong>
												@endif
											</h6>
										</div>

									</a>

								</div>
							</div>
						</div>

					@endforeach

				<div>
			</section>
		</div>

	@endforeach

@else

	<div class="container my-5 pt-5">
		<section>
			<div class="row">

				<div class="col-12 text-center">
					<h4 class="font-weight-bold dark-grey-text mb-5">
						<strong>Até o momento este estabelecimento não cadastrou nenhum produto!</strong>
					</h4>
					<p>Caso necessário entre em contato</p>
				</div>

			</div>
		</section>
	</div>

@endif

<div class="mt-3 ">
	<section>
		<div class="card">
			<div class="row mx-0 amber darken-1">
				<div class="col-12 col-md-6 px-0">

					<!-- Google Maps -->
					<div id="map-within-card-2" class="map-container rounded-left" style="height: 400px">
						<iframe src="https://maps.google.com/maps?q={{ $addressComplete }}&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					</div>
					<!-- Google Maps -->

				</div>

				<div class="col-12 col-md-6 mx-auto align-self-center p-4 black-text "  itemscope itemtype="https://schema.org/LocalBusiness">
					<h3 class="font-weight-bold" itemprop="name">{{ $company->fantasy_name }}</h3>

					<h5 class="font-weight-normal mb-4" itemprop="description">{{ $company->description }}</h5>

					<p class=" font-weight-light" itemprop="telephone">Telefone: {{ $company->phone }}</p>

					<p class=" font-weight-light mb-0" itemprop="address">Endereço: {{ $addressComplete }}</p>
				</div>
			</div>
		</div>
	</section>
</div>

@endsection
