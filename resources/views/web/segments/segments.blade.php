@extends('web.layouts.app')

@section('content')

<div class="container mt-5 pt-5">
	<section class="dark-grey-text text-center ">

		<div class="row">
			<div class="col-12 ">
				<h3 class="font-weight-normal mx-0 py-5 rounded  my-5 peach-gradient white-text">
					{!! $titlePage !!}
				</h3>
			</div>
		</div>

		@if ($count > 0)

			<div class="row">
				<div class="col-12">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
								Lojas ({{ $count }})
							</a>
						</li>
					</ul>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active py-5 text-left" id="home" role="tabpanel" aria-labelledby="home-tab">
							<div class="row">

								{!! $companies !!}

							</div>
						</div>

					</div>

				</div>
			</div>

		@else

			<div class="row">
            <div class="col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-4 offset-lg-4 text-center">
            <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
<lottie-player src="https://assets1.lottiefiles.com/packages/lf20_bseNSF.json"  background="transparent"  speed="1"  style="width: 100%; height: 300px;"  loop  autoplay></lottie-player>
            </div>
				<div class="col-12 text-center py-5">

					<h3>Nenhum item encontrado</h3>
					<p>Tente buscar com outro termo</p>
				</div>
			</div>

		@endif

	</section>

</div>

@endsection
