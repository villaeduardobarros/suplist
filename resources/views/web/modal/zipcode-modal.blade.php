<div class="modal fade" id="zipcodemodal" tabindex="-1" role="dialog" aria-labelledby="zipcodemodal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

			<form action="{{ route('web.cities.zipcode') }}" id="formZipcodeModal" method="post" role="form">

				<div class="modal-header text-center">
					<h4 class="modal-title w-100 font-weight-bold">Onde você quer receber seu pedido?</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body mx-3">
					<p class="message" style="margin:0 0 10px;"></p>
					
					<div class="md-form mb-5">
						<i class="fas fa-map-marker prefix grey-text"></i>
						<input type="text" name="zipcode_modal" id="zipcode_modal" class="form-control zipcode" placeholder="Informe o seu CEP" autocomplete="off" required>
					</div>
				</div>

				<div class="modal-footer d-flex justify-content-center">
					<button type="button" class="btn btn-default">Buscar</button>
				</div>

			</form>

		</div>
	</div>
</div>