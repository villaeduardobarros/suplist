<div class="modal fade right" id="cart" tabindex="-1" role="dialog" aria-labelledby="cart" aria-hidden="true">
	<div class="modal-dialog modal-full-height modal-right" role="document">
		<div class="modal-content">

			<div class="modal-header">
				<h4 class="modal-title w-100" id="myModalLabel">Meu pedido</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<div class="list-product-cart">
					<div id="imgPreloader" style="text-align:center;width:100%;">
						<img src="web/img/preloader.gif">
					</div>
				</div>

				<div class="form-cart"></div>
			</div>

			@if (Cart::count() > 0)
				<div class="modal-footer justify-content-center" id="footerCart">
					<button type="button" class="btn btn-amber btn-block" id="sendRequest">Enviar Pedido</button>
				</div>
			@endif
			
		</div>
	</div>
</div>