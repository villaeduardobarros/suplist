<div class="modal fade" id="productmodal" tabindex="-1" role="dialog" aria-labelledby="productmodal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

			<form action="{{ route('web.shopping.cart.store') }}" id="formAttributesOptions" method="post" role="form">
				@csrf
				<input type="hidden" name="id" value="{{ $product->id }}">

				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">{{ $product->title }}</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="row">
						<div class="col-12 col-lg-6">
							<img src="{{ $image }}" class="photo img-fluid rounded">
						</div>

						<div class="col-12 col-lg-6">
							<p class="description">{{ str_limit($product->description, 240) }}</p>

							<p><strong>R$ {{ number_format((($product->value_promotional) ? $product->value_promotional : $product->value), 2, ',', '.') }}</strong></p>
						</div>
					</div>

					<div class="list-options">
						<hr class="mb-1">
						<div id="imgPreloader" style="text-align:center;width:100%;display:none;">
							<img src="web/img/preloader.gif">
						</div>
					</div>

					<p class="message"></p>
				</div>

				<div class="modal-footer">
					<div class="row w-100">
						<div class="col-12 col-md-6 pl-0">
							<div class="form-group d-flext">
								<label for="quantity" class="mr-1">Quantidade</label>
								<input type="number" name="quantity" min="1" id="quantity" class="form-control form-control-sm number" value="1" required onkeypress="return checkNumber(event)" style='height: 36px;line-height: 36px; min-height: auto;'>
							</div>
						</div>
						<div class="col-12 col-md-6 pr-0">
							<!-- <button type="button" class="btn btn-block btn-amber add-to-cart" data-name="{{ $product->title }}" data-price="{{ $value }}" data-id="{{ $product->id }}">Adicionar ao carrinho</button> -->
							<button type="button" class="btn btn-block btn-amber" onclick="addShoppingCart()">Adicionar ao carrinho</button>
						</div>
					</div>
				</div>

			</form>

		</div>
	</div>
</div>

<script>
//      // *****************************************
//   // Triggers / Events
//   // *****************************************
//   // Add item
//   $(".add-to-cart").click(function (event) {

//     event.preventDefault();
//     var name = $(this).data("name");
//     var price = Number($(this).data("price"));

//     shoppingCart.addItemToCart(name, price, Number($('#productmodal input[name=qtd]').val()));

//     $('.toast-body').html(name + ' adicionado com sucesso!')
//     $('.toast').toast('show');

//     displayCart();
//   });
</script>
