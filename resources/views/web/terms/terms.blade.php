@extends('web.layouts.app')

@section('content')

<div class="container my-5 py-5 ">
	<section class="px-md-5 mx-md-5  dark-grey-text">
		<div class="row">
			<div class="col-12 mb-md-0 text-muted">

				<h1 class="font-weight-bold mt-5">SUPList - Termo de uso</h1>

				<p>Os Termos de Uso são as nossas regras. É aqui que definimos como vai ser nossa relação. Então, vamos começar nso apresentando: Olá, somos a SUPList plataforma pra que você possa ofertar seu catalogo de produtos digital e receber pedidos via Whatsapp. Ou seja, nós damos a tecnologia necessária para que você receba pedidos online e gerencie seus pedidos.</p>

				<h3>O que NÃO é a SUPList</h3>
				<p>Nós não somos um marketplace. Ou seja, nós não temos qualquer responsabilidade pelo que é vendido. Então, cabe ao VENDEDOR toda a responsabilidade sobre a veracidade e legalidade do que é vendido, e também sobre a entrega do produto ou da prestação do serviço.</p>

				<h3>Sobre pagamentos</h3>
				<p>O estabelecimento irá acordar a forma de pagamento direto com o cliente via Whatsapp. E isso não temos como interferir. São regras que estão além do nosso alcance.</p>

				<h3>Privacidade</h3>
				<p>Seus dados de cadastro, nós armazenamos de forma segura e não repassamos a ninguém. É para uso exclusivo do estritamente necessário para que tudo funcione.</p>

				<h3>Alterações nesses Termos</h3>
				<p>Nós poderemos mudar várias coisas em nosso modelo de negócios, na estrutura da nossa plataforma, e na maneira que somos remunerados pelo nosso serviços. Mudanças relacionadas a questões técnicas de desenvolvimento, layout, usabilidade, e similares, serão feitas constantemente sem aviso prévio.</p>

				<p>Mas, se formos fazer alguma alteração no modelo de negócios avisaremos com antecedência para que o VENDEDOR aceite os novos Termos e siga conosco.</p>

				<h3>Foro de eleição</h3>
				<p>Esperamos que sempre ocorra tudo direitinho, e que não haja nenhum problema. Mas, caso haja, iremos pedir ajuda à Justiça pra arbitrar, e isso será resolvido em Ribeirão Preto - SP.</p>

			</div>
		</div>
	</section>
</div>

@endsection