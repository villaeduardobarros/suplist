<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="color:#485460;font-family:sans-serif;font-size:14px;margin:0 auto;width:650px;">

    <p style="background:#171c27!important;color:#FFF;font-size:18px;padding:16px 0;margin:20px 0 0;text-align:center;"><strong>SUPList Digital</strong></p>

	<div style="padding:0 15px !important;">
		
		<p><strong>Nossas boas-vindas a você,  {{ $field['nameTo'] }},</strong></p>

		<p>Antes de ver todos os benefícios que o SUPList Digital tem para o seu estabelcimento, você tem 
           uma tarefa muito importante para realizar: completar o seu cadastro para que possa desfrutar da 
           nossa plataforma com toda tranquilidade.</p>

        <p>Para realiza-lá é simples, basta clicar no botão "COMPLETAR CADASTRO" e utilizar os dados de acesso abaixo.</p>

		<p>
			E-mail: <strong>{{ $field['emailTo'] }}</strong>
			<br>
			Senha temporária: <strong>{{ $field['passwordTemp'] }}</strong>
        </p>
        
        <br>
        
        <p style="text-align:center;">
            <a style="background:#ff6f00;border:0;border-radius:.125rem;box-shadow:0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);color:#FFF;cursor:pointer;font-size: .81rem;padding:.84rem 2.14rem;text-decoration:none;text-transform:uppercase;word-wrap:break-word;white-space:normal;" href="{{ $field['url'] }}" target="_blank">
                COMPLETAR CADASTRO
            </a>
        </p>

		<br>

		<p><i>Equipe {{ SettingsHelperWeb::searchItem('title-project') }}</i></p>

		<br>

		<p style="font-size:12px;text-align:center;">
            <i>Este e-mail foi gerado automaticamente. Por favor não responda esta mensagem.</i>
        </p>

	</div>

</body>
</html>