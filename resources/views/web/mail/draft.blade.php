<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="color:#485460;font-family:sans-serif;font-size:14px;margin:0 auto;width:650px;">

    <p style="background:#171c27!important;color:#FFF;font-size:18px;padding:16px 0;margin:20px 0 0;text-align:center;"><strong>SUPList Digital</strong></p>

	<div style="padding:0 15px !important;">
		
		<p><strong>Parabéns {{ $field['nameTo'] }}! Você concluiu o seu cadastro.</strong></p>

        <p>Falta muito pouco para você começar a vender!</p>
        
        <p>Acesse o painel utilizando o e-mail cadastrado ({{ $field['emailTo'] }}) e sua senha.</p>

		<br>

		<p><i>Equipe {{ SettingsHelperWeb::searchItem('title-project') }}</i></p>

		<br>

		<p style="font-size:12px;text-align:center;">
			<i>Este e-mail foi gerado automaticamente. Por favor não responda esta mensagem.</i>
		</p>

	</div>

</body>
</html>