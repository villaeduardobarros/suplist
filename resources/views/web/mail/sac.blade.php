<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>

	<p><strong>Nome:</strong> {{ $field->nameFrom }}</p>

	<p><strong>E-mail:</strong> {{ $field->emailFrom }}</p>

	<p><strong>Telefone:</strong> {{ $field->phoneFrom }}</p>

	<p><strong>Assunto:</strong> {{ $field->subjectFrom }}</p>

	<p>
		<strong>Mensagem:</strong>
		<br>
		{{ $field->messageFrom }}
	</p>

</body>
</html>