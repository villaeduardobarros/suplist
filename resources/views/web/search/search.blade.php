@extends('web.layouts.app')

@section('content')

<div class="container mt-5 pt-5">
	<section class="dark-grey-text text-center ">

		<div class="row">
			<div class="col-12">
				<h3 class="font-weight-normal mx-0 py-5 rounded  my-5 aqua-gradient white-text">
					Buscando por <span class="font-weight-bold">{{ $searchedTerm }}</span>
				</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link {{ (($companiesCount > 0) ? 'active' : (($productsCount == 0) ? 'active' : NULL)) }}" id="comapnies-tab" data-toggle="tab" href="#comapnies" role="tab" aria-controls="comapnies" aria-selected="true">Lojas ({{ $companiesCount }})</a>
					</li>

					<li class="nav-item">
						<a class="nav-link {{ (($productsCount > 0 && $companiesCount == 0) ? 'show active' : NULL) }}" id="products-tab" data-toggle="tab" href="#products" role="tab" aria-controls="products" aria-selected="false">Produtos ({{ $productsCount }})</a>
					</li>
				</ul>

				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade {{ (($companiesCount > 0 && $productsCount > 0) ? 'show active' : (($companiesCount > 0 && $productsCount == 0) ? 'show active' : NULL)) }} py-5 text-left" id="comapnies" role="tabpanel" aria-labelledby="comapnies-tab">
						<div class="row">
							@if ($companiesCount > 0)

								{!! $companiesSearch !!}

							@else

                            <div class="col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-4 offset-lg-4 text-center">
                            <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
<lottie-player src="https://assets1.lottiefiles.com/packages/lf20_bseNSF.json"  background="transparent"  speed="1"  style="width: 100%; height: 300px;"  loop  autoplay></lottie-player>
                            </div>
								<div class="col-12 text-center py-5">
									<h3>Nenhum item encontrado</h3>
									<p>Tente buscar com outro termo</p>
								</div>

							@endif
						</div>
					</div>

					<div class="tab-pane fade {{ (($productsCount > 0 && $companiesCount == 0) ? 'show active' : NULL) }} py-5 text-left" id="products" role="tabpanel" aria-labelledby="products-tab">
						<div class="row">
							@if ($productsCount > 0)

								{!! $productsSearch !!}

							@else
                            <div class="col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-4 offset-lg-4 text-center">
                            <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
<lottie-player src="https://assets1.lottiefiles.com/packages/lf20_bseNSF.json"  background="transparent"  speed="1"  style="width: 300px; height: 300px;"  loop controls autoplay></lottie-player>
                            </div>
								<div class="col-12 text-center py-5">
									<h3>Nenhum item encontrado</h3>
									<p>Tente buscar com outro termo</p>
								</div>

							@endif
						</div>
					</div>
				</div>
			</div>
		</div>

	</section>
</div>

@endsection
