<heade class="{{ (Request::is('/')) ? 'home' : '' }}">

	<nav class="navbar navbar-expand-lg mdb-color darken-4 navbar-dark fixed-top scrolling-navbar {{ (Request::is('/')) ? 'home' : '' }}">
		<div class="container">
			<a class="navbar-brand" href="{{ route('web.home') }}">
				<strong>SUPList</strong>
			</a>

			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent-7">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item ">
						<a class="nav-link" href="{{ route('web.about') }}">Sobre</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{ route('web.policy') }}">Política de privacidade</a>
					</li>
				</ul>

				<form action="{{ route('web.search') }}" class="form-inline d-flex justify-content-center md-form form-sm my-0 text-white" method="get">
					<i class="fas fa-search" aria-hidden="true"></i>
					<input class="form-control form-control-sm ml-3 w-75" type="text" name="s" placeholder="Buscar item ou loja" autocomplete="off" aria-label="Search">
				</form>

				<a class="btn btn-white btn-rounded" href="{{ route('web.register') }}">Cadastre sua empresa</a>
			</div>
		</div>
	</nav>

	@if ($banner && $banner == true)

		<div class="view" style="background-image: url('web/img/banner-home.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
			<div class="mask rgba-gradient d-flex justify-content-center align-items-center">
				<div class="container">
					<div class="row mt-5">

						<div class="col-md-6 mb-5 mt-md-0 mt-5 white-text text-center text-md-left">
							<h1 class="h1-responsive font-weight-bold wow fadeInLeft" data-wow-delay="0.3s">SUPList Seu catalogo de produtos online</h1>
							<hr class="hr-light wow fadeInLeft" data-wow-delay="0.3s">
							<h6 class="mb-3 wow fadeInLeft" data-wow-delay="0.3s">Faça já seu catalogo de produtos online e disponibilize para seus clientes façam pedidos direto no whatsapp, sem intermediários.</h6>
							<a href="{{ route('web.register') }}" class="btn btn-outline-white btn-rounded wow fadeInLeft" data-wow-delay="0.3s">Cadastre sua empresa</a>
						</div>

						<div class="col-md-6 col-xl-5 mb-4">
							<div class="card wow fadeInRight" data-wow-delay="0.3s">
								<div class="card-body">
									<div class="text-center">
										<h3 class="cyan-text accent-3 font-weight-bold">
											Os melhores produtos estão aqui. Descubra!</h3>
										<hr class="hr-light">
									</div>

									<form class="text-center" action="{{ route('web.cities.zipcode') }}" id="formZipCode" method="post">
										@csrf
										<p class="message" style="margin:0 0 10px;"></p>
										<input type="text" name="zip_code" id="zip_code" class="form-control zipcode" autocomplete="off" placeholder="Digite seu CEP">
										<div class="text-center mt-4">
											<button type="button" id="btnZipCode" class="btn btn-block aqua-gradient btn-rounded">Buscar</button>
										</div>
									</form>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

	@endif

</header>