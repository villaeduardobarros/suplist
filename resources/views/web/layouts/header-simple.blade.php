<header>
	<nav class="navbar navbar-expand-lg mdb-color darken-4 navbar-dark fixed-top scrolling-navbar">
		<div class="container">
			<a class="navbar-brand" href="{{ URL::to('/') }}">
				<strong>SUPList</strong>
			</a>

			<!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button> -->
		</div>
	</nav>
</header>