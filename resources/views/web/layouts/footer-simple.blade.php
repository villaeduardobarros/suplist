<footer class="page-footer font-small unique-color-dark">
	<div class="footer-copyright text-center py-3">
		© 2020 SUPList - By <a href="https://createstorm.com.br/" target="_blank"> CreateStorm.com.br</a>
	</div>
</footer>
