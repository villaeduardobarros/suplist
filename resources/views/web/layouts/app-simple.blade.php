<?php
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	setlocale(LC_TIME, "pt_BR", "pt_BR.utf-8", "pt_BR.utf-8", "portuguese");
	date_default_timezone_set("America/Sao_Paulo");
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<base href="{{ URL::to('/') }}">
	<title>{{ SettingsHelperWeb::searchItem('title-project') }}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="canonical" href="{{ URL::current() }}"/>
	<?php # exibe CSS controller ?>
	@if ($arrCss)
		@foreach ($arrCss as $css)
			{!! HTML::style($css) !!}
		@endforeach
	@endif

	<script>var baseUrl = '{{ URL::to('/') }}';</script>
</head>
<body>

	<?php # header ?>
	@include('web.layouts.header-simple')

	<?php # conteudo ?>
	@yield('content')

	<?php # header ?>
	@include('web.layouts.footer-simple')

	<?php # exibe JS controller ?>
	@if ($arrJs)
		@foreach ($arrJs as $js)
			{!! HTML::script($js) !!}
		@endforeach
	@endif

</body>
</html>