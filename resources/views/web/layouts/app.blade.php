<?php
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	setlocale(LC_TIME, "pt_BR", "pt_BR.utf-8", "pt_BR.utf-8", "portuguese");
	date_default_timezone_set("America/Sao_Paulo");
?>

<!DOCTYPE html>
<html lang="en" class="{{ (Request::is('/')) ? 'home' : '' }}">
<head>
	<base href="{{ URL::to('/') }}">
	<title>{{ SettingsHelperWeb::searchItem('title-project') }}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="canonical" href="{{ URL::current() }}"/>

    <meta name="theme-color" content="#05ffa3">
    <meta name="msapplication-navbutton-color" content="#05ffa3">
    <link rel="icon" type="image/png" sizes="16x16" href="web/icon.png">

	<!-- meta tags -->
	<meta name="description" content="Faça já seu catalogo de produtos online e disponibilize para seus clientes façam pedidos direto no whatsapp, sem intermediários." /> <!-- na single pegar automatico descricao do estabelecimento 160caracteres, nas demais seria bom padronizar e so mudar se ta no cadastro, sobre etc, se quiser por no admin melhor ainda -->
	<meta name="robots" content="index, follow" />
		<meta property="og:locale" content="pt_BR">

	<meta property="og:url" content="{{ URL::current() }}">

	<meta property="og:title" content="{{ SettingsHelperWeb::searchItem('title-project') }}"> <!-- na single pegar automatico: nome do local - categoria - cidade-uf - suplist.com.br -->
	<meta property="og:site_name" content="SUPList - Seu catalogo de produtos online">
	<meta property="og:description" content="Faça já seu catalogo de produtos online e disponibilize para seus clientes façam pedidos direto no whatsapp, sem intermediários."> <!-- na single pegar automatico descricao do estabelecimento 160caracteres, nas demais seria bom padronizar e so mudar se ta no cadastro, sobre etc, se quiser por no admin melhor ainda -->

	<meta property="og:image" content="www.meusite.com.br/imagem.jpg"> <!-- logo do estabelecimento -->
	<meta property="og:image:type" content="image/jpeg">
	<meta property="og:image:width" content="768">
	<meta property="og:image:height" content="768">
	<meta property="og:type" content="website">
	<!-- meta tags -->

	<?php # exibe CSS controller ?>
	@if ($arrCss)
		@foreach ($arrCss as $css)
			{!! HTML::style($css) !!}
		@endforeach
	@endif

	<script>var baseUrl = '{{ URL::to('/') }}';</script>
</head>
<body class="{{ (Request::is('/')) ? 'home' : '' }}">

	<?php # header ?>
	@include('web.layouts.header')

	<?php # conteudo ?>
	@yield('content')

	<div class="get-modal"></div>

	<?php # header ?>
	@include('web.layouts.footer')

	<?php # standard modal ?>
	<div id="getModal"></div>

	<?php # exibe JS controller ?>
	@if ($arrJs)
		@foreach ($arrJs as $js)
			{!! HTML::script($js) !!}
		@endforeach
	@endif

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-4589403-61"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-4589403-61');
</script>


</body>
</html>
