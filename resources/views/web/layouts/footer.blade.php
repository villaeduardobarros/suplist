<footer class="page-footer font-small unique-color-dark">
	<div class="container text-center text-md-left">
		<div class="row">

			<div class="col-md-3 mx-auto">
				<h5 class="font-weight-bold text-uppercase mt-3 mb-4">Institucional</h5>

				<ul class="list-unstyled">
					<li><a href="{{ route('web.about') }}">Sobre</a></li>
					<li><a href="{{ route('web.policy') }}">Política de privacidade</a></li>
					<li><a href="{{ route('web.sac') }}">SAC</a></li>
					<li><a href="{{ route('web.register') }}">Cadastre sua empresa</a></li>
					<li><a href="{{ route('control.login') }}">Login Empresas</a></li>
				</ul>
			</div>

			<hr class="clearfix w-100 d-md-none">

			<!-- <div class="col-md-3 mx-auto">
				<h5 class="font-weight-bold text-uppercase mt-3 mb-4">Segmentos</h5>

				<ul class="list-unstyled">
					<li><a href="#!">Link 1</a></li>
					<li><a href="#!">Link 2</a></li>
					<li><a href="#!">Link 3</a></li>
					<li><a href="#!">Link 4</a></li>
				</ul>
			</div> -->

			<hr class="clearfix w-100 d-md-none">

			<div class="col-md-3 mx-auto">
				<h5 class="font-weight-bold text-uppercase mt-3 mb-4">Cidades</h5>

				{!! SettingsHelperWeb::mountPreviewCitiesFooter() !!}
			</div>

			<hr class="clearfix w-100 d-md-none">

			<div class="col-md-3 mx-auto">
				<h5 class="font-weight-bold text-uppercase mt-3 mb-4">NEWSLETTER</h5>

				<p class="white-text w-responsive">Receba nossa newsletter</p>

				<div class="form-wrap" data-form-type="formoid">
					<form action="{{ route('web.footer.newsletter.register') }}" id="formNewsletter" method="post" class="form-inline d-flex md-form form-sm mt-0 mb-3 text-white" novalidate="novalidate">
						@csrf
						<button type="submit" id="btnForm" class="aqua-gradient">
							<i class="fas fa-envelope white-text" aria-hidden="true"></i>
						</button>
						<input class="form-control form-control-sm ml-2 w-75 mb-0" type="email" name="email_news" id="email_news" required placeholder="Informe seu e-mail" autocomplete="off" aria-label="Newsletter">
						<span class="message" style="margin:0 0 20px;">{{ session()->get('message') }}</span>
					</form>
				</div>
			</div>

		</div>
	</div>

	<div class="footer-copyright text-center py-3">
		© 2020 SUPList - By <a href="https://createstorm.com.br/" target="_blank"> CreateStorm.com.br</a>
	</div>
</footer>
