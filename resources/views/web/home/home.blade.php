@extends('web.layouts.app')

@section('content')

<div class="container mt-5">
	<section class="dark-grey-text text-center">
		{!! $productsFeatured !!}
	</section>
</div>

<div class="container my-5">
	<section>
		<div class="row">

			<div class="col-lg-4 col-md-12">
				{!! $productsPromotions !!}
			</div>

			<div class="col-lg-4 col-md-12">
				{!! $productsLaunches !!}
			</div>

			<div class="col-lg-4 col-md-12">
				{!! $productsMoreOrders !!}
			</div>

		</div>
	</section>
</div>

@endsection