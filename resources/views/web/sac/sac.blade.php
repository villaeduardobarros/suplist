@extends('web.layouts.app')

@section('content')

<div class="container my-5 py-5 ">
	<section class="px-md-5 mx-md-5 text-center text-lg-left dark-grey-text">
		<div class="row d-flex justify-content-center">
			<div class="col-md-10 col-lg-8">

				<form class="text-center" action="{{ route('web.sac.store') }}" id="formRegisterSac" method="post">
					@csrf
					<h1 class="h3 mt-4 mb-2">Atendimento ao Cliente</h1>

					<p class="mb-5">Utilize o formulário abaixo pra entrar em contato conosco.</p>

					<p class="message mb-4" style="margin:0 0 20px;"></p>

					<input type="text" id="name" name="name" class="form-control mb-4" placeholder="Nome responsável" autocomplete="off">

					<input type="email" id="email" name="email" class="form-control mb-4" placeholder="E-mail" autocomplete="off">

					<input type="text" id="phone" name="phone" class="form-control mb-4 phone" placeholder="Telefone" autocomplete="off">

					<select id="subject" name="subject" class="browser-default custom-select mb-4">
						<option value="">Selecione um assunto</option>
						<option value="1">Sugestão de melhoria</option>
						<option value="2">Report a bug</option>
						<option value="3">Reclamação</option>
						<option value="4">Desejo anunciar</option>
					</select>

					<div class="form-group">
						<textarea class="form-control rounded-0" id="message" name="message" rows="3" placeholder="Message"></textarea>
					</div>

					<div class="custom-control custom-checkbox mt-3">
						<input type="checkbox" id="news" name="news" class="custom-control-input" checked>
						<label class="custom-control-label" for="news">Desejo receber novidades por e-mail</label>
					</div>

					<button class="btn btn-amber my-5 btn-block" type="submit">ENVIAR</button>
				</form>

			</div>
		</div>
	</section>
</div>

@endsection