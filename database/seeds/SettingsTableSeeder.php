<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		DB::table('settings')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
		
		DB::table('settings')->insert([
			['label' => 'base-url',					'value' => 'http://127.0.0.1:8000/'],
			['label' => 'title-project',			'value' => 'SUPList Digital'],

			['label' => 'draft-email',				'value' => 'villa.eduardobarros@gmail.com'],
			['label' => 'draft-name',				'value' => 'SUPList '],
			['label' => 'draft-subject',			'value' => 'SUPList Cadastramento'],
			
			['label' => 'register-email',			'value' => 'villa.eduardobarros@gmail.com'],
			['label' => 'register-name',			'value' => 'SUPList '],
			['label' => 'register-subject',			'value' => 'SUPList Cadastro Finalizado'],
			
			['label' => 'sac-email',				'value' => 'villa.eduardobarros@gmail.com'],
			['label' => 'sac-name',					'value' => 'SUPList '],
			['label' => 'sac-subject',				'value' => 'SAC SUPList'],
			
			['label' => 'folder-upload-products',	'value' => 'products/'],
			['label' => 'width-products',			'value' => '781'],
			['label' => 'height-products',			'value' => '520'],
			
			['label' => 'folder-upload-companies',	'value' => 'companies/'],
			['label' => 'width-companies',			'value' => '781'],
			['label' => 'height-companies',			'value' => '520'],
		]);
	}
}
