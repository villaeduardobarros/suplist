<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		DB::table('states')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('states')->insert([
            ['id' => 1, 'title' => 'Acre', 'abbreviation' => 'AC'],
            ['id' => 2, 'title' => 'Alagoas', 'abbreviation' => 'AL'],
            ['id' => 3, 'title' => 'Amapá', 'abbreviation' => 'AP'],
            ['id' => 4, 'title' => 'Amazonas', 'abbreviation' => 'AM'],
            ['id' => 5, 'title' => 'Bahia', 'abbreviation' => 'BA'],
            ['id' => 6, 'title' => 'Ceará', 'abbreviation' => 'CE'],
            ['id' => 7, 'title' => 'Distrito Federal', 'abbreviation' => 'DF'],
            ['id' => 8, 'title' => 'Espírito Santo', 'abbreviation' => 'ES'],
            ['id' => 9, 'title' => 'Goiás', 'abbreviation' => 'GO'],
            ['id' => 10, 'title' => 'Maranhão', 'abbreviation' => 'MA'],
            ['id' => 11, 'title' => 'Mato Grosso', 'abbreviation' => 'MT'],
            ['id' => 12, 'title' => 'Mato Grosso do Sul', 'abbreviation' => 'MS'],
            ['id' => 13, 'title' => 'Minas Gerais', 'abbreviation' => 'MG'],
            ['id' => 14, 'title' => 'Pará', 'abbreviation' => 'PA'],
            ['id' => 15, 'title' => 'Paraíba', 'abbreviation' => 'PB'],
            ['id' => 16, 'title' => 'Paraná', 'abbreviation' => 'PR'],
            ['id' => 17, 'title' => 'Pernambuco', 'abbreviation' => 'PE'],
            ['id' => 18, 'title' => 'Piauí', 'abbreviation' => 'PI'],
            ['id' => 19, 'title' => 'Rio de Janeiro', 'abbreviation' => 'RJ'],
            ['id' => 20, 'title' => 'Rio Grande do Norte', 'abbreviation' => 'RN'],
            ['id' => 21, 'title' => 'Rio Grande do Sul', 'abbreviation' => 'RS'],
            ['id' => 22, 'title' => 'Rondônia', 'abbreviation' => 'RO'],
            ['id' => 23, 'title' => 'Roraima', 'abbreviation' => 'RR'],
            ['id' => 24, 'title' => 'Santa Catarina', 'abbreviation' => 'SC'],
            ['id' => 25, 'title' => 'São Paulo', 'abbreviation' => 'SP'],
            ['id' => 26, 'title' => 'Sergipe', 'abbreviation' => 'SE'],
            ['id' => 27, 'title' => 'Tocantins', 'abbreviation' => 'TO'],
        ]);
    }
}
