<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments_charges', function (Blueprint $table) {
            $table->increments('id');
            $table->date('due_date')->nullable();
            $table->float('value', 10, 3)->nullable();
            $table->integer('companies_id')->nullable()->unsigned();
            $table->integer('payments_form_id')->nullable()->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('companies_id')->nullable()->references('id')->on('companies');
            $table->foreign('payments_form_id')->nullable()->references('id')->on('payments_form');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments_charges');
    }
}
