<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100)->nullable();
            $table->string('address', 100)->nullable();
            $table->integer('number')->nullable();
            $table->string('complement', 100)->nullable();
            $table->string('neighborhood', 100)->nullable();
            $table->string('zip_code', 10)->nullable();
            $table->char('is_active', 1)->nullable();
            $table->integer('cities_id')->nullable()->unsigned();
            $table->integer('states_id')->nullable()->unsigned();
            $table->integer('companies_id')->nullable()->unsigned();
            $table->integer('customers_id')->nullable()->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('cities_id')->nullable()->references('id')->on('cities');
            $table->foreign('states_id')->nullable()->references('id')->on('states');
            $table->foreign('companies_id')->nullable()->references('id')->on('companies');
            $table->foreign('customers_id')->nullable()->references('id')->on('customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
