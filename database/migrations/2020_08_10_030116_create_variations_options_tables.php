<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariationsOptionsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variations_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('products_id')->nullable()->unsigned();
            $table->integer('attributes_id')->nullable()->unsigned();
            $table->integer('attributes_options_id')->nullable()->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('products_id')->nullable()->references('id')->on('companies');
            $table->foreign('attributes_id')->nullable()->references('id')->on('attributes');
            $table->foreign('attributes_options_id')->nullable()->references('id')->on('attributes_options');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variations_options');
    }
}
