<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {            
            $table->char('type', 1)->nullable()->after('id');
            $table->float('value', 10, 3)->nullable()->after('description_packing');
            $table->float('value_promotional', 10, 3)->nullable()->after('value');
            $table->integer('companies_id')->nullable()->unsigned()->after('is_active');
            $table->integer('categories_id')->nullable()->unsigned()->after('companies_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}
