<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAffiliatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliates', function (Blueprint $table) {
            $table->increments('id');
            $table->text('token');
            $table->string('name', 100)->nullable();
            $table->string('slug', 100)->nullable();
            $table->string('id_card', 45)->nullable();
            $table->string('reg_of_individuals', 20)->nullable()->unique();
            $table->string('email', 100)->nullable()->unique();
            $table->string('phone', 15)->nullable();
            $table->string('cell_phone', 15)->nullable();
            $table->char('is_active', 1)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affiliates');
    }
}
