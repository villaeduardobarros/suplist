<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributesOptionsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes_options', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100)->nullable();
            $table->char('is_active', 1)->nullable();
            $table->integer('attributes_id')->nullable()->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('attributes_id')->nullable()->references('id')->on('attributes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes_options');
    }
}
