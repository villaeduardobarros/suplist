<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->text('token');
            $table->char('type_of_person', 1);
            $table->string('name', 100)->nullable();
            $table->string('corporate_name', 100)->nullable();
            $table->string('fantasy_name', 100)->nullable()->index();
            $table->string('slug', 100)->nullable();
            $table->string('id_card', 45)->nullable();
            $table->string('reg_of_individuals', 20)->nullable()->unique();
            $table->string('reg_legal_entity', 20)->nullable()->unique();
            $table->string('email', 100)->nullable()->unique();
            $table->string('phone', 15)->nullable();
            $table->string('cell_phone', 15)->nullable();
            $table->longText('description')->nullable();
            $table->float('value_delivery', 10, 3)->nullable();
            $table->string('hours_monday', 15)->nullable();
            $table->string('hours_tuesday', 15)->nullable();
            $table->string('hours_wednesday', 15)->nullable();
            $table->string('hours_thursday', 15)->nullable();
            $table->string('hours_friday', 15)->nullable();
            $table->string('hours_saturday', 15)->nullable();
            $table->string('hours_sunday', 15)->nullable();
            $table->char('is_featured', 1)->nullable();
            $table->char('is_active', 1)->nullable();
            $table->integer('categories_id')->nullable()->unsigned();
            $table->integer('drafts_id')->nullable()->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('categories_id')->nullable()->references('id')->on('categories');
            $table->foreign('drafts_id')->nullable()->references('id')->on('drafts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
