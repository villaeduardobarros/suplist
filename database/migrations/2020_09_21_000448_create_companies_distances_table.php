<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesDistancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies_distances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('start_distance')->nullable();
            $table->integer('end_distance')->nullable();
            $table->float('value', 10, 3)->nullable();
            $table->time('time', 0)->nullable();
            $table->integer('companies_id')->nullable()->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('companies_id')->nullable()->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies_distances');
    }
}
