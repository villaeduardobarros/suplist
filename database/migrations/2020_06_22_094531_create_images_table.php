<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100)->nullable();
            $table->string('file', 100)->index();;
            $table->char('is_active', 1)->nullable();
            $table->integer('companies_id')->nullable()->unsigned();
            $table->integer('products_id')->nullable()->unsigned();
            $table->integer('categories_id')->nullable()->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('companies_id')->nullable()->references('id')->on('companies');
            $table->foreign('products_id')->nullable()->references('id')->on('products');
            $table->foreign('categories_id')->nullable()->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
