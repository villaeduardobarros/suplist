<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100)->index();
            $table->string('slug', 100);
            $table->text('barcode')->nullable();
            $table->longText('description')->nullable();
            $table->longText('description_packing')->nullable();
            $table->float('value', 10, 3);
            $table->float('value_discount', 10, 3)->nullable();
            $table->float('value_percentage', 10, 3)->nullable();
            $table->char('is_featured', 1)->nullable();
            $table->char('is_active', 1)->nullable();
            $table->integer('companies_id')->unsigned();
            $table->integer('categories_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('companies_id')->nullable()->references('id')->on('companies');
            $table->foreign('categories_id')->nullable()->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
