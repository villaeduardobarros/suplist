<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->float('amount', 10, 3);
            $table->float('value_delivery', 10, 3)->nullable();
            $table->float('value_change', 10, 3)->nullable();
            $table->longText('comments')->nullable();
            $table->integer('companies_id')->nullable()->unsigned();
            $table->integer('customers_id')->nullable()->unsigned();
            $table->integer('addresses_id')->nullable()->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('companies_id')->nullable()->references('id')->on('companies');
            $table->foreign('customers_id')->nullable()->references('id')->on('customers');
            $table->foreign('addresses_id')->nullable()->nullable()->references('id')->on('addresses');
        });

        Schema::create('orders_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quantity')->lenght('5')->nullable();
            $table->float('value_unitary', 10, 3);
            $table->longText('comments')->nullable();
            $table->integer('orders_id')->nullable()->unsigned();
            $table->integer('products_id')->nullable()->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('orders_id')->nullable()->references('id')->on('orders');
            $table->foreign('products_id')->nullable()->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');

        Schema::dropIfExists('orders_products');
    }
}
