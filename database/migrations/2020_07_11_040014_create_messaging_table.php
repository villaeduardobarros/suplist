<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messaging', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 100)->nullable()->unique();
            $table->integer('drafts_id')->nullable()->unsigned();
            $table->integer('companies_id')->nullable()->unsigned();
            $table->timestamps();

            $table->foreign('drafts_id')->nullable()->references('id')->on('drafts');
            $table->foreign('companies_id')->nullable()->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messaging');
    }
}
