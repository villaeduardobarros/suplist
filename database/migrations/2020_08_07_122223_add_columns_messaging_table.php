<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsMessagingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messaging', function (Blueprint $table) {
            $table->dropColumn('email');

            $table->string('subject', 100)->nullable()->after('id');
            $table->string('email_to', 100)->nullable()->after('subject');
            $table->string('name_to', 100)->nullable()->after('email_to');
            $table->string('email_from', 100)->nullable()->after('name_to');
            $table->string('name_from', 100)->nullable()->after('email_from');
            $table->string('email_cc', 100)->nullable()->after('name_from');
            $table->string('email_bcc', 100)->nullable()->after('email_cc');
            $table->string('view', 100)->nullable()->after('email_bcc');
            $table->text('field')->nullable()->after('view');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messaging', function (Blueprint $table) {
            //
        });
    }
}
